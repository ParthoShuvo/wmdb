<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 1:26 PM
 */

namespace App\Models;


use App\Http\Controllers\DataRetrievers\interfaces\TVSeriesDataAttributes;
use App\Http\Controllers\DataRetrievers\interfaces\TVSeriesSeasonsDataAttributes;
use Illuminate\Database\Eloquent\Model;

class TVSeriesSeason extends Model implements
    TVSeriesDataAttributes, TVSeriesSeasonsDataAttributes
{
    public $timestamps = false;
    protected $table = 'TV_SERIES_SEASONS';
    protected $primaryKey = 'seasonId';
    protected $guarded = [
        self::seasonId,
        self::seasonNo,
        self::tvSeriesId
    ];

    protected $fillable = [
        self::startYear
    ];

    /**
     * TVSeriesSeasons constructor.
     */
    public function __construct()
    {
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tvSeries()
    {
        return $this->belongsTo('App\Models\TVSeries', 'tvSeriesId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tvSeriesEpisodes()
    {
        return $this->hasMany('App\Models\TVSeriesEpisode', 'seasonId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trailers()
    {
        return $this->hasMany('App\Models\TVTrailer', 'seasonId');
    }


    public function insertTVSeriesSeaosnData($inputData)
    {
        $tvSeriesId = $inputData['tvSeriesId'];
        $seasonNo = $inputData['seasonNo'];
        $existingTVSeriesSeasons = $this->getTVSeriesSeasonData($tvSeriesId, $seasonNo);
        if ($existingTVSeriesSeasons != null) {
            return false;
        } else {
            $newTVSeriesSeasons = new TVSeriesSeason();
            $newTVSeriesSeasons->seasonNo = $seasonNo;
            $newTVSeriesSeasons->tvSeriesId = $tvSeriesId;
            $newTVSeriesSeasons->startYear = $inputData['startYear'];
            $newTVSeriesSeasons->save();
            return true;
        }
    }

    /**
     * @param $tvSeriesId
     * @param $seasonNo
     * @return mixed
     */
    public function getTVSeriesSeasonData($tvSeriesId, $seasonNo)
    {
        $tvSeries = TVSeries::find($tvSeriesId);
        $tvSeriesSeasons = $tvSeries->tvSeriesSeasons()
            ->where($this::seasonNo, '=', $seasonNo)->first();
        return $tvSeriesSeasons;
    }


    public function getUpcomingTVSeasons()
    {
        $currentDate = date('Y-m-d');
        $upComingTVSeasonsList = TVSeriesSeason::where('startYear', '>', $currentDate)
            ->distinct()
            ->get(['tvSeriesId']);
        return $upComingTVSeasonsList;
    }


}