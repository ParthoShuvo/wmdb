<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/4/2015
 * Time: 8:21 PM
 */

namespace App\Models;


use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\DataRetrievers\interfaces\TheaterDataAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Theater extends Model implements TheaterDataAttributes
{
    public $timestamps = false;
    protected $table = 'THEATERS';
    protected $primaryKey = 'theaterId';
    protected $guarded = ['theaterId'];
    protected $fillable = [
        'theaterName',
        'locationCity',
        'countryCode',
        'address',
        'noOfHalls',
        'webSites'
    ];

    /**
     * Theater constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $cityName
     * @param $countryCode
     * @return bool
     */
    public static function isCityNameExist($cityName, $countryCode)
    {
        $isExist = false;
        $theater = self::where(self::cityName, $cityName)
            ->where(self::countryCode, $countryCode)
            ->first();
        if ($theater != null) {
            $isExist = true;
        }
        return $isExist;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function movies()
    {
        return $this->belongsToMany(
            'App\Models\Movie',
            'Now_Showings',
            'theaterId',
            'movieId'
        )->withPivot('active');
    }

    /**
     * @param $cityName
     * @param null $countryCode
     * @return mixed
     */
    public function getMovieTheaterList($cityName)
    {
        $movieTheaterList = $this::where($this::cityName, $cityName)
            ->orderBy('theaterName', 'ASC')
            ->orderBy('address', 'ASC')
            ->get()
            ->toArray();
        return $movieTheaterList;
    }

    public function getTheaterData($theaterName, $cityName)
    {
        $theater = $this::where($this::theaterName, '=', $theaterName)
            ->where($this::cityName, '=', $cityName)
            ->first();
        return $theater;
    }

    /**
     * @param Theater $theater
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNowShowingMoviesInTheater(Theater $theater)
    {
        $subQuery = DB::table('movies')
            ->join('now_showings', 'movies.movieId', '=', 'now_showings.movieId')
            ->select('movies.movieId')
            ->where('now_showings.theaterId', '=', $theater['theaterId'])
            ->where('now_showings.active', '=', 1)
            ->get();
        $nowShowingMovies = DB::table('movies')
            ->leftJoin('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->leftJoin('BoxOffices', 'movies.movieId', '=', 'BoxOffices.movieId')
            ->select(DB::raw(
                'movies.movieName,
                movies.summary,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                avg(feedbacks.rating) as rate'))
            ->whereIn('movies.movieId', $subQuery)
            ->groupBy('movies.movieId')
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.movieName', 'ASC')
            ->orderBy('releaseYear', 'DESC')
            ->get();
        return $nowShowingMovies;
    }

    public function insertTheaterData($inputData)
    {
        $theaterName = $inputData['theaterName'];
        $locationCity = $inputData['locationCity'];
        $address = $inputData['address'];
        $countryCode = $inputData['countryCode'];

        $existingTheaterData = $this::where($this::theaterName, '=', $theaterName)
            ->where($this::theaterLocality, '=', $locationCity)
            ->where($this::countryCode, '=', $countryCode)
            ->first();
        if ($existingTheaterData != null) {
            return false;
        } else {
            $newTheater = new Theater();
            $newTheater->theaterName = $theaterName;
            $newTheater->locationCity = $locationCity;
            $newTheater->countryCode = $countryCode;
            $newTheater->address = AdminPageController::checkNullValue($address);
            $newTheater->noOfHalls = AdminPageController::checkNullValue($inputData['noOfHalls']);
            $newTheater->webSite = AdminPageController::checkNullValue($inputData['webSite']);
            if (isset($inputData['theaterImage'])) {
                $theaterImage = $inputData['theaterImage'];
                $theaterImageName = camel_case($theaterName) . '.' . $theaterImage->getClientOriginalExtension();
                $newTheater->theaterImage = $theaterImageName;
                AdminPageController::storeImage('theater', $theaterImage, $theaterImageName);
            }
            $newTheater->save();
            return true;
        }

    }


}