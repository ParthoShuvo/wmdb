<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 9:38 PM
 */

namespace App\Models;


use Illuminate\Support\Facades\DB;

class TVTrailer extends Trailer
{
    protected $table = 'TV_Trailers';
    protected $guarded = ['trailerId', 'seasonId'];


    /**
     * TVTrailer constructor.
     */
    public function __construct()
    {
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tvSeriesSeasons()
    {
        return $this->belongsTo('App\Models\TVSeriesSeason', 'seasonId');
    }

    /**
     * @return array|null
     */
    public function getRecentlyAddedTrailers()
    {
        $tvTrailerList = DB::table('tv_trailers')
            ->join('tv_series_seasons', 'tv_trailers.seasonId', '=', 'tv_series_seasons.seasonId')
            ->join('tv_series', 'tv_series_seasons.tvSeriesId', '=', 'tv_series.tvSeriesId')
            ->select(
                'tv_series.seriesName',
                'tv_series.imageName',
                'tv_series_seasons.seasonNo',
                'tv_trailers.trailerNo',
                'tv_trailers.trailerCode',
                'tv_trailers.uploadDate'
            )
            ->orderBy('tv_trailers.uploadDate', 'DESC')
            ->orderBy('tv_trailers.trailerNo', 'DESC')
            ->limit(10)
            ->get();
        return $tvTrailerList;
    }


    public function getRecentTrailers()
    {
        $trailerList = null;
        $trailer = new Trailer();
        $movieTrailerList = $trailer->getRecentlyAddedTrailers();
        $tvTrailerList = $this->getRecentlyAddedTrailers();
        if ($movieTrailerList == null || count($movieTrailerList) == 0) {
            $trailerList = $tvTrailerList;
        } else if ($tvTrailerList == null || count($tvTrailerList) == 0) {
            $trailerList = $movieTrailerList;
        } else {
            $trailerList = $this->mergeTrailerList($movieTrailerList, $tvTrailerList);
        }
        return $trailerList;
    }

    /**
     * @param $movieId
     * @return bool
     */
    /* public function hasTrailer($tvSeriesData)
     {
         $trailers = $this->getTrailer($tvSeriesData);
         if ($trailers != null && count($trailers) > 0) {
             return true;
         } else {
             return false;
         }
     }*/


    public function getTrailer($tvSeriesData)
    {
        $trailerList = DB::table('tv_trailers')
            ->join('tv_series_seasons', 'tv_trailers.seasonId', '=', 'tv_series_seasons.seasonId')
            ->join('tv_series', 'tv_series_seasons.tvSeriesId', '=', 'tv_series.tvSeriesId')
            ->where('tv_series.tvSeriesId', '=', $tvSeriesData['tvSeriesId'])
            ->select(
                'tv_series.seriesName',
                'tv_series_seasons.seasonNo',
                'tv_series.genre',
                'tv_trailers.trailerId',
                'tv_trailers.trailerNo',
                'tv_trailers.trailerCode',
                'tv_series.summary'
            )
            ->orderBy('tv_trailers.trailerNo', 'DESC')
            ->get();
        return $trailerList;
    }


    /**
     * @param $trailerId
     */
    public function increaseViewCount($trailerId)
    {
        $trailer = $this::find($trailerId);
        if ($trailer != null) {
            $trailer->viewCount += 1;
            $trailer->save();
        }

    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertTVSeriesTrailers($inputData)
    {
        $tvSeriesId = $inputData['tvSeriesId'];
        $seasonNo = $inputData['seasonNo'];
        $trailerNo = $inputData['trailerNo'];
        $trailerCode = $inputData['trailerCode'];
        $uploadDate = $inputData['uploadDate'];
        $tvSeriesSeason = new TVSeriesSeason();
        $existingTVSeriesSeason = $tvSeriesSeason->getTVSeriesSeasonData(
            $tvSeriesId, $seasonNo);
        if ($existingTVSeriesSeason != null) {
            $existingTrailer = $existingTVSeriesSeason->trailers()
                ->where($this::trailerNo, '=', $trailerNo)
                ->where($this::trailerCode, '=', $trailerCode)
                ->first();
            if ($existingTrailer != null) {
                $existingTrailer->uploadDate = $uploadDate;
                $existingTrailer->save();
                return true;
            } else {
                $newTrailer = new TVTrailer();
                $newTrailer->seasonId = $existingTVSeriesSeason['seasonId'];
                $newTrailer->trailerNo = $seasonNo;
                $newTrailer->trailerCode = $trailerCode;
                $newTrailer->uploadDate = $uploadDate;
                $newTrailer->save();
                return true;
            }
        }
        return false;
    }

    private function mergeTrailerList($movieTrailerList, $tvTrailerList)
    {
        $count1 = count($movieTrailerList);
        $count2 = count($tvTrailerList);
        $totalCount = ($count1 + $count2);
        array_push($movieTrailerList, ['uploadDate' => '1950-10-01']);
        array_push($tvTrailerList, ['uploadDate' => '1950-10-01']);
        $i = 0;
        $j = 0;
        $trailerList = [];
        for ($k = 0; $k < $totalCount; $k++) {
            $movieTrailer = $movieTrailerList[$i];
            $tvTrailer = $tvTrailerList[$j];
            if ($movieTrailer['uploadDate'] >= $tvTrailer['uploadDate']) {
                array_push($trailerList, $movieTrailer);
                $i++;
            } else {
                array_push($trailerList, $tvTrailer);
                $j++;
            }
        }
        return $trailerList;
    }


}