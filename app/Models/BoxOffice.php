<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/19/2015
 * Time: 10:50 AM
 */

namespace App\Models;


use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use Illuminate\Database\Eloquent\Model;

class BoxOffice extends Model implements MovieDataAttributes
{
    public $timestamps = false;
    protected $table = 'BoxOffices';
    protected $primaryKey = 'movieId';
    protected $fillable = [
        'budget',
        'weekendCollection',
        'gross'
    ];


    public function  __construct($movieId = null)
    {
        if ($movieId != null) {
            $this->movieId = $movieId;
        }
    }

    public function movies()
    {
        return $this->belongsTo('App\Models\Movie');
    }

    public function getBoxOfficeCollection()
    {
        $movie = Movie::where($this::movieId, $this->movieId)->first();
        $boxOfficeRes = $movie->boxOffices;
        $boxOffice = [];
        if ($boxOfficeRes != null) {
            $boxOffice = [
                'budget' => $this->checkMoneyValue($boxOfficeRes->budget),
                'openingWeekend' => $this->checkMoneyValue($boxOfficeRes->weekendCollection),
                'gross' => $this->checkMoneyValue($boxOfficeRes->gross)
            ];
        }
        return $boxOffice;
    }

    private function checkMoneyValue($value)
    {
        if ($value == null && $value == '') {
            return 'Not Added Yet';
        } else {
            return '$' . $value . '(approximate)';
        }
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertBoxOfficeData($inputData)
    {
        $movieId = (int)$inputData['movieId'];
        $budget = AdminPageController::checkNullValue($inputData['budget']);
        $weekendCollection = AdminPageController::checkNullValue($inputData['weekendCollection']);
        $gross = AdminPageController::checkNullValue($inputData['gross']);
        $movie = Movie::where('movieId', '=', $movieId)->first();
        $existBoxOfficeData = $movie->boxOffices()->first();
        if ($existBoxOfficeData != null) {
            $existBoxOfficeData->budget = $budget;
            $existBoxOfficeData->weekendCollection = $weekendCollection;
            $existBoxOfficeData->gross = $gross;
            $existBoxOfficeData->save();
            return true;
        } else {
            $newBoxOfficeData = new BoxOffice();
            $newBoxOfficeData->movieId = $movieId;
            $newBoxOfficeData->budget = $budget;
            $newBoxOfficeData->weekendCollection = $weekendCollection;
            $newBoxOfficeData->gross = $gross;
            $newBoxOfficeData->save();
            return true;
        }

    }
}