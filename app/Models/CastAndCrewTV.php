<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 8:29 PM
 */

namespace App\Models;


class CastAndCrewTV extends CastAndCrew
{
    protected $table = 'CASTS_AND_CREWS_TVS';
    protected $guarded = ['tvSeriesId', 'profileId'];
    private $tvSeriesId = null;
    private $tvSeries = null;

    public function __construct($tvSeriesId = null, $profileId = null)
    {
        if ($tvSeriesId == null) {
            $this->profileId = $profileId;
        } else if ($profileId == null) {
            $this->tvSeriesId = $tvSeriesId;
            $this->tvSeries = TVSeries::where('tvSeriesId', $this->tvSeriesId)->first();
        }
    }

    /**
     * @return array|null
     */
    public function getActorList()
    {
        $data = null;
        $profileList = $this->getCastAndCrewData('Actor');
        if ($profileList != null && count($profileList) > 0) {
            $data = [];
            for ($i = 0; $i < count($profileList); $i++) {
                $profile = $profileList[$i];
                $data[$i] = [
                    'actorName' => $profile->name,
                    'image' => $profile->profileImage,
                    'role' => $profile->pivot->role
                ];
            }
        }
        return $data;
    }

    /**
     * @param $profession
     * @return mixed
     */
    public function getCastAndCrewData($profession)
    {
        $profileList = $this->tvSeries->profiles
            ->where($this::profession, ucfirst($profession));
        return $profileList;
    }

    /**
     * @return array|null
     */
    public function getDirectorList()
    {
        $data = null;
        $profileList = $this->getCastAndCrewData('Director');
        if ($profileList != null && count($profileList) > 0) {
            $data = [];
            for ($j = 0; $j < count($profileList); $j++) {
                $profile = $profileList[$j];
                if ($profile->profession == 'Director') {
                    $data[$j] = [
                        'directorName' => $profile->name,
                        'image' => $profile->image,
                        'directorPosition' => $profile->pivot->role
                    ];
                }
            }
        }
        return $data;
    }

}