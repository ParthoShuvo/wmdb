<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/18/2015
 * Time: 8:16 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MovieNews extends Model
{
    public $timestamps = false;
    protected $table = 'movie_news';
    protected $primaryKey = 'newsId';
    protected $guarded = [
        'newsId',
        'movieId'];

    /**
     * MovieNews constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movies()
    {
        return $this->belongsTo('App\Models\Movie', 'movieId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function news()
    {
        return $this->hasOne('App]Models\News', 'newsId');
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertMovieNews($inputData)
    {
        $newsId = $inputData['newsId'];
        $movieId = $inputData['movieId'];
        $existingNews = $this::where('newsId', '=', $newsId)->first();
        if ($existingNews != null) {
            return false;
        } else {
            $newNews = new MovieNews();
            $newNews->newsId = $newsId;
            $newNews->movieId = $movieId;
            $newNews->save();
            return true;
        }
    }


}