<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 1:20 PM
 */

namespace App\Models;


use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\DataRetrievers\interfaces\TVSeriesDataAttributes;
use App\Http\DateTimeConverters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TVSeries extends Model implements TVSeriesDataAttributes
{
    public $timestamps = false;
    protected $table = 'TV_SERIES';
    protected $primaryKey = 'tvSeriesId';
    protected $guarded = [
        self::tvSeriesId,
        self::seriesName,
        self::imageName
    ];

    protected $fillable = [
        self::startYear,
        self::endYear,
        self::duration,
        self::maturity,
        self::network,
        self::summary,
        self::story
    ];

    private $tvSeriesName = null;

    /**
     * TVSeries constructor.
     * @param null $tvSeriesName
     */
    public function __construct($tvSeriesName = null)
    {
        $this->tvSeriesName = $tvSeriesName;
    }

    public static function searchResultMovieList($tvSeriesIdList)
    {
        $tvSeriesList = DB::table('tv_series')
            ->leftJoin('tv_feedbacks', 'tv_series.tvSeriesId', '=', 'tv_feedbacks.tvSeriesId')
            ->select(DB::raw(
                'tv_series.seriesName,
                tv_series.genre,
                tv_series.summary,
                tv_series.imageName,
                tv_series.startYear,
                tv_series.endYear,
                avg(tv_feedbacks.rating) as rate'))
            ->whereIn('tv_series.tvSeriesId', $tvSeriesIdList)
            ->groupBy('tv_Series.tvSeriesId')
            ->orderBy('tv_feedbacks.rating', 'DESC')
            ->orderBy('tv_series.seriesName', 'ASC')
            ->orderBy('tv_series.startYear', 'DESC')
            ->get();
        return $tvSeriesList;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tvSeriesSeasons()
    {
        return $this->hasMany('App\Models\TVSeriesSeason', 'tvSeriesId');
    }

    /**
     * @return $this
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User',
            'TV_Feedbacks', 'tvSeriesId', 'userId')
            ->withPivot(
                'review',
                'rating',
                'watchlist',
                'feedbackDate');
    }

    /**
     * @return $this
     */
    public function profiles()
    {
        return $this->belongsToMany('App\Models\Profile', 'Casts_And_Crews_TVS',
            'tvSeriesId', 'profileId')->withPivot('role');
    }

    /**
     * @return null
     */
    public function getTVSeriesInfo()
    {
        $tvSeriesInfo = null;
        if ($this->tvSeriesName != null) {
            $tvSeriesInfo = $this::where(self::seriesName, '=', $this->tvSeriesName)->first();
        }
        return $tvSeriesInfo;
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertTVSeriesData($inputData)
    {
        $seriesName = ucwords($inputData['seriesName']);
        $startYear = $inputData['startYear'];
        $endYear = AdminPageController::checkNullValue($inputData['endYear']);
        $duration = AdminPageController::checkNullValue($inputData['duration']);
        $summary = $inputData['summary'];
        $network = ucwords($inputData['network']);
        $story = $inputData['story'];
        $genre = $inputData['genre'];
        $maturity = $inputData['maturity'];
        $existingTVSeriesData = $this::where($this::seriesName, '=', $seriesName)
            ->where($this::startYear, '=', $startYear)
            ->where($this::network, '=', $network)
            ->first();
        if ($existingTVSeriesData != null) {
            return false;
        } else {
            $newTVSeries = new TVSeries();
            $newTVSeries->seriesName = $seriesName;
            $newTVSeries->startYear = $startYear;
            $newTVSeries->endYear = $endYear;
            $newTVSeries->duration = $duration;
            $newTVSeries->summary = $summary;
            $newTVSeries->network = $network;
            $newTVSeries->story = $story;
            $newTVSeries->maturity = $maturity;
            $newTVSeries->genre = $genre;
            if (isset($inputData['imageName'])) {
                $tvSeriesImage = $inputData['imageName'];
                $imageFileName = camel_case($seriesName) . '.'
                    . $tvSeriesImage->getClientOriginalExtension();
                $newTVSeries->imageName = $imageFileName;
                AdminPageController::storeImage('tv-series', $tvSeriesImage, $imageFileName);
            }
            $newTVSeries->save();
            return true;
        }
    }

    public function getTopRatedTVSeries()
    {

        $topRankedTVSeries = DB::table('tv_series')
            ->join('tv_feedbacks', 'tv_series.tvSeriesId', '=', 'tv_feedbacks.tvSeriesId')
            ->select(DB::raw(
                'tv_series.seriesName,
                tv_series.genre,
                tv_series.summary,
                tv_series.imageName,
                tv_series.startYear,
                tv_series.endYear,
                avg(tv_feedbacks.rating) as rate'))
            ->groupBy('tv_Series.tvSeriesId')
            ->orderBy('tv_feedbacks.rating', 'DESC')
            ->orderBy('tv_series.seriesName', 'ASC')
            ->orderBy('tv_series.startYear', 'DESC')
            ->get();

        return $topRankedTVSeries;
    }

    public function getNewTVSeries()
    {
        $subQuery = DB::table('TV_SERIES_SEASONS')
            ->select('tvSeriesId')
            ->where('startYear', '=', DateTimeConverters::getYear())
            ->get();
        $newTVSeries = DB::table('tv_series')
            ->leftJoin('tv_feedbacks', 'tv_series.tvSeriesId', '=', 'tv_feedbacks.tvSeriesId')
            ->select(DB::raw(
                'tv_series.seriesName,
                tv_series.genre,
                tv_series.summary,
                tv_series.imageName,
                tv_series.startYear,
                tv_series.endYear,
                avg(tv_feedbacks.rating) as rate'))
            ->whereIn('tv_series.tvSeriesId', $subQuery)
            ->groupBy('tv_Series.tvSeriesId')
            ->orderBy('tv_feedbacks.rating', 'DESC')
            ->orderBy('tv_series.seriesName', 'ASC')
            ->orderBy('tv_series.startYear', 'DESC')
            ->get();
        return $newTVSeries;
    }

    public function getUpComingTVSeries()
    {
        $tvSeriesSeason = new TVSeriesSeason();
        $seasonIdList = $tvSeriesSeason->getUpcomingTVSeasons();
        $upComingTVSeries = DB::table('tv_series')
            ->join('tv_series_seasons', 'tv_series.tvSeriesId', '=', 'tv_series_seasons.tvSeriesId')
            ->leftJoin('tv_feedbacks', 'tv_series.tvSeriesId', '=', 'tv_feedbacks.tvSeriesId')
            ->select(DB::raw(
                'tv_series.seriesName,
                tv_series.genre,
                tv_series.summary,
                tv_series.imageName,
                tv_series.startYear,
                tv_series.endYear,
                avg(tv_feedbacks.rating) as rate'))
            ->whereIn('tv_series_seasons.tvSeriesId', $seasonIdList)
            ->groupBy('tv_Series.tvSeriesId')
            ->orderBy('tv_feedbacks.rating', 'DESC')
            ->orderBy('tv_series.seriesName', 'ASC')
            ->orderBy('tv_series.startYear', 'DESC')
            ->get();
        return $upComingTVSeries;
    }


}