<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 7:29 PM
 */

namespace App\Models;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TVFeedback extends Feedback
{
    protected $table = 'TV_FEEDBACKS';
    protected $guarded = ['tvSeriesId', 'userId'];
    private $tvSeriesId = null;

    /**
     * TVFeedback constructor.
     * @param null $tvSeriesId
     */
    public function __construct($tvSeriesId = null)
    {
        $this->tvSeriesId = $tvSeriesId;
    }


    /**
     * @return array|null
     */
    public function getUserTVFeedBackInfo()
    {
        $feedbackData = null;
        if (Session::has('userId') && Session::get('userId') != null) {
            $feedBack = $this::where('tvSeriesId', '=', $this->tvSeriesId)
                ->where(Feedback::userIdDB, Session::get('userId'))
                ->first();
            if ($feedBack != null) {
                $feedbackData = [
                    'userRating' => $feedBack->rating,
                    'userReview' => $feedBack->review,
                    'userWatched' => $feedBack->watchlist,
                    'feedbackDate' => $feedBack->feedbackDate
                ];
            }
        }
        return $feedbackData;
    }

    /**
     * @return array
     */
    public function getRating()
    {
        $feedback = DB::table('TV_FEEDBACKS')
            ->select(DB::raw
            ('count(userId) as totalFeedback, avg(rating) as rating'))
            ->where('tvSeriesId', $this->tvSeriesId)
            ->first();
        return $data = [
            'totalUser' => $feedback['totalFeedback'],
            'rate' => round($feedback['rating'], 1)
        ];
    }

    /**
     * @return array|null
     */
    public function getAllReviews()
    {
        $data = null;
        $tvSeries = TVSeries::find($this->tvSeriesId);
        $userList = $tvSeries->users;
        if ($userList != null && count($userList) > 0) {
            $data = [];
            for ($i = 0; $i < count($userList); $i++) {
                $user = $userList[$i];
                if ($user->pivot->review != null) {
                    $data[$i] = [
                        'userName' => $user->userName,
                        'review' => $user->pivot->review
                    ];
                }
            }
        }
        return $data;
    }

    /**
     * @param $userName
     * @return mixed
     */
    public function getUserTVSeriesWatchList($userName)
    {
        $subQuery = DB::table('tv_feedbacks')
            ->join('users', 'tv_feedbacks.userId', '=', 'users.userId')
            ->where('tv_feedbacks.watchlist', '=', 1)
            ->where('users.userName', '=', (string)$userName)
            ->select('tv_feedbacks.tvSeriesId')
            ->get();
        $movieWatchList = DB::table('tv_series')
            ->join('tv_feedbacks', 'tv_series.tvSeriesId', '=', 'tv_feedbacks.movieId')
            ->whereIn('tv_feedbacks.tvSeriesId', $subQuery)
            ->select(DB::raw(
                'tv_series.seriesName,
                tv_series.genre,
                tv_series.summary,
                tv_series.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                avg(tv_feedbacks.rating) as rate'))
            ->groupBy('tv_feedbacks.movieId')
            ->orderBy('tv_feedbacks.rating', 'DESC')
            ->orderBy('tv_series.startYear', 'ASC')
            ->orderBy('tv_series.seriesName', 'ASC')
            ->get();
        return $movieWatchList;
    }

    /**
     * @param $userName
     * @return mixed
     */
    public function getUserRatingsList($userName)
    {
        $user = User::where(self::userNameDB, (string)$userName)->first();
        $userRatedMovieList = $user->tvSeries()->select(DB::raw(
            'tv_series.seriesName,
            tv_series.imageName,
            tv_series.startYear as startYear,
            tv_series.endYear as endYear,
            tv_series.summary,
            tv_feedbacks.rating as rate'))
            ->orderBy('tv_feedbacks.rating', 'DESC')
            ->orderBy('tv_series.startYear', 'ASC')
            ->orderBy('tv_series.seriesName', 'ASC')
            ->get()
            ->toArray();
        return $userRatedMovieList;
    }

    /**
     * @param $userName
     */
    public function getUserReviewsList($userName)
    {
        $user = User::where(self::userNameDB, (string)$userName)->first();
        $userRatedTVSeriesList = $user->tvSeries()->select(DB::raw(
            'tv_series.seriesName,
            tv_series.imageName,
            tv_series.startYear as startYear,
            tv_series.endYear as endYear,
            tv_feedbacks.review,
            DATE_FORMAT(tv_feedbacks.feedbackDate,\'%d %M %Y\') as feedbackDate,
            tv_feedbacks.rating as rate'))
            ->orderBy('tv_feedbacks.rating', 'DESC')
            ->orderBy('tv_series.startYear', 'ASC')
            ->orderBy('tv_series.seriesName', 'ASC')
            ->get()
            ->toArray();
        return $userRatedTVSeriesList;
    }
}