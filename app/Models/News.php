<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/18/2015
 * Time: 8:28 PM
 */

namespace App\Models;


use App\Http\Controllers\DataRetrievers\interfaces\NewsDataAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class News extends Model implements NewsDataAttributes
{
    public $timestamps = false;
    protected $table = 'NEWS';
    protected $primaryKey = 'newsId';
    protected $guarded = ['newsId'];
    protected $fillable = [
        'newsType',
        'headLine',
        'news',
        'uploadTime'
    ];


    /**
     * News constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movieNews()
    {
        return $this->belongsTo('App\Models\MovieNews', 'newsId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profileNews()
    {
        return $this->belongsTo('App\Models\ProfileNews', 'newsId');
    }

    /**
     * @return null
     */
    public function getMovieNews()
    {
        $movieNews = null;
        $movieNews = DB::table('Movies')
            ->join('Movie_News', 'Movies.movieId', '=', 'Movie_News.movieId')
            ->join('News', 'Movie_News.newsId', '=', 'News.newsId')
            ->where('News.newsType', '=', 'Movie')
            ->select(
                'Movies.imageName',
                'Movies.movieName',
                'News.headLine',
                'News.news',
                'News.newsType',
                'News.uploadTime')
            ->orderBy('News.uploadTime', 'DESC')
            ->orderBy('Movies.movieName')
            ->get();
        return $movieNews;
    }

    /**
     * @return null
     */
    public function getProfileNews()
    {
        $movieNews = null;
        $movieNews = DB::table('Profiles')
            ->join('Profile_News', 'Profiles.profileId', '=', 'Profile_News.profileId')
            ->join('News', 'Profile_News.newsId', '=', 'News.newsId')
            ->where('News.newsType', '=', 'Profile')
            ->select(
                'Profiles.profileImage',
                'Profiles.profession',
                'Profiles.name',
                'News.headLine',
                'News.news',
                'News.newsType',
                'News.uploadTime')
            ->orderBy('News.uploadTime', "DESC")
            ->orderBy('Profiles.name')
            ->get();
        return $movieNews;
    }

    /**
     * @return array|null
     */
    public function getTopNews()
    {
        $latestNews = null;
        $topNews = $this::orderBy($this::uploadTime, 'DESC')->get();
        if ($topNews != null && count($topNews) > 0) {
            $latestNews = [];
            foreach ($topNews as $news) {
                $newsType = $news['newsType'];
                $tmpNews = null;
                if ($newsType == 'Movie') {
                    $movieNews = $news->movieNews;
                    $movie = $movieNews->movies;
                    $tmpNews = [
                        'imageName' => $movie['imageName'],
                        'movieName' => $movie['movieName'],
                        'headLine' => $news['headLine'],
                        'news' => $news['news'],
                        'newsType' => $news['newsType'],
                        'uploadTime' => $news['uploadTime']
                    ];
                } else if ($newsType == 'Profile') {
                    $profileNews = $news->profileNews;
                    $profile = $profileNews->profiles;
                    $tmpNews = [
                        'profileImage' => $profile['profileImage'],
                        'profession' => $profile['profession'],
                        'name' => $profile['name'],
                        'headLine' => $news['headLine'],
                        'news' => $news['news'],
                        'newsType' => $news['newsType'],
                        'uploadTime' => $news['uploadTime']
                    ];
                }
                array_push($latestNews, $tmpNews);
            }
        }
        return $latestNews;
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertNews($inputData)
    {
        $newsType = $inputData['newsType'];
        $headLine = $inputData['headLine'];
        $news = $inputData['news'];
        $uploadTime = $inputData['uploadTime'];
        $existingNewsData = $this::where('newsType', '=', $newsType)
            ->where('headLine', '=', $headLine)
            ->first();
        if ($existingNewsData != null) {
            $existingNewsData->news = $news;
            $existingNewsData->uploadTime = $uploadTime;
            $existingNewsData->save();
            return true;
        } else {
            $newNews = new News();
            $newNews->newsType = $newsType;
            $newNews->headLine = $headLine;
            $newNews->news = $news;
            $newNews->uploadTime = $uploadTime;
            $newNews->save();
            return true;
        }
    }


}