<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/18/2015
 * Time: 9:10 AM
 */

namespace App\Models;


use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use App\Http\Controllers\DataRetrievers\interfaces\TrailerDataAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Trailer extends Model implements TrailerDataAttributes, MovieDataAttributes
{
    public $timestamps = false;
    protected $table = 'Trailers';
    protected $primaryKey = 'trailerId';
    protected $fillable = [
        'trailerNo',
        'trailerCode',
        'uploadDate',
        'viewCount'
    ];


    protected $guarded = ['trailerId', 'movieId'];
    private $movieName = null;

    /**
     * Trailer constructor.
     * @param null $movieName
     */
    public function __construct($movieName = null)
    {
        if ($movieName != null) {
            $this->movieName = $movieName;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movies()
    {
        return $this->belongsTo('App\Models\Movie', 'movieId');
    }

    /**
     * @return array|null
     */
    public function getRecentlyAddedTrailers()
    {
        $trailerList = self::orderBy(self::uploadDate, 'desc')->limit(10)->get();
        $data = null;
        if ($trailerList != null && count($trailerList) > 0) {
            $data = [];
            for ($i = 0; $i < count($trailerList); $i++) {
                $trailer = $trailerList[$i];
                $movie = $trailer->movies;
                if ($movie != null) {
                    $data[$i] = [
                        'movieName' => $movie->movieName,
                        'imageName' => $movie->imageName,
                        'trailerCode' => $trailer->trailerCode,
                        'uploadDate' => $trailer->uploadDate
                    ];
                }
            }
        }
        return $data;
    }


    /**
     * @param $movieData
     * @return bool
     */
    public function hasTrailer($movieData)
    {
        $trailers = $this->getTrailer($movieData);
        if ($trailers != null && count($trailers) > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function getTrailer($movie)
    {
        $trailerList = $movie->trailers()
            ->orderBy($this::trailerNo, 'DESC')
            ->get();
        return $trailerList;
    }


    /**
     * @param $trailerId
     */
    public function increaseViewCount($trailerId)
    {
        $trailer = self::where(self::trailerId, $trailerId)->first();
        if ($trailer != null) {
            $trailer['viewCount'] += 1;
            $trailer->save();
        }
    }

    /**
     * @return mixed
     */
    public function getPopularTrailerList()
    {
        $popularTrailer = DB::table('movies')
            ->join('trailers', 'movies.movieId', '=', 'trailers.movieId')
            ->select('movies.movieName', 'movies.imageName')
            ->orderBy('trailers.viewCount', 'DESC')
            ->get();

        return $popularTrailer;
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertMovieTrailerData($inputData)
    {
        $movieId = $inputData['movieId'];
        $trailerNo = $inputData['trailerNo'];
        $trailerCode = $inputData['trailerCode'];
        $uploadDate = $inputData['uploadDate'];
        $existTrailerData = $this::where(self::trailerCode, '=', $trailerCode)->first();
        if ($existTrailerData != null) {
            return false;
        } else {
            $newTrailer = new Trailer();
            $newTrailer->movieId = $movieId;
            $newTrailer->trailerNo = $trailerNo;
            $newTrailer->trailerCode = $trailerCode;
            $newTrailer->uploadDate = $uploadDate;
            $newTrailer->save();
            return true;
        }
    }
}