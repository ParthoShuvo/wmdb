<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/4/2015
 * Time: 8:31 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NowShowing extends Model
{
    public $timestamps = false;
    protected $table = 'NOW_SHOWINGS';
    protected $guarded = [
        'showId',
        'movieId',
        'theaterId'
    ];
    protected $fillable = ['active'];

    /**
     * NowShowing constructor.
     */
    public function __construct()
    {
    }


    /**
     * @return mixed
     */
    public function getNowShowingMoviesInTheaters()
    {
        $nowShowingMoviesInTheaters = DB::table('Now_showings')
            ->distinct()
            ->select('movieId')
            ->where('active', '=', 1)
            ->get();
        return $nowShowingMoviesInTheaters;
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertNowShowingMoviesInTheater($inputData)
    {
        $movieId = $inputData['movieId'];
        $theaterId = $inputData['theaterId'];
        $active = $inputData['active'];
        $movieData = Movie::where('movieId', '=', $movieId)->first();
        $existingNowShowingMovieInTheater = $movieData->theaters()
            ->find($theaterId);
        if ($existingNowShowingMovieInTheater != null) {
            $existingNowShowingMovieInTheater->pivot->active = (int)$active;
            $existingNowShowingMovieInTheater->pivot->save();
            dd($existingNowShowingMovieInTheater);
            return true;
        } else {
            $newNowShowingMovieInTheater = new NowShowing();
            $newNowShowingMovieInTheater->movieId = $movieId;
            $newNowShowingMovieInTheater->theaterId = $theaterId;
            $newNowShowingMovieInTheater->active = (int)$active;
            $newNowShowingMovieInTheater->save();
            return true;
        }
    }
}