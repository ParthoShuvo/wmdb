<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/4/2015
 * Time: 1:09 AM
 */

namespace App\Models;


use Illuminate\Support\Facades\DB;

class ProfileAwardsMovies extends MovieAward
{
    protected $table = 'PROFILE_AWARDS_MOVIES';
    public $timestamps = false;
    protected $guarded = [
        'movieId',
        'profileId'
    ];


    /**
     * ProfileAwardsMovies constructor.
     */
    public function __construct($movieId = null)
    {
        parent::__construct($movieId);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function castAndCrews()
    {
        return $this->belongsTo('App\Models\CastAndCrew', [
            'movieId',
            'profileId']);
    }

    /**
     * @return null
     */
    public function getMovieProfileAwardsAndNominations()
    {
        $awardList = $this::where('movieId', '=', parent::getMovieId())->get();
        $data = null;
        if ($awardList != null && count($awardList) > 0) {
            $data = parent::fetchDataFromAwardList($awardList);
        }
        return $data;
    }

    /**
     * @param $profileData
     * @return array|null
     */
    public function getProfileAwardsAndNominations($profileData)
    {
        $movieAwardData = DB::table('movies')
            ->join('profile_awards_movies', 'movies.movieId', '=', 'profile_awards_movies.movieId')
            ->where('profile_awards_movies.profileId', '=', $profileData['profileId'])
            ->select(
                'movies.movieName',
                'profile_awards_movies.awardProvider',
                'profile_awards_movies.year',
                'profile_awards_movies.nomination',
                'award')
            ->orderBy('year', 'ASC')
            ->orderBy('awardProvider', 'ASC')
            ->get();
        $data = null;
        if ($movieAwardData != null && count($movieAwardData) > 0) {
            $data = [];
            for ($i = 0; $i < count($movieAwardData); $i++) {
                $movie = $movieAwardData[$i];
                $data[$i] = [
                    'movieName' => $movie['movieName'],
                    'awardName' => $movie['awardProvider'],
                    'awardYear' => '(' . $movie['year'] . ')',
                    'awardCategory' => parent::checkForAwardWin(
                        $movie['nomination'],
                        $movie['award'])
                ];
            }
        }
        return $data;
    }


    /**
     * @param $inputData
     * @return bool
     */
    public function insertProfileMovieAward($inputData)
    {
        $movieId = $inputData['movieId'];
        $profileId = $inputData['profileId'];
        $nomination = $inputData['nomination'];
        $awardProvider = $inputData['awardProvider'];
        $year = $inputData['year'];
        $award = $inputData['award'];
        $existingProfileMovieAwardData = ProfileAwardsMovies::where('movieId', '=', $movieId)
            ->where('profileId', '=', $profileId)
            ->where('awardProvider', '=', $awardProvider)
            ->where('nomination', '=', $nomination)
            ->where('year', '=', $year)
            ->first();
        if ($existingProfileMovieAwardData != null) {
            $existingProfileMovieAwardData->award = $award;
            $existingProfileMovieAwardData->save();
            return true;
        } else {
            $newProfileMovieAward = new ProfileAwardsMovies();
            $newProfileMovieAward->movieId = $movieId;
            $newProfileMovieAward->profileId = $profileId;
            $newProfileMovieAward->nomination = $nomination;
            $newProfileMovieAward->awardProvider = $awardProvider;
            $newProfileMovieAward->year = $year;
            $newProfileMovieAward->award = $award;
            $newProfileMovieAward->save();
            return true;
        }
    }

}