<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 2:51 PM
 */

namespace App\Models;


use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\DataRetrievers\interfaces\TVSeriesEpisodesDataAttributes;
use App\Http\Controllers\DataRetrievers\interfaces\TVSeriesSeasonsDataAttributes;
use Illuminate\Database\Eloquent\Model;

class TVSeriesEpisode extends Model implements
    TVSeriesSeasonsDataAttributes, TVSeriesEpisodesDataAttributes
{
    public $timestamps = false;
    protected $table = 'TV_SERIES_EPISODES';
    protected $primaryKey = 'episodeId';
    protected $guarded = [
        self::episodeId,
        self::episodeName,
        self::episodeNo,
        self::seasonId,
    ];

    protected $fillable = [
        self::onAirDate,
        self::episodeImage,
        self::summary,
        self::summary
    ];

    /**
     * TVSeriesEpisode constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tvSeriesSeasons()
    {
        return $this->belongsTo('App\Models\TVSeriesSeason', 'seasonId');
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertTVSeriesEpisodes($inputData)
    {
        $tvSeriesId = $inputData['tvSeriesId'];
        $seasonNo = $inputData['seasonNo'];
        $episodeNo = $inputData['episodeNo'];
        $onAirDate = $inputData['onAirDate'];
        $episodeName = $inputData['episodeName'];
        $summary = $inputData['summary'];
        $story = AdminPageController::checkNullValue($inputData['story']);
        $episodeImageName = 'tvEpisode.jpg';
        $episodeImage = null;
        $tvSeriesSeason = new TVSeriesSeason();
        $existingTVSeriesSeasons = $tvSeriesSeason->getTVSeriesSeasonData($tvSeriesId, $seasonNo);
        if ($existingTVSeriesSeasons != null) {
            $seasonId = $existingTVSeriesSeasons->seasonId;
            $existingTVSeriesEpisode = $existingTVSeriesSeasons->tvSeriesEpisodes
                ->where($this::episodeNo, '=', $episodeNo)->first();
            if (isset($inputData['episodeImage'])) {
                $episodeImage = $inputData['episodeImage'];
                $episodeImageName = camel_case($episodeName . $seasonNo . $episodeNo) . '.'
                    . $episodeImage->getClientOriginalExtension();
                AdminPageController::storeImage('episode', $episodeImage, $episodeImageName);
            }
            if ($existingTVSeriesEpisode != null) {
                $existingTVSeriesEpisode->summary = $summary;
                $existingTVSeriesEpisode->story = $story;
                $existingTVSeriesEpisode->episodeImage = $episodeImage;
                $existingTVSeriesEpisode->onAirDate = $onAirDate;
                $existingTVSeriesEpisode->save();
                return true;
            } else {
                $newEpisode = new TVSeriesEpisode();
                $newEpisode->seasonId = $seasonId;
                $newEpisode->episodeNo = $episodeNo;
                $newEpisode->episodeName = $episodeName;
                $newEpisode->summary = $summary;
                $newEpisode->story = $story;
                $newEpisode->onAirDate = $onAirDate;
                $newEpisode->episodeImage = $episodeImageName;
                $newEpisode->save();
                return true;
            }
        }
        return false;
    }


    /**
     * @param $tvSeriesId
     * @param $seasonNo
     * @return mixed
     */



}