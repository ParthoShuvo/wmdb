<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/22/2015
 * Time: 2:15 AM
 */

namespace App\Models;


use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use App\Http\Controllers\DataRetrievers\interfaces\UserDataAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Feedback extends Model implements MovieDataAttributes, UserDataAttributes
{
    public $timestamps = false;
    protected $table = 'FEEDBACKS';
    protected $guarded = ['movieId', 'userId'];
    protected $fillable = [
        'rating',
        'review',
        'watchlist',
        'feedbackDate'
    ];
    private $movieId = null;


    /**
     * Feedback constructor.
     * @param array $movieId
     */
    public function __construct($movieId = null)
    {
        if ($movieId != null) {
            $this->movieId = $movieId;
        }
    }

    /**
     * @return array|null
     */
    public function getUserMovieFeedBackInfo()
    {
        $feedbackData = null;
        if (Session::has('userId') && Session::get('userId') != null) {
            $feedBack = $this::where(Feedback::movieId, $this->movieId)
                ->where(Feedback::userIdDB, Session::get('userId'))
                ->first();
            if ($feedBack != null) {
                $feedbackData = [
                    'userRating' => $feedBack->rating,
                    'userReview' => $feedBack->review,
                    'userWatched' => $feedBack->watchlist,
                    'feedbackDate' => $feedBack->feedbackDate
                ];
            }
        }
        return $feedbackData;
    }

    /**
     * @return array
     */
    public function getRating()
    {
        $feedback = DB::table('feedbacks')
            ->select(DB::raw
            ('count(userId) as totalFeedback, avg(rating) as rating'))
            ->where('movieId', $this->movieId)
            ->first();
        return $data = [
            'totalUser' => $feedback['totalFeedback'],
            'rate' => round($feedback['rating'], 1)
        ];
    }

    /**
     * @return array|null
     */
    public function getAllReviews()
    {
        $data = null;
        $movie = Movie::where($this::movieId, $this->movieId)->first();
        $userList = $movie->users;
        if ($userList != null && count($userList) > 0) {
            $data = [];
            for ($i = 0; $i < count($userList); $i++) {
                $user = $userList[$i];
                if ($user->pivot->review != null) {
                    $data[$i] = [
                        'userName' => $user->userName,
                        'review' => $user->pivot->review
                    ];
                }
            }
        }
        return $data;
    }

    /**
     * @param $userName
     * @return mixed
     */
    public function getUserMovieWatchList($userName)
    {
        $subQuery = DB::table('feedbacks')
            ->join('users', 'feedbacks.userId', '=', 'users.userId')
            ->where('feedbacks.watchlist', '=', 1)
            ->where('users.userName', '=', (string)$userName)
            ->select('feedbacks.movieId')
            ->get();
        $movieWatchList = DB::table('movies')
            ->join('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->whereIn('feedbacks.movieId', $subQuery)
            ->select(DB::raw(
                'movies.movieName,
                movies.genre,
                movies.summary,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                avg(feedbacks.rating) as rate'))
            ->groupBy('feedbacks.movieId')
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.movieName', 'ASC')
            ->orderBy('releaseYear', 'DESC')
            ->get();
        return $movieWatchList;
    }

    /**
     * @param $userName
     * @return mixed
     */
    public function getUserRatingsList($userName)
    {
        $user = User::where(self::userNameDB, (string)$userName)->first();
        $userRatedMovieList = $user->movies()->select(DB::raw(
            'movies.movieName,
            movies.imageName,
            YEAR(movies.releaseDate) as releaseYear,
            movies.summary,
            feedbacks.rating as rate'))
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.releaseDate', 'ASC')
            ->orderBy('movies.movieName', 'ASC')
            ->get()
            ->toArray();
        return $userRatedMovieList;
    }

    /**
     * @param $userName
     */
    public function getUserReviewsList($userName)
    {
        $user = User::where(self::userNameDB, (string)$userName)->first();
        $userRatedMovieList = $user->movies()
            ->select(DB::raw(
            'movies.movieName,
            movies.imageName,
            YEAR(movies.releaseDate) as releaseYear,
            feedbacks.review,
            DATE_FORMAT(feedbacks.feedbackDate,\'%d %M %Y\') as feedbackDate,
            feedbacks.rating as rate'))
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.releaseDate', 'ASC')
            ->orderBy('movies.movieName', 'ASC')
            ->get()
            ->toArray();
        return $userRatedMovieList;
    }
}