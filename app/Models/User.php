<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    public static $rules = [
        'firstName' => 'required|max:30',
        'lastName' => 'required|max:30',
        'email' => 'required|max:50|email|unique:users',
        'cityName' => 'required|max:30',
        'userName' => 'required|between:8,30|unique:users',
        'password' => 'required|min:8',
        'confirmPassword' => 'required|min:8|same:password',
        'birthDate' => 'required'
    ];
    public static $messages = [
        'required' => 'all the fields is required',
        'same' => 'password and confirm password mismatch',
        'unique' => ' already registered'
    ];
    public $timestamps = false;
    protected $table = 'USERS';
    protected $primaryKey = 'userId';
    protected $hidden = ['password'];
    protected $guarded = ['userId'];

    public function movies()
    {
        return $this->belongsToMany('App\Models\Movie', 'Feedbacks',
            'userId', 'movieId')
            ->withPivot(
                'review',
                'rating',
                'watchlist',
                'feedbackDate');
    }

    public function tvSeries()
    {
        return $this->belongsToMany('App\Models\TVSeries', 'TV_Feedbacks',
            'userId', 'tvSeriesId')
            ->withPivot(
                'review',
                'rating',
                'watchlist',
                'feedbackDate');
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        // TODO: Implement getAuthIdentifier() method.
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        // TODO: Implement getAuthPassword() method.
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }


}
