<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/20/2015
 * Time: 2:21 AM
 */

namespace App\Models;


use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use Illuminate\Database\Eloquent\Model;

class MovieAward extends Model implements MovieDataAttributes
{
    public $timestamps = false;
    protected $table = 'MOVIE_AWARDS';
    protected $primaryKey = 'awardId';
    protected $fillable = [
        'nomination',
        'year',
        'awardProvider',
        'award'
    ];

    protected $guarded = ['awardId', 'movieId'];

    /**
     * @param null $movieId
     */
    public function __construct($movieId = null)
    {
        if ($movieId != null) {
            $this->movieId = $movieId;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movies()
    {
        return $this->belongsTo('App\Models\Movie', 'movieId');
    }

    /**
     * @return array
     */
    public function getAwardsAndNominations()
    {
        $movie = Movie::where($this::movieId, $this->movieId)->first();
        $awardList = $movie->movieAwards;
        $data = null;
        if ($awardList != null && count($awardList) > 0) {
            $data = $this->fetchDataFromAwardList($awardList);
        }
        return $data;
    }

    /**
     * @param $nomination
     * @param $award
     * @return string
     */
    public function checkForAwardWin($nomination, $award)
    {
        if ($award) {
            return 'won ' . $nomination;
        } else {
            return 'nominated for ' . $nomination;
        }
    }

    /**
     * @param $awardList
     * @return array
     */
    public function fetchDataFromAwardList($awardList)
    {
        $data = [];
        if ($awardList != null && count($awardList) > 0) {
            for ($i = 0; $i < count($awardList); $i++) {
                $award = $awardList[$i];
                $data[$i] = [
                    'awardName' => $award->awardProvider,
                    'awardYear' => '(' . $award->year . ')',
                    'awardCategory' => $this->checkForAwardWin(
                        $award->nomination,
                        $award->award)
                ];
            }
        }
        return $data;
    }

    /**
     * @return null
     */
    public function getMovieId()
    {
        return $this->movieId;
    }

    /**
     * @param null $movieId
     */
    public function setMovieId($movieId)
    {
        $this->movieId = $movieId;
    }


    /**
     * @param $inputData
     * @return bool
     */
    public function insertMovieAwardData($inputData)
    {
        $movieId = $inputData['movieId'];
        $nomination = $inputData['nomination'];
        $awardProvider = $inputData['awardProvider'];
        $year = $inputData['year'];
        $award = $inputData['award'];
        $movieData = Movie::where('movieId', '=', $movieId)->first();
        $existingMovieAwardData = $movieData->movieAwards
            ->where('awardProvider', '=', $awardProvider)
            ->where('nomination', '=', $nomination)
            ->where('year', '=', $year)
            ->first();
        if ($existingMovieAwardData != null) {
            $existingMovieAwardData->award = $award;
            $existingMovieAwardData->save();
            return true;
        }
        else{
            $newMovieAward = new MovieAward();
            $newMovieAward->movieId = $movieId;
            $newMovieAward->nomination = $nomination;
            $newMovieAward->awardProvider = $awardProvider;
            $newMovieAward->year = $year;
            $newMovieAward->award = $award;
            $newMovieAward->save();
            return true;
        }
    }
}