<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/19/2015
 * Time: 4:19 AM
 */

namespace App\Models;


use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use App\Http\Controllers\DataRetrievers\interfaces\ProfileDataAttributes;
use Illuminate\Database\Eloquent\Model;

class CastAndCrew extends Model implements MovieDataAttributes, ProfileDataAttributes
{
    public $timestamps = false;
    protected $table = 'Casts_and_Crews';
    protected $fillable = ['role'];
    protected $guarded = ['movieId', 'profileId'];
    private $movie = null;

    /**
     * @param null $movieId
     * @param null $profileId
     */
    public function __construct($movieId = null, $profileId = null)
    {
        if ($movieId == null) {
            $this->profileId = $profileId;
        } else if ($profileId == null) {
            $this->movieId = $movieId;
            $this->movie = Movie::where($this::movieId, $this->movieId)->first();
        }
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function castAndCrewAwards()
    {
        return $this->hasMany('App\Models\ProfileAwardsMovies', [
            'movieId',
            'profileId']);
    }


    /**
     * @return array|null
     */
    public function getActorList()
    {
        $data = null;
        $profileList = $this->getCastAndCrewData('Actor');
        if ($profileList != null && count($profileList) > 0) {
            $data = [];
            for ($i = 0; $i < count($profileList); $i++) {
                $profile = $profileList[$i];
                $data[$i] = [
                    'actorName' => $profile->name,
                    'image' => $profile->profileImage,
                    'role' => $profile->pivot->role
                ];
            }
        }
        return $data;
    }

    /**
     * @param $profession
     * @return mixed
     */
    public function getCastAndCrewData($profession)
    {
        $profileList = $this->movie->profiles
            ->where($this::profession, ucfirst($profession));
        return $profileList;
    }

    /**
     * @return array|null
     */
    public function getDirectorList()
    {
        $data = null;
        $profileList = $this->movie->profiles;
        if ($profileList != null && count($profileList) > 0) {
            $data = [];
            for ($j = 0; $j < count($profileList); $j++) {
                $profile = $profileList[$j];
                if ($profile->profession == 'Director') {
                    $data[$j] = [
                        'directorName' => $profile->name,
                        'image' => $profile->image,
                        'directorPosition' => $profile->pivot->role
                    ];
                }
            }
        }
        return $data;
    }

    public function insertMovieCastAndCrewData($inputData)
    {
        $movieId = $inputData['movieId'];
        $profileId = $inputData['profileId'];
        $role = $inputData['role'];
        $movieData = Movie::where('movieId', '=', $movieId)->first();
        $profileData = $movieData->profiles->where('profileId', '=', $profileId)->first();
        if ($profileData != null) {
            $profileData->pivot->role = $role;
            $profileData->pivot->save();
            return true;
        } else {
            $newMovieCastCrewData = new CastAndCrew();
            $newMovieCastCrewData->movieId = $movieId;
            $newMovieCastCrewData->profileId = $profileId;
            $newMovieCastCrewData->role = $role;
            $newMovieCastCrewData->save();
            return true;
        }
    }


}