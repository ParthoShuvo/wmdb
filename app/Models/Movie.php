<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/18/2015
 * Time: 12:33 AM
 */

namespace App\Models;


use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use App\Http\DateTimeConverters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Movie extends Model implements MovieDataAttributes
{
    public $timestamps = false;
    protected $table = 'MOVIES';
    protected $primaryKey = 'movieId';
    protected $guarded = ['movieId'];
    protected $fillable = [
        'movieName',
        'releaseDate',
        'genre',
        'duration',
        'maturity',
        'summary',
        'story',
        'imageName'
    ];

    /**
     * @param null $movieName
     */
    public function __construct($movieName = null)
    {
        if ($movieName != null) {
            $this->movieName = $movieName;
        }
    }

    /**
     * @param $movieIdList
     * @return mixed
     */
    public static function searchResultMovieList($movieIdList)
    {
        $movieList = DB::table('movies')
            ->leftJoin('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->select(DB::raw(
                'Distinct movies.movieId,
                movies.movieName,
                movies.genre,
                movies.summary,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                avg(feedbacks.rating) as rate'))
            ->whereIn('movies.movieId', $movieIdList)
            ->groupBy('movies.movieId')
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.movieName', 'ASC')
            ->orderBy('releaseYear', 'DESC')
            ->get();
        return $movieList;
    }

    /**
     * @return null
     */
    public function getMovieData()
    {
        $movieData = null;
        if ($this->movieName != null) {
            $movieData = $this::where($this::movieName, '=', $this->movieName)->first();
        }
        return $movieData;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trailers()
    {
        return $this->hasMany('App\Models\Trailer', 'movieId');
    }

    /**
     * @return $this
     */
    public function profiles()
    {
        return $this->belongsToMany('App\Models\Profile', 'Casts_And_Crews',
            'movieId', 'profileId')->withPivot('role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function boxOffices()
    {
        return $this->hasOne('App\Models\BoxOffice', 'movieId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function movieAwards()
    {
        return $this->hasMany('App\Models\MovieAward', 'movieId');
    }

    /**
     * @return $this
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User',
            'Feedbacks', 'movieId', 'userId')
            ->withPivot(
                'review',
                'rating',
                'watchlist',
                'feedbackDate');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function theaters()
    {
        return $this->belongsToMany(
            'App\Models\Theater',
            'Now_Showings',
            'movieId',
            'theaterId'
        )->withPivot('active');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news()
    {
        return $this->hasMany('App\Models\MovieNews', 'movieId');
    }

    /**
     * @return mixed
     */
    public function getTopRankedMovies()
    {

        $topRankedMovies = DB::table('movies')
            ->join('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->select(DB::raw(
                'movies.movieName,
                movies.genre,
                movies.summary,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                avg(feedbacks.rating) as rate'))
            ->groupBy('feedbacks.movieId')
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.movieName', 'ASC')
            ->orderBy('releaseYear', 'DESC')
            ->get();

        return $topRankedMovies;
    }

    /**
     * @param $movieCategory
     * @return mixed
     */
    public function getCategoryWiseMoviesListData($movieCategory)
    {
        $movieList = DB::table('movies')
            ->leftJoin('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->select(DB::raw(
                'movies.movieId,
                movies.movieName,
                movies.genre,
                movies.summary,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                avg(feedbacks.rating) as rate'))
            ->where('movies.genre', '=', $movieCategory)
            ->groupBy('movies.movieId')
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.movieName', 'ASC')
            ->orderBy('releaseYear', 'DESC')
            ->get();
        return $movieList;
    }

    /**
     * @return NowShowing
     */
    public function getNowShowingMovies()
    {
        $nowShowingMovies = new NowShowing();
        $inTheaters = $nowShowingMovies->getNowShowingMoviesInTheaters();
        $nowShowingMoviesData = DB::table('movies')
            ->leftJoin('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->leftJoin('BoxOffices', 'movies.movieId', '=', 'BoxOffices.movieId')
            ->select(DB::raw(
                'movies.movieName,
                movies.summary,
                movies.imageName,
                BoxOffices.gross,
                YEAR(movies.releaseDate) as releaseYear,
                avg(feedbacks.rating) as rate'))
            ->whereIn('movies.movieId', $inTheaters)
            ->groupBy('movies.movieId')
            ->orderBy('BoxOffices.gross', 'DESC')
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.movieName', 'ASC')
            ->orderBy('releaseYear', 'DESC')
            ->get();
        return $nowShowingMoviesData;
    }

    /**
     * @return mixed
     */
    public function getWeeklyBoxOfficeResults()
    {
        $nowShowingMovies = new NowShowing();
        $inTheaters = $nowShowingMovies->getNowShowingMoviesInTheaters();
        $nowShowingMoviesData = DB::table('movies')
            ->leftJoin('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->leftJoin('BoxOffices', 'movies.movieId', '=', 'BoxOffices.movieId')
            ->select(DB::raw(
                'movies.movieName,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                BoxOffices.weekendCollection/1000000 as weekendCollection,
                BoxOffices.gross/1000000 as gross,
                CEIL(DATEDIFF(CURDATE(),movies.releaseDate)/7) as week,
                avg(feedbacks.rating) as rate'
            ))
            ->whereIn('movies.movieId', $inTheaters)
            ->groupBy('movies.movieId')
            ->orderBy('BoxOffices.weekendCollection', 'DESC')
            ->orderBy('BoxOffices.gross', 'DESC')
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('movies.movieName', 'ASC')
            ->orderBy('week', 'ASC')
            ->limit(10)
            ->get();
        return $nowShowingMoviesData;
    }

    /**
     * @return mixed
     */
    public function getOpeningThisWeekMovieList()
    {
        $week = (int)DateTimeConverters::getWeekNumber();
        $year = (int)DateTimeConverters::getYear();
        $movieList = Movie::where(DB::raw('WEEK(movies.releaseDate, 3)'), '=', $week)
            ->where(DB::raw('YEAR(movies.releaseDate)'), '=', $year)
            ->get([$this::movieName, $this::imageName]);
        return $movieList;
    }

    /**
     * @return mixed
     */
    public function getUpcomingMovieList()
    {
        $date = date('Y-m-d');
        $week = (int)DateTimeConverters::getWeekNumber();
        $movieList = $movieList = DB::table('movies')
            ->leftJoin('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->select(DB::raw(
                'movies.movieName,
                movies.summary,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                avg(feedbacks.rating) as rate'))
            ->where('movies.ReleaseDate', '>', $date)
            ->where(DB::raw('WEEK(movies.releaseDate, 3)'), '<>', $week)
            ->groupBy('movies.movieId')
            ->orderBy('releaseDate', 'ASC')
            ->orderBy('movies.movieName', 'ASC')
            ->get();
        return $movieList;
    }

    /**
     * @return mixed
     */
    public function getUpComingMoviesReleaseDate()
    {
        $week = (int)DateTimeConverters::getWeekNumber();
        $date = date('Y-m-d');
        $releaseDates = $this::where($this::releaseDate, '>', $date)
            ->where(DB::raw('WEEK(movies.releaseDate, 3)'), '<>', $week)
            ->distinct()
            ->orderBy($this::releaseDate)
            ->get([$this::releaseDate])
            ->toArray();
        return $releaseDates;
    }

    /**
     * @param $releaseDate
     * @return mixed
     */
    public function getMovieDataOnReleasedDate($releaseDate)
    {
        $movieList = DB::table('movies')
            ->leftJoin('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->select(DB::raw(
                'movies.movieName,
                movies.summary,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                avg(feedbacks.rating) as rate'))
            ->where('movies.ReleaseDate', '=', $releaseDate)
            ->groupBy('movies.movieId')
            ->orderBy('releaseDate', 'ASC')
            ->orderBy('movies.movieName', 'ASC')
            ->get();
        return $movieList;
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertMovieData($inputData)
    {
        $movieName = ucfirst($inputData['movieName']);
        $releaseYear = DateTimeConverters::getYear($inputData['releaseDate']);
        $existMovieData = $this::whereRaw('movieName = ? AND Year(releaseDate) = ?',
            [$movieName, $releaseYear])->first();
        if ($existMovieData != null) {
            return false;
        } else {
            $image = $inputData['imageName'];
            $imageFileName = $movieName . '.' . $inputData['imageName']->getClientOriginalExtension();
            $newMovie = new Movie();
            $newMovie->movieName = $movieName;
            $newMovie->genre = $inputData['genre'];
            $newMovie->maturity = AdminPageController::checkNullValue($inputData['maturity']);
            $newMovie->duration = AdminPageController::checkNullValue($inputData['duration']);
            $newMovie->releaseDate = $inputData['releaseDate'];
            $newMovie->summary = $inputData['summary'];
            $newMovie->story = AdminPageController::checkNullValue($inputData['story']);
            $newMovie->imageName = $imageFileName;
            AdminPageController::storeImage('movie', $image, $imageFileName);
            $newMovie->save();
            return true;
        }
    }

    /**
     * @return mixed
     */
    public function getAllTimeTopChartMovieList()
    {
        $allTimeTopChartMovieData = DB::table('movies')
            ->join('feedbacks', 'movies.movieId', '=', 'feedbacks.movieId')
            ->join('BoxOffices', 'movies.movieId', '=', 'BoxOffices.movieId')
            ->select(DB::raw(
                'movies.movieName,
                movies.imageName,
                YEAR(movies.releaseDate) as releaseYear,
                BoxOffices.weekendCollection/1000000 as weekendCollection,
                BoxOffices.gross/1000000 as gross,
                avg(feedbacks.rating) as rate'
            ))
            ->groupBy('movies.movieId')
            ->orderBy('BoxOffices.gross', 'DESC')
            ->orderBy('feedbacks.rating', 'DESC')
            ->orderBy('BoxOffices.weekendCollection', 'DESC')
            ->orderBy('movies.movieName', 'ASC')
            ->limit(10)
            ->get();
        return $allTimeTopChartMovieData;
    }

    public function getLatestTrailerReleasedMovieDetails()
    {
        $trailer = Trailer::orderBy('uploadDate', 'DESC')->first();
        $movie = $trailer->movies->movieName;
        return $movie;
    }
}