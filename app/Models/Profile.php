<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/19/2015
 * Time: 3:57 AM
 */

namespace App\Models;


use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\DataRetrievers\interfaces\ProfileDataAttributes;
use App\Http\DateTimeConverters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Profile extends Model implements ProfileDataAttributes
{
    public $timestamps = false;
    protected $table = 'Profiles';
    protected $primaryKey = 'profileId';
    protected $guarded = ['profileId'];
    protected $fillable = [
        'name',
        'birthName',
        'birthDate',
        'bio',
        'birthPlace',
        'height'
    ];

    private $profileData = null;

    /**
     * Profile constructor.
     * @param array $profileType
     * @param $profileName
     */
    public function __construct($profileType = null, $profileName = null)
    {
        if ($profileType != null && $profileName != null) {
            $this->profileData = $this::where($this::profession, $profileType)
                ->where($this::profileName, $profileName)
                ->first();
        }
    }


    /**
     * @return $this
     */
    public function movies()
    {
        return $this->belongsToMany(
            'App\Models\Movie',
            'Casts_And_Crews',
            'profileId',
            'movieId')
            ->withPivot('role');
    }

    /**
     * @return $this
     */
    public function tvSeries()
    {
        return $this->belongsToMany(
            'App\Models\tvSeries',
            'Casts_And_Crews_TVS',
            'profileId',
            'tvSeriesId')
            ->withPivot('role');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news()
    {
        return $this->hasMany('App\Models\ProfileNews', 'profileId');
    }


    /**
     * @return null
     */
    public function getProfileData()
    {
        return $this->profileData;
    }


    /**
     * @return mixed
     */
    public function getPopularMovies()
    {
        $movieList = DB::table('Movies')
            ->join('Casts_And_Crews', 'Movies.movieId', '=', 'Casts_And_Crews.movieId')
            ->join('Profiles', 'Casts_And_Crews.profileId', '=', 'Profiles.profileId')
            ->leftJoin('Feedbacks', 'Movies.movieId', '=', 'Feedbacks.movieId')
            ->select(DB::raw
            (
                'Movies.movieName,
                Movies.imageName,
                avg(Feedbacks.rating) as rate')
            )
            ->where('Profiles.profileId', $this->profileData['profileId'])
            ->orderBy('Feedbacks.rating', 'DESC', 'Movies.movieName', 'ASC')
            ->get();
        return $movieList;
    }

    /**
     * @return array
     */
    public function getAllFilms()
    {
        $data = [];
        $movieList = $this->profileData->movies;
        if ($movieList != null && count($movieList) > 0) {
            for ($i = 0; $i < count($movieList); $i++) {
                $movie = $movieList[$i];
                $data[$i] = [
                    'movieName' => $movie->movieName,
                    'releaseYear' => DateTimeConverters::yearFormater($movie->releaseDate),
                    'role' => $movie->pivot->role
                ];
            }
        }
        return $data;
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertProfileData($inputData)
    {
        $profileName = ucfirst($inputData['name']);
        $existProfileData = $this::where(self::profileName, '=', $profileName)->first();
        if ($existProfileData != null) {
            return false;
        } else {
            $newProfile = new Profile();
            $newProfile->name = $profileName;
            $newProfile->profession = $inputData['profession'];
            $newProfile->birthName = AdminPageController::checkNullValue($inputData['birthName']);
            $newProfile->birthDate = AdminPageController::checkNullValue($inputData['birthDate']);
            $newProfile->birthPlace = AdminPageController::checkNullValue($inputData['birthPlace']);
            $newProfile->bio = AdminPageController::checkNullValue($inputData['bio']);
            $newProfile->height = AdminPageController::checkNullValue($inputData['height']);
            if (isset($inputData['profileImage'])) {
                $profileImage = $inputData['profileImage'];
                $imageFileName = camel_case($profileName) . '.' .
                    $inputData['profileImage']->getClientOriginalExtension();
                $newProfile->profileImage = $imageFileName;
                AdminPageController::storeImage(strtolower(
                    $inputData['profession']),
                    $profileImage,
                    $imageFileName);
            }
            $newProfile->save();
            return true;
        }
    }




}