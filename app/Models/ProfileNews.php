<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/18/2015
 * Time: 8:35 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProfileNews extends Model
{
    public $timestamps = false;
    protected $table = 'Profile_News';
    protected $primaryKey = 'newsId';
    protected $guarded = [
        'newsId',
        'profileId'];

    /**
     * ProfileNews constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function news()
    {
        return $this->hasOne('App]Models\News', 'newsId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profileId');
    }

    /**
     * @param $inputData
     * @return bool
     */
    public function insertProfileNews($inputData)
    {
        $newsId = $inputData['newsId'];
        $profileId = $inputData['profileId'];
        $existingNews = $this::where('newsId', '=', $newsId)->first();
        if ($existingNews != null) {
            return false;
        } else {
            $newNews = new ProfileNews();
            $newNews->newsId = $newsId;
            $newNews->profileId = $profileId;
            $newNews->save();
            return true;
        }
    }


}