<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomePageController@showHomePageView'
]);


Route::get('/home', [
    'as' => 'home',
    'uses' => 'HomePageController@showHomePageView'
]);

Route::get('/topRankedMovies/', [
    'as' => 'topRankedMovies',
    'uses' => 'MovieCategoryPageController@showTopRankedMoviesList'
]);

Route::get('/nowShowingMovies', [
    'as' => 'nowShowing',
    'uses' => 'MovieCategoryPageController@showNowShowingMovieList'
]);

Route::get('/openingThisWeekMovies', [
    'as' => 'openingThisWeek',
    'uses' => 'MovieCategoryPageController@showOpeningThisWeekMovieList'
]);

Route::get('/terms', [
    'as' => 'terms',
    'uses' => 'TermsAndConditionsPageController@showTermsPage']);

Route::get('/trailer/movie/{movieName}', [
    'as' => 'movie_trailer_route',
    'uses' => 'MovieTrailerPageController@showMovieTrailerPage'
]);

Route::get('trailer/tv-series/{tvSeriesName}',[
    'as' => 'tv_series_trailer_route',
    'uses' => 'TVSeriesTrailerPageController@showTVSeriesTrailerPage'
]);

Route::get('/movies/category/{category}/', [
    'as' => 'moviesCategory',
    'middleware' => 'categoryIdentify',
    'uses' => 'MovieCategoryPageController@showCategoryWiseMoviesList'
]);

Route::get('/trailers', [
    'as' => 'allTrailers',
    'uses' => 'MovieTrailerPageController@showAllTrailersPage'
]);


Route::get('/boxOffice', [
    'as' => 'boxOffice',
    'uses' => 'MovieCategoryPageController@showBoxOfficeCollection']);

Route::get('/allTimeTopChart', [
    'as' => 'allTimeTopChart',
    'uses' => 'MovieCategoryPageController@showAllTimeTopChart'
]);

Route::get('/movies/moviesInfo/{movieName}/',
    ['as' => 'moviesInfo', 'uses' => 'MovieInfoPageController@showMovieInfoView']);

Route::get('/movies/profile/{profileType}/{profileName}',
    ['as' => 'profileInfo', 'uses' => 'ProfileInfoPageController@showProfileInfoView']);


/*Guest Accessible Routes */
Route::group(
    ['middleware' => 'guestAccess'], function () {

  /*  Route::post('/adminLogInAuth', [
        'as' => 'adminAuth',
        'uses' => 'AdminPageController@authonicateAdmin'
    ]);

    Route::get('/adminLogInPage', [
        'as' => 'adminLogInPage',
        'uses' => 'AdminPageController@showAdminLogInPage'
    ]);

    */


    Route::get('/signIn', [
        'as' => 'signIn',
        'uses' => 'SignInPageController@showSignInPage'
    ]);

    Route::get('/signUp', [
        'as' => 'signUp',
        'uses' => 'SignInPageController@showSignUpPage'
    ]);

    Route::post('/createUser', [
        'as' => 'createUser',
        'uses' => 'SignInPageController@createUser'
    ]);

    Route::post('/signInAuth', [
        'as' => 'signInAuth',
        'uses' => 'SignInPageController@signInAuth'
    ]);

});


/*Auth Accessible Routes */
Route::group(['middleware' => 'authAccess'], function () {

    Route::get('/{userName}/ratings', [
        'as' => 'ratings',
        'uses' => 'FeedbackPageController@showUserRatingsPage'
    ]);

    Route::get('/{userName}/reviewlist', [
        'as' => 'reviews',
        'uses' => 'FeedbackPageController@showUserReviewsPage'
    ]);

    Route::get('/{userName}/watchlist', [

        'as' => 'watchList',
        'uses' => 'FeedbackPageController@showUserWatchListPage'
    ]);
    Route::get('/logOut',
        [
            'as' => 'logOut',
            'uses' => 'SignInPageController@logOut'
        ]);

    Route::get('/tv-series/feedback/{tvSeriesName}/',[
        'as' => 'feedbackTVSeries',
        'uses' => 'TVSeriesFeedbackPageController@showFeedBackPage'
    ]);

    Route::post('tv-series/save/feedback/{tvSeriesName}/',[
        'as' => 'tvFeedbackAuth',
        'uses' => 'TVSeriesFeedbackPageController@saveFeedback'
    ]);

    Route::get('/movies/feedback/{movieName}/',
        [
            'as' => 'feedbackMovie',
            'uses' => 'FeedbackPageController@showFeedBackPage'
        ]);
    Route::post('/movies/save/feedback/{movieName}/',
        [
            'as' => 'feedbackAuth',
            'uses' => 'FeedbackPageController@saveFeedBack'
        ]);
});


/*error route*/
Route::get('/Errors', ['as' => 'error', function () {
    return View::make('pages/ExceptionPage');
}]);


/*Admin Auth routes */
/*Route::group(['middleware' => 'adminAccess'], function () {
    Route::get('/adminPanelPage', [
        'as' => 'adminPanel',
        'uses' => 'AdminPageController@showAdminPanelPage'
    ]);

    Route::get('/adminInsertDataPage/{insertDataPageType}', [
        'as' => 'movieInsertDataPage',
        'uses' => 'AdminPageController@showInsertDataPage'
    ]);
    Route::post('/adminInsertData/{insertDataType}', [
        'as' => 'adminInsertData',
        'uses' => 'AdminPageController@insertData'
    ]);
});
*/


/*Movie Theaters Routes*/

Route::get('/nearestTheaters', [
    'as' => 'nearestTheaters',
    'uses' => 'MovieTheatersPageController@showNearestTheaters']);

Route::get('/theaters/{cityName}/{theaterName}', [
    'as' => 'nowShowingMoviesInTheater',
    'uses' => 'MovieTheatersPageController@showNowShowingMoviesInTheater'
]);

Route::get('/findTheater', [
    'as' => 'theaterFinderPage',
    'uses' => 'MovieTheatersPageController@showFindTheaterPage'
]);

Route::post('/findTheaterList', [
    'as' => 'findTheater',
    'middleware' => 'theaterFinder',
    'uses' => 'MovieTheatersPageController@ShowTheaterListPage'
]);


Route::get('/upComingMovies', [
    'as' => 'upComingMovieList',
    'uses' => 'MovieCategoryPageController@showComingSoonMovieList'
]);

Route::get('latestNews/{newsType}/', [
    'as' => 'latestNews',
    'uses' => 'NewsPageController@getLatestNews'
]);

Route::get('/{newsType}', [
    'as' => 'news',
    'uses' => 'NewsPageController@getAllNews'
]);

Route::get('/tvSeries/{seriesName}/season-{seasonNo}', [
    'as' => 'tvSeriesSeasons',
    'uses' => 'TVSeriesPageController@showTVSeriesSeasonsInfoPage'
]);

Route::get('/tvSeries/{pageType}/{pageNum}', [
    'as' => 'tvSeriesList',
    'uses' => 'TVSeriesPageController@showTVSeriesList'
]);


Route::get('/tvSeries/{seriesName}', [
    'as' => 'tvSeries',
    'uses' => 'TVSeriesPageController@showTVSeriesInfoPage'
]);


Route::post('/search', [
    'as' => 'search',
    'uses' => 'SearchPageController@findSearchResult'
]);
