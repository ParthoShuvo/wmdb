<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/6/2015
 * Time: 9:51 PM
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;


class CategoryMiddleware
{
    /**
     * CategoryMiddleware constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param $request
     * @param Closure $next
     * @param $category
     * @return
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $category = $this->getCategoryParam();
        if (!$this->checkNonExistenceCategory($category)) {
            return Redirect::route('error');
        }
        return $response;
    }

    /**
     * @return mixed
     */
    public function getCategoryParam()
    {
        $parameters = Route::current()->parameters();
        $categoryParam = $parameters['category'];
        return $categoryParam;
    }

    /**
     * @param $category
     * @return bool
     */
    public function checkNonExistenceCategory($category)
    {
        $isExist = false;
        switch (ucfirst($category)) {
            case 'Action':
                $isExist = true;
                break;
            case 'Comedy':
                $isExist = true;
                break;
            case 'Romantic':
                $isExist = true;
                break;
            case 'Drama':
                $isExist = true;
                break;
            case 'Thriller':
                $isExist = true;
                break;
            case 'Horror':
                $isExist = true;
                break;
            case 'Sci-fi':
                $isExist = true;
                break;
            case 'Adventure':
                $isExist = true;
                break;
            case 'Biography':
                $isExist = true;
                break;
            case 'Animation':
                $isExist = true;
                break;
        }

        return $isExist;

    }


}