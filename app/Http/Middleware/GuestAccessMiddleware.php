<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class GuestAccessMiddleware
{

    /**
     * GuestAccessMiddleware constructor.
     */
    public function __construct()
    {
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (Session::has('isAdminLogIn') && Session::get('isAdminLogIn') == true) {
            return Redirect::route('adminPanel');
        }
        else if (Session::has('userId') && Session::get('userId')
            && Session::has('userName') && Session::get('userName')
        ) {
            return Redirect::route('home');
        }
        return $response;
    }
}
