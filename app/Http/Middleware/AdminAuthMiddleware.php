<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/27/2015
 * Time: 9:55 AM
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AdminAuthMiddleware
{

    /**
     * AdminAuthMiddleware constructor.
     */
    public function __construct()
    {
    }


    public function handle($request, Closure $next)
    {
        if (!Session::has('isAdminLogIn')) {
            return Redirect::route('adminLogInPage')->with('flash_msg', 'Log in first');
        }
        return $next($request);
    }
}