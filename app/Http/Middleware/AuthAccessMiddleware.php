<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AuthAccessMiddleware
{
    /**
     * AuthAccessMiddleware constructor.
     */
    public function __construct()
    {
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('userId') && !Session::has('userName')) {
            return Redirect::route('signIn')->with('flash_msg', 'Log in first');
        }
        return $next($request);
    }
}
