<?php

namespace App\Http\Middleware;

use App\Models\Theater;
use Closure;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class FindTheaterMiddleware
{
    /**
     * FindTheaterMiddleware constructor.
     */
    public function __construct()
    {
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $input = Input::all();
        $isExist = Theater::isCityNameExist($input['cityName'], $input['countryCode']);
        if (!$isExist) {
            return Redirect::route('theaterFinderPage')->with('notFoundMsg', 'Unknown City Name');
        }
        return $response;
    }
}
