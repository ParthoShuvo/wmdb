<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/28/2015
 * Time: 2:26 AM
 */

namespace App\Http\Controllers;


use App\Models\TVFeedback;
use App\Models\TVSeries;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TVSeriesFeedbackPageController extends Controller
{

    /**
     * TVSeriesFeedbackPageController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $tvSeriesName
     * @return \Illuminate\View\View
     */
    public function showFeedBackPage($tvSeriesName)
    {
        $tvSeries = new TVSeries($tvSeriesName);
        $tvSeriesData = $tvSeries->getTVSeriesInfo();
        if ($tvSeriesData != null) {
            $tvSeriesId = $tvSeriesData['tvSeriesId'];
            $tvSeriesFeedBack = new TVFeedback($tvSeriesId);
            $tvSeriesRatingData = $tvSeriesFeedBack->getRating();
            $currentUserFeedback = $tvSeriesFeedBack->getUserTVFeedBackInfo();
            return view('pages/tvSeriesFeedbackPage', [
                'tvSeriesData' => $tvSeriesData,
                'tvSeriesRating' => $tvSeriesRatingData,
                'userFeedBackData' => $currentUserFeedback
            ]);
        }
        return Redirect::route('error');
    }


    /**
     * @param $tvSeriesName
     * @return mixed
     */
    public function saveFeedBack($tvSeriesName)
    {
        $input = Input::all();
        $rules = [
            'rating' => 'required',
            'review' => 'required'
        ];
        $feedBackValidator = Validator::make($input, $rules);
        if ($feedBackValidator->fails()) {
            return Redirect::back();
        } else {
            $data = $this->getInputData($input);
            $tvSeries = new TVSeries($tvSeriesName);
            $tvSeriesData = $tvSeries->getTVSeriesInfo();
            $tvSeriesId = $tvSeriesData['tvSeriesId'];
            $tvSeriesFeedback = new TVFeedback($tvSeriesId);
            $currentUserFeedback = $tvSeriesFeedback->getUserTVFeedBackInfo();
            $userId = Session::get('userId');
            if ($currentUserFeedback == null) {
                $this->insertNewFeedBackData($data, $userId, $tvSeriesId);
            } else {
                $this->updateFeedBackData($data, $userId, $tvSeriesData);
            }
            Session::put("movieId", $tvSeriesData->movieId);
            Session::put("movieName", $tvSeriesData->movieName);
            Session::put("movieGenre", $tvSeriesData->genre);
            return Redirect::route("home");
        }
    }

    /**
     * @param $data
     * @param $userId
     * @param $tvSeriesId
     */
    private function insertNewFeedBackData($data, $userId, $tvSeriesId)
    {
        $feedBack = new TVFeedback();
        $feedBack->userId = $userId;
        $feedBack->tvSeriesId = $tvSeriesId;
        $feedBack->rating = $data['rating'];
        $feedBack->review = $data['review'];
        $feedBack->watchlist = $data['watchlist'];
        $feedBack->feedbackDate = $data['feedbackDate'];
        $feedBack->save();
    }


    /**
     * @param $data
     * @param $userId
     * @param $tvSeries
     */
    private function updateFeedBackData($data, $userId, $tvSeries)
    {
        $user = $tvSeries->users()->find($userId);
        if ($user != null) {
            $user->pivot->rating = $data['rating'];
            $user->pivot->review = $data['review'];
            $user->pivot->watchlist = $data['watchlist'];
            $user->pivot->feedbackDate = $data['feedbackDate'];
            $user->pivot->save();
        }
    }

    /**
     * @param $input
     * @return array
     */
    private function getInputData($input)
    {
        $watched = 0;
        if (isset($input['watched'])) {
            $watched = 1;
        }
        $data = [
            'rating' => round($input['rating']),
            'review' => $input['review'],
            'watchlist' => $watched,
            'feedbackDate' => date("Y-m-d")
        ];
        return $data;
    }

}