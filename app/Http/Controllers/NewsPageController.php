<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/18/2015
 * Time: 12:48 AM
 */

namespace App\Http\Controllers;


use App\Models\News;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class NewsPageController extends Controller
{

    /**
     * News constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $newsType
     * @return mixed
     */
    public function getLatestNews($newsType)
    {
        $topNews = null;
        $newsInfo = $this->getNews($newsType);
        if ($newsInfo['newsTypeExist']) {
            if ($newsInfo['latestNews'] != null && count($newsInfo['latestNews']) > 0) {
                $topNews = $newsInfo['latestNews'][0];
            }
            return View::make("templates/contentsTemplate/latestNewsContentsTemplate", [
                'topNews' => $topNews,
                'newsType' => $newsType
            ]);
        } else {
            return Redirect::route('error');
        }

    }

    public function getAllNews($newsType)
    {
        $newsInfo = $this->getNews($newsType);
        if ($newsInfo['newsTypeExist']) {

            return View::make("pages/newsPage", [
                'news' => $newsInfo['latestNews'],
                'newsType' => $newsType
            ]);

        } else {
            return Redirect::route('error');
        }
    }

    public function getNews($newsType)
    {
        $latestNews = null;
        $news = new News();
        $found = false;
        switch ($newsType) {
            case 'Top News':
                $latestNews = $news->getTopNews();
                $found = true;
                break;
            case 'Movie News':
                $latestNews = $news->getMovieNews();
                $found = true;
                break;
            case 'Celeb News':
                $latestNews = $news->getProfileNews();
                $found = true;
                break;
            case 'TV News':
                $found = true;
                break;
        }
        return $newsInfo = ['latestNews' => $latestNews, 'newsTypeExist' => $found];
    }
}
