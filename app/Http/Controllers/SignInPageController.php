<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/9/15
 * Time: 11:18 AM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\DataRetrievers\interfaces\UserDataAttributes;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SignInPageController extends Controller implements UserDataAttributes
{


    public function __construct()
    {

    }

    /**
     * @return View
     */
    public function showSignInPage()
    {

        return view('pages/signInPage');
    }

    /**
     * @return View
     */
    public function  showSignUpPage()
    {
        return view('pages/signUpPage');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createUser(Request $request)
    {
        $input = Input::all();
        $signUpFormValidator = Validator::make($input, User::$rules, User::$messages);
        if ($signUpFormValidator->fails()) {
            return Redirect::back()
                ->withInput(Input::except('password', 'confirmPassword'))
                ->withErrors($signUpFormValidator);
        } else {
            $this->insertUserData($input);
            $notification = $input['userName'];
            return View::make('pages/notificationPage',
                ['notification' => $notification]);
        }
    }

    /**
     * @param $data
     */
    private function insertUserData($data)
    {
        $user = new User();
        $user->firstName = $data['firstName'];
        $user->lastName = $data['lastName'];
        $user->userName = $data['userName'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->cityName = $data['cityName'];
        $user->birthDate = $data['birthDate'];
        $user->save();
    }

    /**
     * @param Request $request
     */
    public function signInAuth(Request $request)
    {
        $input = Input::all();
        $rules = [
            'userName' => 'required|alphaNum|between:8,30',
            'password' => 'required|min:8',
        ];
        $messages = [
            'required' => 'should be filled up',
            'alphaNum' => 'should be alpha numeric'
        ];
        $signInValidation = Validator::make($input, $rules, $messages);
        if ($signInValidation->fails()) {
            return Redirect::back()
                ->withInput(Input::except('password'))
                ->withErrors($signInValidation);
        } else {
            $user = User::where($this::userNameDB, $input['userName'])->first();
            if ($user != null && Hash::check($input['password'], $user->password)) {
                Auth::login($user);
                if (Auth::check()) {
                    Session::put('userName', Auth::user()->userName);
                    Session::put('userId', Auth::user()->userId);
                    return Redirect::intended('home');
                } else {
                    return abort(402, 'log in unsuccessful');
                }
            } else {
                return Redirect::back()
                    ->with('flash_msg', 'username and password combination mismatch');
            }
        }
    }

    /**
     *
     */
    public function logOut()
    {
        Auth::logout();
        if (Auth::check()) {
            return abort(402, 'log out unsuccessful');
        } else {
            Session::forget('userName');
            Session::forget('userId');
            return Redirect::route('home');
        }
    }


}