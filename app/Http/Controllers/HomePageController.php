<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/7/15
 * Time: 9:02 PM
 */
use App\Models\Movie;
use App\Models\Trailer;
use App\Models\TVTrailer;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class HomePageController extends Controller
{


    /**
     *
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function showHomePageView()
    {
        $tvTrailer = new TVTrailer();
        $recentTrailerList = $this->sliceData($tvTrailer->getRecentTrailers(), 0, 4);
        $movie = new Movie();
        $topRankedMovieList = $this->sliceData($movie->getTopRankedMovies(), 0, 6);
        $topMovie = $topRankedMovieList[rand(0, count($topRankedMovieList) - 1)];
        $nowShowingMovieList = $this->sliceData($movie->getNowShowingMovies(), 0, 4);
        $openingThisWeekMovieList = $this->sliceData(
            $movie->getOpeningThisWeekMovieList()->toArray(), 0, 5);
        $upcomingMovieList = $this->sliceData(
            $movie->getUpcomingMovieList(), 0, 5
        );
        Session::forget('authFailedMsg');
        Session::forget('msg');
        Session::forget('notFoundMsg');
        Session::forget('isAdminLogIn');
        return View::make('pages/home', [
            'topMovie' => $topMovie,
            'recentlyTrailerList' => $recentTrailerList,
            'topRankedMovieList' => $topRankedMovieList,
            'nowShowingMovieList' => $nowShowingMovieList,
            'openingThisWeekMovieList' => $openingThisWeekMovieList,
            'upcomingMovieList' => $upcomingMovieList
        ]);
    }

    /**
     * @param $data
     * @param $offset
     * @param $length
     * @return array|null
     */
    public static function sliceData($data, $offset, $length)
    {
        $sliceData = null;
        if ($data != null && count($data) > 0) {
            $sliceData = array_slice($data, $offset, $length);
        }
        return $sliceData;
    }

}