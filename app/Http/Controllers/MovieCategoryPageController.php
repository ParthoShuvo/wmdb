<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/13/2015
 * Time: 12:23 PM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use App\Http\DateTimeConverters;
use App\Models\Movie;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;

class MovieCategoryPageController extends Controller implements MovieDataAttributes
{


    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * @param $category
     * @return \Illuminate\View\View
     */
    public function showCategoryWiseMoviesList($category)
    {
        $movie = new Movie();
        $pageNum = Input::get('pageNum');
        $category = ucfirst($category);
        $movieList = $movie->getCategoryWiseMoviesListData($category);
        $perPage = 10;
        return $this::openMovieCategoryViewPage(
            $movieList,
            $category,
            $pageNum,
            $perPage);

    }

    /**
     * @param $tmpMovieList
     * @param $category
     * @param $pageNum
     * @param $perPage
     * @return \Illuminate\View\View
     */
    public static function openMovieCategoryViewPage($tmpMovieList, $category, $pageNum, $perPage)
    {

        $totalPages = (int)self::countTotalPages($tmpMovieList, $perPage);
        if ($pageNum > 0 && $pageNum <= $totalPages) {
            $offset = ($pageNum - 1) * $perPage;
            $movieList = array_slice($tmpMovieList, $offset, $perPage);
        } else {
            $movieList = null;
            $totalPages = 0;
        }

        return view('pages/movieCategoryListPage',
            [
                'category' => $category,
                'movieList' => $movieList,
                'totalPages' => $totalPages,
                'pageNum' => $pageNum
            ]);
    }

    /**
     * @param $movieList
     * @param $perPage
     * @return float|int
     */
    public static function countTotalPages($movieList, $perPage)
    {
        $totalPages = 0;
        if ($movieList != null && count($movieList) > 0) {
            $totalPages = ceil(count($movieList) / $perPage);
        }
        return $totalPages;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showTopRankedMoviesList()
    {
        $movie = new Movie();
        $movieList = $movie->getTopRankedMovies();
        $pageNum = Input::get('pageNum');
        $perPage = 10;
        return $this->openMovieCategoryViewPage(
            $movieList,
            'Top Ranked Movies',
            $pageNum,
            $perPage);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showNowShowingMovieList()
    {
        $movie = new Movie();
        $movieList = $movie->getNowShowingMovies();
        $category = 'Now Showing Movies';
        return view('pages/movieCategoryListPage',
            [
                'category' => $category,
                'movieList' => $movieList,
            ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showBoxOfficeCollection()
    {
        $movie = new Movie();
        $boxOfficeCollection = $movie->getWeeklyBoxOfficeResults();
        $category = 'Box Office Collection';
        $openingThisWeekMovieList = HomePageController::sliceData(
            $movie->getOpeningThisWeekMovieList()->toArray(), 0, 5);
        $upcomingMovieList = HomePageController::sliceData(
            $movie->getUpcomingMovieList(), 0, 5);
        return view('pages/movieCategoryListPage',
            [
                'category' => $category,
                'boxOfficeCollection' => $boxOfficeCollection,
                'openingThisWeekMovieList' => $openingThisWeekMovieList,
                'upcomingMovieList' => $upcomingMovieList
            ]);
    }


    /**
     * @return \Illuminate\View\View
     */
    public function showAllTimeTopChart()
    {
        $movie = new Movie();
        $allTimeTopChart = $movie->getAllTimeTopChartMovieList();
        $category = 'All Time Top Chart';
        $openingThisWeekMovieList = HomePageController::sliceData(
            $movie->getOpeningThisWeekMovieList()->toArray(), 0, 5);
        $upcomingMovieList = HomePageController::sliceData(
            $movie->getUpcomingMovieList(), 0, 5);
        return view('pages/movieCategoryListPage',
            [
                'category' => $category,
                'allTimeTopChart' => $allTimeTopChart,
                'openingThisWeekMovieList' => $openingThisWeekMovieList,
                'upcomingMovieList' => $upcomingMovieList
            ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showOpeningThisWeekMovieList()
    {
        $openingThisWeekMovieData = [];
        $year = (int)DateTimeConverters::getYear();
        $week = (int)DateTimeConverters::getWeekNumber();
        $count = 0;
        $movie = new Movie();
        $category = 'Opening This Week';
        for ($day = 1; $day <= 7; $day++) {
            $date = date('Y-m-d', strtotime($year . "W" . $week . $day));
            $givenDateMovieData = $movie->getMovieDataOnReleasedDate($date);
            if ($givenDateMovieData != null && count($givenDateMovieData) > 0) {
                $openingThisWeekMovieData[$count++] = [
                    'releaseDate' => DateTimeConverters::dateFormater($date),
                    'movieList' => $givenDateMovieData
                ];
            }
        }
        return view('pages/movieCategoryListPage',
            [
                'category' => $category,
                'dateWiseMovieList' => $openingThisWeekMovieData,
            ]);
    }

    /**
     *
     */
    public function showComingSoonMovieList()
    {
        $movie = new Movie();
        $upComingMoviesReleaseDates = $movie->getUpComingMoviesReleaseDate();
        $upComingMovieDataList = null;
        $category = 'Up Coming Movies';
        if ($upComingMoviesReleaseDates != null && count($upComingMoviesReleaseDates) > 0) {
            $upComingMovieDataList = [];
            $count = 0;
            foreach($upComingMoviesReleaseDates as $releaseDates) {
                $releaseDate = $releaseDates['releaseDate'];
                $givenDateMovieData = $movie->getMovieDataOnReleasedDate($releaseDate);
                if ($givenDateMovieData != null && count($givenDateMovieData) > 0) {
                    $upComingMovieDataList[$count++] = [
                        'releaseDate' => DateTimeConverters::dateFormater($releaseDate),
                        'movieList' => $givenDateMovieData
                    ];
                }
            }
        }

        return view('pages/movieCategoryListPage',
            [
                'category' => $category,
                'dateWiseMovieList' => $upComingMovieDataList,
            ]);

    }


}