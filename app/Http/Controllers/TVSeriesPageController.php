<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 3:07 PM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\DataRetrievers\interfaces\TVSeriesDataAttributes;
use App\Http\Controllers\DataRetrievers\interfaces\TVSeriesSeasonsDataAttributes;
use App\Models\CastAndCrewTV;
use App\Models\TVFeedback;
use App\Models\TVSeries;
use App\Models\TVTrailer;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class TVSeriesPageController extends Controller implements
    TVSeriesSeasonsDataAttributes, TVSeriesDataAttributes
{

    private $actorsList = null;
    private $directorsList = null;
    private $awardList = null;
    private $tvSeriesRatingData = null;
    private $tvSeriesReviews = null;
    private $hasTVSeriesTrailer = false;
    private $currentUserFeedback = null;

    /**
     * TVSeriesPageController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $seriesName
     * @return \Illuminate\View\View
     */
    public function showTVSeriesInfoPage($seriesName)
    {
        $tvSeries = new TVSeries($seriesName);
        $tvSeriesData = $tvSeries->getTVSeriesInfo();
        $dataFound = false;
        $seasons = null;
        $isTVSeriesOnAir = false;
        if ($tvSeriesData != null) {
            $dataFound = true;
            $seasons = $tvSeriesData->tvSeriesSeasons()
                ->get([self::seasonNo, self::startYear]);
            if ($seasons != null && count($seasons) > 0) {
                $isTVSeriesOnAir = true;
            }
            $this->setTVSeriesFeedBackData($tvSeriesData['tvSeriesId']);
            $this->setCastsAndCrews($tvSeriesData['tvSeriesId']);
            $tvTrailer = new TVTrailer();
            $this->hasTVSeriesTrailer = $tvTrailer->hasTrailer($tvSeriesData);

        }
        return view('pages/movieInfoPage', [
            'dataFound' => $dataFound,
            'tvSeriesData' => $tvSeriesData,
            'seasons' => $seasons,
            'directorsList' => $this->directorsList,
            'actorsList' => $this->actorsList,
            'reviews' => $this->tvSeriesReviews,
            'awardList' => $this->awardList,
            'tvSeriesRating' => $this->tvSeriesRatingData,
            'hasTVSeriesTrailer' => $this->hasTVSeriesTrailer,
            'currentUserFeedBack' => $this->currentUserFeedback,
            'isTVSeriesOnAir' => $isTVSeriesOnAir
        ]);
    }

    /**
     * @param $tvSeriesId
     *
     */
    private function setTVSeriesFeedBackData($tvSeriesId)
    {
        $tvSeriesFeedback = new TVFeedback($tvSeriesId);
        $this->tvSeriesReviews = $tvSeriesFeedback->getAllReviews();
        $this->tvSeriesRatingData = $tvSeriesFeedback->getRating();
        $this->currentUserFeedback = $tvSeriesFeedback->getUserTVFeedBackInfo();
    }

    /**
     * @param $tvSeriesId
     */
    public function setCastsAndCrews($tvSeriesId)
    {
        $castsAndCrewsTV = new CastAndCrewTV($tvSeriesId);
        if ($castsAndCrewsTV != null) {
            $this->actorsList = $castsAndCrewsTV->getActorList();
            $this->directorsList = $castsAndCrewsTV->getDirectorList();
        }
    }

    /**
     * @param $seriesName
     * @param $seasonNo
     * @return mixed
     */
    public function showTVSeriesSeasonsInfoPage($seriesName, $seasonNo)
    {
        $tvSeries = new TVSeries(ucfirst($seriesName));
        $tvSeriesData = $tvSeries->getTVSeriesInfo();
        if ($tvSeries != null) {
            $seasons = $tvSeriesData->tvSeriesSeasons()
                ->orderBy(self::seasonNo, 'ASC')
                ->get([self::seasonNo, self::seasonId]);
            if ($this->isSeasonExist($seasonNo, $seasons)) {
                $desiredSeason = $seasons[$seasonNo - 1];
                $episodes = $desiredSeason->tvSeriesEpisodes()
                    ->select(DB::raw(
                        'TV_SERIES_EPISODES.episodeName,
                        TV_SERIES_EPISODES.episodeImage,
                       TV_SERIES_EPISODES.episodeNo,
                       TV_SERIES_EPISODES.summary,
                        DATE_FORMAT(TV_SERIES_EPISODES.onAirDate, "%d %M %Y") as onAirDate'))
                    ->get();
                return View::make('pages/tvSeriesEpisodesPage', [
                    'category' => 'TV Series',
                    'tvSeriesName' => $seriesName,
                    'pageNum' => $seasonNo,
                    'totalPages' => count($seasons),
                    'episodes' => $episodes
                ]);
            }
        }
        return Redirect::route('error');
    }

    private function isSeasonExist($seasonNo, $seasons)
    {
        $isExist = false;
        if ($seasons != null && count($seasons) > 0) {
            $low = 0;
            $high = count($seasons) - 1;
            while ($low <= $high && !$isExist) {
                $mid = (int)($low + $high) / 2;
                if ($seasonNo < $seasons[$mid]['seasonNo']) {
                    $high = $mid - 1;
                } else if ($seasonNo > $seasons[$mid]['seasonNo']) {
                    $low = $mid + 1;
                } else {
                    $isExist = true;
                }
            }
        }
        return $isExist;
    }

    /**
     * @param $pageType
     * @param $pageNum
     * @return \Illuminate\View\View
     */
    public function showTVSeriesList($pageType, $pageNum)
    {

        $tvSeries = new TVSeries();
        $tvSeriesList = null;
        $category = null;
        $found = false;
        switch ($pageType) {
            case 'top-rated-tv-series':
                $tvSeriesList = $tvSeries->getTopRatedTVSeries();
                $category = 'Top Ranked TV Series';
                $found = true;
                break;
            case 'new-tv-series':
                $tvSeriesList = $tvSeries->getNewTVSeries();
                $category = 'New TV Series';
                $found = true;
                break;
            case 'upcoming-tv-series':
                $tvSeriesList = $tvSeries->getUpComingTVSeries();
                $category = 'Upcoming TV Series';
                $found = true;
                break;
            default:
                break;
        }
        if ($found) {
            return MovieCategoryPageController::openMovieCategoryViewPage(
                $tvSeriesList, $category, $pageNum, 10);
        } else {
            return Redirect::route('error');
        }
    }


}