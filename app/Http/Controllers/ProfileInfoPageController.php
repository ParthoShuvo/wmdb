<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/16/2015
 * Time: 7:14 AM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\DataRetrievers\ProfileDataRetriever;
use App\Models\Profile;
use App\Models\ProfileAwardsMovies;
use Illuminate\Routing\Controller;

class ProfileInfoPageController extends Controller
{

    /**
     * ProfileInfoPageController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $profileType
     * @param $profileName
     * @return \Illuminate\View\View
     */
    public function showProfileInfoView($profileType, $profileName)
    {
        $profile = new Profile($profileType, $profileName);
        $profileData = $profile->getProfileData();
        $topMovies = null;
        $movieData = null;
        $awards = null;
        $dataFound = false;
        if ($profileData != null) {
            $dataFound = true;
            $topMovies = $profile->getPopularMovies();
            $movieData = $profile->getAllFilms();
            $profileAward = new ProfileAwardsMovies(null, null);
            $awards = HomePageController::sliceData(
                $profileAward->getProfileAwardsAndNominations($profileData), 0, 6);
        }
        return view('pages/profileInfoPage', [
            'profileData' => $profileData,
            'topMovies' => $topMovies,
            'movieData' => $movieData,
            'awards' => $awards,
            'dataFound' => $dataFound
        ]);

    }
}