<?php

namespace App\Http\Controllers;

use App\Http\Controllers\DataRetrievers\interfaces\FeedBackDataAttributes;
use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use App\Http\Controllers\DataRetrievers\interfaces\UserDataAttributes;
use App\Models\Feedback;
use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/29/2015
 * Time: 12:12 AM
 */
class FeedbackPageController extends Controller implements FeedBackDataAttributes,
    MovieDataAttributes, UserDataAttributes
{
    /**
     * FeedbackPageController constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param $mediaName
     * @return \Illuminate\View\View
     */
    public function showFeedBackPage($mediaName)
    {
        $movie = new Movie($mediaName);
        $movieData = $movie->getMovieData();
        if ($movieData != null) {
            $movieId = $movieData['movieId'];
            $movieFeedback = new Feedback($movieId);
            $movieRatingData = $movieFeedback->getRating();
            $currentUserFeedback = $movieFeedback->getUserMovieFeedBackInfo();
            return view('pages/feedbackPage', [
                'movieData' => $movieData,
                'movieRating' => $movieRatingData,
                'userFeedBackData' => $currentUserFeedback
            ]);
        }
        return Redirect::route('error');

    }


    /**
     * @param $movieName
     * @return mixed
     */
    public function saveFeedBack($movieName)
    {
        $input = Input::all();
        $rules = [
            'rating' => 'required',
            'review' => 'required'
        ];
        $feedBackValidator = Validator::make($input, $rules);
        if ($feedBackValidator->fails()) {
            return Redirect::back();
        } else {
            $data = $this->getInputData($input);
            $movie = new Movie($movieName);
            $movieData = $movie->getMovieData();
            $movieId = $movieData['movieId'];
            $movieFeedback = new Feedback($movieId);
            $currentUserFeedback = $movieFeedback->getUserMovieFeedBackInfo();
            $userId = Session::get('userId');
            if ($currentUserFeedback == null) {
                $this->insertNewFeedBackData($data, $userId, $movieData->movieId);
            } else {
                $this->updateFeedBackData($data, $userId, $movieData);
            }
            Session::put("movieId", $movieData->movieId);
            Session::put("movieName", $movieData->movieName);
            Session::put("movieGenre", $movieData->genre);
            return Redirect::route("home");
        }
    }

    /**
     * @param $input
     * @return array
     */
    private function getInputData($input)
    {
        $watched = 0;
        if (isset($input['watched'])) {
            $watched = 1;
        }
        $data = [
            'rating' => round($input['rating']),
            'review' => $input['review'],
            'watchlist' => $watched,
            'feedbackDate' => date("Y-m-d")
        ];
        return $data;
    }

    /**
     * @param $data
     * @param $userId
     * @param $movieId
     */
    private function insertNewFeedBackData($data, $userId, $movieId)
    {
        $feedBack = new Feedback();
        $feedBack->userId = $userId;
        $feedBack->movieId = $movieId;
        $feedBack->rating = $data['rating'];
        $feedBack->review = $data['review'];
        $feedBack->watchlist = $data['watchlist'];
        $feedBack->feedbackDate = $data['feedbackDate'];
        $feedBack->save();
    }

    /**
     * @param $data
     * @param $userId
     * @param $movie
     */
    private function updateFeedBackData($data, $userId, $movie)
    {
        $user = $movie->users()->find($userId);
        if ($user != null) {
            $user->pivot->rating = $data['rating'];
            $user->pivot->review = $data['review'];
            $user->pivot->watchlist = $data['watchlist'];
            $user->pivot->feedbackDate = $data['feedbackDate'];
            $user->pivot->save();
        }
    }

    /**
     * @param $userName
     * @return \Illuminate\View\View
     */
    public function showUserWatchListPage($userName)
    {
        $input = Input::all();
        $pageNum = $input['pageNum'];
        $feedback = new Feedback();
        $userWatchList = $feedback->getUserMovieWatchList($userName);
        return MovieCategoryPageController::openMovieCategoryViewPage(
            $userWatchList,
            'Your WatchList',
            $pageNum,
            10);

    }

    /**
     * @param $userName
     * @return \Illuminate\View\View
     */
    public function showUserRatingsPage($userName)
    {
        $input = Input::all();
        $pageNum = $input['pageNum'];
        $feedback = new Feedback();
        $userRatingsList = $feedback->getUserRatingsList($userName);
        return MovieCategoryPageController::openMovieCategoryViewPage(
            $userRatingsList,
            'Your Ratings',
            $pageNum,
            10);
    }


    public function showUserReviewsPage($userName)
    {
        $input = Input::all();
        $pageNum = $input['pageNum'];
        $feedback = new Feedback();
        $userReviewsList = $feedback->getUserReviewsList($userName);
        return MovieCategoryPageController::openMovieCategoryViewPage(
            $userReviewsList,
            'Your Reviews',
            $pageNum,
            10);
    }


}