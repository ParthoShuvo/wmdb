<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/23/2015
 * Time: 4:47 AM
 */

namespace App\Http\Controllers;


use App\Models\BoxOffice;
use App\Models\CastAndCrew;
use App\Models\Movie;
use App\Models\MovieAward;
use App\Models\MovieNews;
use App\Models\News;
use App\Models\NowShowing;
use App\Models\Profile;
use App\Models\ProfileAwardsMovies;
use App\Models\ProfileNews;
use App\Models\Theater;
use App\Models\Trailer;
use App\Models\TVSeries;
use App\Models\TVSeriesEpisode;
use App\Models\TVSeriesSeason;
use App\Models\TVTrailer;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AdminPageController extends Controller
{

    const ADMIN_NAME = 'ParthoShuvo09';
    const PASSWORD = 'Bluebirds9';

    /**
     * AdminPageController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $pageType
     * @param $image
     * @param $imageFileName
     */
    public static function storeImage($pageType, $image, $imageFileName)
    {
        $storePath = base_path() . '/public/images/';
        switch ($pageType) {
            case 'movie':
                $storePath .= 'movies';
                break;
            case 'actor':
                $storePath .= 'actors';
                break;
            case 'director':
                $storePath .= 'directors';
                break;
            case 'theater':
                $storePath .= 'theaters';
                break;
            case 'tv-series':
                $storePath .= 'tvSeries';
                break;
            case 'episodes':
                $storePath .= 'episodes';
                break;
            default:
                break;
        }
        $image->move($storePath, $imageFileName);
    }

    /**
     * @return mixed
     */
    public function showAdminLogInPage()
    {
        return View::make('adminPages/adminPanelSignInPage');
    }


    public function authonicateAdmin()
    {
        $input = Input::all();
        $adminName = $input['userName'];
        $password = $input['password'];
        if ($adminName == $this::ADMIN_NAME && $password == $this::PASSWORD) {
            Session::put('isAdminLogIn', true);
            return Redirect::route('adminPanel');
        } else {
            return Redirect::back()
                ->with('flash_msg', 'admin name and password combination mismatch');
        }
    }

    /**
     * @return mixed
     */
    public function showAdminPanelPage()
    {
        return View::make('adminPages/adminPanel');
    }

    /**
     * @param $insertDataPageType
     * @return mixed
     */
    public function  showInsertDataPage($insertDataPageType)
    {
        $insertDataPageType = strtolower($insertDataPageType);
        $view = null;
        switch ($insertDataPageType) {
            case 'movie':
            case 'profile':
            case 'news':
            case 'movie-theaters':
            case 'tv-series':
                $view = View::make('adminPages/adminDataInsertionPageModel',
                    ['pageType' => $insertDataPageType]);
                break;
            case 'movie-trailer':
            case 'box-office':
            case 'movie-award':
                $movieData = Movie::get(['movieId', 'movieName']);
                $view = View::make('adminPages/adminDataInsertionPageModel',
                    [
                        'pageType' => $insertDataPageType,
                        'movieData' => $movieData
                    ]);
                break;
            case 'movie-cast-and-crew':
            case 'movie-cast-and-crew-award':
                $movieData = Movie::get(['movieId', 'movieName']);
                $profileData = Profile::get(['profileId', 'name']);
                $view = View::make('adminPages/adminDataInsertionPageModel',
                    [
                        'pageType' => $insertDataPageType,
                        'movieData' => $movieData,
                        'profileData' => $profileData
                    ]);
                break;
            case 'movie-news':
            case 'celeb-news':
                $view = $this->getCategoryWiseNewsView($insertDataPageType);
                break;
            case 'now-showing':
                $movieData = Movie::where('releaseDate', '<=', date('Y-m-d'))
                    ->orderBy('movieName', 'ASC')
                    ->get(['movieId', 'movieName']);
                $movieTheaterData = Theater::get(['theaterId', 'theaterName', 'countryCode']);
                $view = View::make('adminPages/adminDataInsertionPageModel',
                    [
                        'pageType' => $insertDataPageType,
                        'movieData' => $movieData,
                        'movieTheaterData' => $movieTheaterData
                    ]);
                break;
            case 'tv-series-seasons':
            case 'tv-series-episodes':
            case 'tv-series-trailer':
                $tvSeriesData = TVSeries::get(['tvSeriesId', 'seriesName']);
                $view = View::make('adminPages/adminDataInsertionPageModel',
                    [
                        'pageType' => $insertDataPageType,
                        'tvSeriesData' => $tvSeriesData
                    ]);
                break;
            default:
                $pageType = null;
                break;
        }
        if ($view != null) {
            return $view;
        } else {
            return Redirect::route('error');
        }
    }

    /**
     * @param $insertDataType
     * @return string
     */
    public function insertData($insertDataType)
    {
        $inputData = Input::all();
        $result = null;
        $redirectPage = null;
        switch ($insertDataType) {
            case 'movie':
                $movie = new Movie();
                $result = $movie->insertMovieData($inputData);
                break;
            case 'movie-trailer':
                $movieTrailer = new Trailer();
                $result = $movieTrailer->insertMovieTrailerData($inputData);
                break;
            case 'box-office':
                $boxOffice = new BoxOffice();
                $result = $boxOffice->insertBoxOfficeData($inputData);
                break;
            case 'movie-award':
                $movieAward = new MovieAward();
                $result = $movieAward->insertMovieAwardData($inputData);
                break;
            case 'movie-cast-and-crew':
                $movieCastAndCrew = new CastAndCrew();
                $result = $movieCastAndCrew->insertMovieCastAndCrewData($inputData);
                break;
            case 'movie-cast-and-crew-award':
                $profileMovieAward = new ProfileAwardsMovies();
                $result = $profileMovieAward->insertProfileMovieAward($inputData);
                break;
            case 'news':
                $news = new News();
                $result = $news->insertNews($inputData);
                break;
            case 'movie-news':
                $movieNews = new MovieNews();
                $result = $movieNews->insertMovieNews($inputData);
                break;
            case 'profile':
                $profile = new Profile();
                $result = $profile->insertProfileData($inputData);
                break;
            case 'celeb-news':
                $profileNews = new ProfileNews();
                $result = $profileNews->insertProfileNews($inputData);
                break;
            case 'now-showing':
                $nowShowing = new NowShowing();
                $result = $nowShowing->insertNowShowingMoviesInTheater($inputData);
                break;
            case 'movie-theaters':
                $movieTheater = new Theater();
                $result = $movieTheater->insertTheaterData($inputData);
                break;
            case 'tv-series':
                $tvSeries = new TVSeries();
                $result = $tvSeries->insertTVSeriesData($inputData);
                break;
            case 'tv-series-seasons':
                $tvSeriesSeasons = new TVSeriesSeason();
                $result = $tvSeriesSeasons->insertTVSeriesSeaosnData($inputData);
                break;
            case 'tv-series-episodes':
                $tvSeriesEpisodes = new TVSeriesEpisode();
                $result = $tvSeriesEpisodes->insertTVSeriesEpisodes($inputData);
                break;
            case 'tv-series-trailer':
                $tvSeriesTrailer = new TVTrailer();
                $result = $tvSeriesTrailer->insertTVSeriesTrailers($inputData);
                break;
            default:
                $pageType = null;
                break;
        }

        if ($result) {
            return 'Data inserted Successfully';
        } else {
            return Redirect::back()
                ->with('errMsg', 'Data already exist');
        }
    }

    /**
     * @param $value
     * @return null
     */
    public static function checkNullValue($value)
    {
        if ($value == null || $value == '') {
            return null;
        }
        return $value;
    }

    private function getCategoryWiseNewsView($pageType)
    {
        $newsType = null;
        $movieData = null;
        $profileData = null;
        $view = null;
        switch ($pageType) {
            case 'movie-news':
                $newsType = 'Movie';
                $movieData = Movie::get(['movieId', 'movieName']);
                break;
            case 'celeb-news':
                $newsType = 'Profile';
                $profileData = Profile::get(['profileId', 'name']);
                break;
            default:
                break;
        }
        $newsData = News::where('newsType', '=', $newsType)->get(['newsId', 'headLine']);
        if ($movieData != null && count($movieData) > 0) {
            $view = View::make('adminPages/adminDataInsertionPageModel',
                [
                    'pageType' => $pageType,
                    'movieData' => $movieData,
                    'newsData' => $newsData
                ]);
        } else {
            $view = View::make('adminPages/adminDataInsertionPageModel',
                [
                    'pageType' => $pageType,
                    'profileData' => $profileData,
                    'newsData' => $newsData
                ]);
        }
        return $view;
    }
}