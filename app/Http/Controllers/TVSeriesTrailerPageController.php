<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/28/2015
 * Time: 1:00 AM
 */

namespace App\Http\Controllers;


use App\Models\TVSeries;
use App\Models\TVTrailer;
use Illuminate\Routing\Controller;

class TVSeriesTrailerPageController extends Controller
{

    /**
     * TVSeriesTrailerPage constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param $tvSeriesName
     * @return mixed
     */
    public function showTVSeriesTrailerPage($tvSeriesName)
    {
        $trailerDetails = null;
        $trailerDetails = $this->getTVSeriesTrailerDataDetails($tvSeriesName);
        if ($trailerDetails != null) {
            $trailerPageController = new MovieTrailerPageController();
            return $trailerPageController->showTrailerPage($trailerDetails);
        }
        return Redirect::route('error');
    }

    /**
     * @param $tvSeriesName
     * @return mixed|null
     */
    private function getTVSeriesTrailerDataDetails($tvSeriesName)
    {
        $trailerDetails = null;
        $tvSeries = new TVSeries($tvSeriesName);
        $tvSeriesData = $tvSeries->getTVSeriesInfo();
        if ($tvSeriesData != null) {
            $tvSeriesData = $this->getTVSeriesTrailer($tvSeriesData);
        }
        return $tvSeriesData;
    }


    /**
     * @param $tvSeriesData
     * @return mixed
     */
    private function getTVSeriesTrailer($tvSeriesData)
    {
        $tvTrailer = new TVTrailer();
        $tvTrailerData = $tvTrailer->getTrailer($tvSeriesData);
        $recentTrailer = $tvTrailerData[0];
        $tvTrailer->increaseViewCount($recentTrailer['trailerId']);
        return $recentTrailer;
    }
}