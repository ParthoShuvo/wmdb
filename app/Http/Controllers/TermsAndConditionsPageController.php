<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/9/2015
 * Time: 8:20 PM
 */

namespace App\Http\Controllers;


use Illuminate\Routing\Controller;

class TermsAndConditionsPageController extends Controller
{


    /**
     * TermsAndConditionsPageController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return $this
     */
    public function showTermsPage()
    {
        $privacyConditions = [];
        $privacyConditions[0] = '<h2>WMDB Conditions of Use</h2>';
        $privacyConditions[1] = 'Welcome to WMDB. WMDB.com, Inc.
    and/or its affiliates ("WMDB") provide its website features and other services
    to you subject to the following conditions. <b>If you visit WMDB.com, use other WMDB
    services, products, or use software or mobile applications provided by IMDb that states
    that it is subject to these Conditions of Use (collectively "WMDB Services"), you accept
    these conditions</b>. Please read them carefully. In addition, when you use any current, future
    WMDB Services, (e.g., WMDBPro.com) you also will be subject to the guidelines, terms and
    agreements ("Terms") applicable to that IMDb Service. If these Conditions of Use are
    inconsistent with the Terms provided for any WMDB service, the Terms will control.';
        $privacyConditions[2] = '<h3>Privacy</h3>';
        $privacyConditions[3] = 'Please review our <a href="#">Privacy Notice</a>,
    which also governs your use of any WMDB Service, to understand our practices.';
        $privacyConditions[4] = '<h3>Electronic Communications</h3>';
        $privacyConditions[5] = 'When you use any IMDb Service or send e-mails to us,
    you are communicating with us electronically. You consent to receive communications from us
    electronically. We will communicate with you by e-mail or by posting notices on this site or
    through the other WMDB Services. You agree that all agreements, notices, disclosures and other
    communications that we provide to you electronically satisfy any legal requirement that such
    communications be in writing.';
        $privacyConditions[6] = '<h3>Copyright</h3>';
        $privacyConditions[7] = 'All content included on this site in or made available
        through any IMDb Service, such as text, graphics, logos, button icons, images,
        audio clips, video clips, digital downloads, data compilations, and software,
        is the property of IMDb or its content suppliers and protected by United States
        and international copyright laws. The compilation of all content included in or
        made available through any IMDb Service is the exclusive property of IMDb and
        protected by U.S. and international copyright laws. All software used in any
        IMDb Service is the property of IMDb or its software suppliers and protected
        by United States and international copyright laws.';
        $privacyConditions[8] = '<h3>Trademarks</h3>';
        $privacyConditions[9] = 'IMDb and STARMETER are registered trademarks,
        and the IMDb logo, IMDbPRO, MOVIEMETER, and other marks indicated in
        any IMDb Services are trademarks of IMDb in the United States and/or other
        countries. Other IMDb graphics, logos, page headers, button icons, scripts,
        and service names are trademarks or trade dress of IMDb. IMDb\'s trademarks and
        trade dress may not be used in connection with any product or service that is not
        IMDb\'s, in any manner that is likely to cause confusion among customers, or in
        any manner that disparages or discredits IMDb. All other trademarks not owned by
        IMDb that appear on this site or in any IMDb Service are the property of their
        respective owners, who may or may not be affiliated with, connected to, or sponsored by IMDb.';
        $privacyConditions[10] = '<h3>License and Site Access</h3>';
        $privacyConditions[11] = 'Subject to your compliance with these Conditions of Use and
        your payment of any applicable fees, IMDb or its content providers grants you a limited,
        non-exclusive, non-transferable, non-sublicenseable license to access and make personal
        and non-commercial use of the IMDb Services, including digital content available through
        the IMDb Services, and not to download (other than page caching) or modify this site,
        or any portion of it, except with express written consent of IMDb. Additional license
        terms may be found in the Terms. The IMDb Services or any portion of such services may
        not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for
        any commercial purpose without express written consent of IMDb. This license does not
        include any resale or commercial use of any IMDb Service or its contents or any derivative
        use of this site or its contents. All licenses are non-exclusive and all rights not
        expressly granted to you in these Conditions of Use or any applicable Terms are reserved
        and retained by IMDb or its licensors, suppliers, publishers, rightsholders, or other
        content providers. You will use all IMDb Services in compliance with all applicable laws.';
        $privacyConditions[12] = '<p><b>Robots and Screen Scraping:</b> You may not use data mining,
        robots, screen scraping, or similar data gathering and extraction tools on this site,
        except with our express written consent as noted below.</p>';
        $privacyConditions[13] = '<p><b>Framing:</b> You may not frame or utilize framing techniques
        to enclose any trademark, logo, or other proprietary information (including images,
        text, page layout, or form) of IMDb without express written consent.</p>';
        $privacyConditions[14] = '<p><b>Meta Tags:</b> You may not use any meta tags or any other
        "hidden text" utilizing IMDb\'s name or trademarks without the express written consent
        of IMDb. Any unauthorized use terminates the permission or license granted by IMDb.</p>';
        $privacyConditions[15] = '<p><b>Licensing IMDb Content; Consent to Use Robots and Crawlers:</b>
        If you are interested in receiving our express written permission to use IMDb content for
        your non-personal (including commercial) use, please visit our <a href="#">Content Licensing section</a> or
        <a href="#">contact our Licensing Department</a>. We do allow the limited use of robots and crawlers, such as
        those from certain search engines, with our express written consent. If you are interested
        in receiving our express written permission to use robots or crawlers on our site,
        please <a href="#">contact our Licensing Department</a>. </p>';
        $privacyConditions[16] = '<p><b>Linking to IMDb.com:</b> You are granted a limited, revocable,
        and nonexclusive right to create a hyperlink to IMDb.com so long as the link follows our
        <a href="#">linking guide</a> and does not portray IMDb, its services in a false, misleading, derogatory,
        or otherwise offensive matter. You may not use any IMDb logo or other proprietary graphic or
        trademark as part of the link without express written permission except as outlined in our <a href="#">help
        section</a>.</p>';
        $privacyConditions[17] = '<h3>Your Account</h3>';
        $privacyConditions[18] = 'If you use any IMDb Service, you are responsible for maintaining the
        confidentiality of log-in information and for restricting access to your computer, and you agree
        to accept responsibility for all activities that occur under your account or password.
        IMDb reserves the right to refuse service, terminate accounts, or remove or edit content
        in its sole discretion. ';
        $privacyConditions[19] = '<h3>Reviews, Comments, Communications, and Other Content</h3>';
        $privacyConditions[20] = 'Visitors may post reviews, comments, and other content; and submit
        suggestions, ideas, comments, questions, or other information, so long as the content is not
        illegal, obscene, threatening, defamatory, invasive of privacy, infringing of intellectual
        property rights, or otherwise injurious to third parties or objectionable and does not consist
        of or contain software viruses, political campaigning, commercial solicitation, chain letters,
        mass mailings, or any form of "spam." You may not use a false e-mail address, impersonate any
        person or entity, or otherwise mislead as to the origin of your content. IMDb reserves the right
        (but not the obligation) to remove or edit such content, but does not regularly review posted
        content.';
        $privacyConditions[21] = '<p><b>Your License to IMDb:</b>
        If you do post content or submit material, and unless we indicate otherwise, you grant IMDb a
        nonexclusive, royalty-free, perpetual, irrevocable, and fully sublicensable right to use,
        reproduce, modify, adapt, publish, translate, create derivative works from, distribute, and
        display such content throughout the world in any media. You grant IMDb and its sublicensees
        the right to use the name that you submit in connection with such content, if they choose.
        You represent and warrant that you own or otherwise control all of the rights to the content
        that you post; that the content is accurate; that use of the content you supply does not violate
        this policy and will not cause injury to any person or entity; and that you will indemnify IMDb
        for all claims resulting from content you supply. IMDb has the right but not the obligation
        to monitor and edit or remove any activity or content. IMDb takes no responsibility and assumes
        no liability for any content posted by you or any third party. If you would like to learn more
        about how we handle content that you submit, please review our <a href="#"> Privacy Notice</a>.</p>';
        $privacyConditions[22] = '<h3>Copyright Complaints</h3>';
        $privacyConditions[23] = 'IMDb respects the intellectual property of others. If you
        believe that your work has been copied in a way that constitutes copyright infringement,
        please follow our <a href = "#">Notice and Procedure for Making Claims of Copyright Infringement</a>.';
        $privacyConditions[24] = '<h3>Disclaimer of Warranties and Limitation of Liability</h3>';
        $privacyConditions[25] = '

    THE IMDB SERVICES AND ALL INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) AND OTHER SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE IMDB SERVICES ARE PROVIDED BY IMDB ON AN "AS IS" AND "AS AVAILABLE" BASIS. IMDB MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE IMDB SERVICES OR THE INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR OTHER SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE IMDB SERVICES. YOU EXPRESSLY AGREE THAT YOUR USE OF THE IMDB SERVICES IS AT YOUR SOLE RISK. IMDB RESERVES THE RIGHT TO WITHDRAW ANY IMDB SERVICE OR DELETE ANY INFORMATION FROM THE IMDB SERVICES AT ANY TIME IN ITS DISCRETION.

    TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, IMDB DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. IMDB DOES NOT WARRANT THAT THE IMDB SERVICES, INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR OTHER SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE IMDB SERVICES, ITS SERVERS, OR ELECTRONIC COMMUNICATIONS SENT FROM IMDB ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. IMDB WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF ANY IMDB SERVICE, OR FROM ANY INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR OTHER SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH ANY IMDB SERVICE, INCLUDING, BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, AND CONSEQUENTIAL DAMAGES.

    CERTAIN STATE LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MIGHT HAVE ADDITIONAL RIGHTS.
';
        return view('pages/termsAndConditionsPage')->with('privacy', $privacyConditions);
    }


}