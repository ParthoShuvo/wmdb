<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/9/2015
 * Time: 1:24 AM
 */

namespace App\Http\Controllers;


use App\Models\Theater;
use Geocoder\Provider\GoogleMaps;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Ivory\HttpAdapter\CurlHttpAdapter;

class MovieTheatersPageController extends Controller
{


    /**
     * MovieTheatersPageController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function showNearestTheaters()
    {
        try {
            $input = Input::get();
            if ($input != null && count($input) == 2
                && isset($input['latitude']) && isset($input['longitude'])
            ) {
                $latitude = (double)$input['latitude'];
                $longitude = (double)$input['longitude'];
                $cityName = $this->getCityName($latitude, $longitude);
                $movieTheaters = null;
                if ($cityName != null) {
                    $theater = new Theater();
                    $movieTheaterList = $theater->getMovieTheaterList($cityName);
                    return View::make('pages/theatersListPage', [
                        'cityName' => $cityName,
                        'movieTheaterList' => $movieTheaterList
                    ]);
                } else {
                    return Redirect::route('theaterFinderPage')->with(
                        'notFoundMsg',
                        'Sorry we did\'t found any theaters in your location');
                }
            } else {
                return Redirect::route('error');
            }
        } catch (\Exception $e) {
            return Redirect::route('theaterFinderPage')->with(
                'notFoundMsg',
                'Sorry we did\'t found any theaters in your location');
        }
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return string
     */
    private function getCityName($latitude, $longitude)
    {
        $curl = new CurlHttpAdapter();
        $geoCoder = new GoogleMaps($curl);
        $userAddress = $geoCoder->reverse($latitude, $longitude)->first();
        $cityName = null;
        return $userAddress->getLocality();
    }

    public function showNowShowingMoviesInTheater($cityName, $theaterName)
    {
        if ($theaterName != null && $cityName != null) {
            $theater = new Theater();
            $theaterData = $theater->getTheaterData($theaterName, $cityName);
            $category = 'Now Showing Movies In ' .
                '<b style="color: #286090; font-weight: bold;"> (' . $theaterName . ')</b>';
            $movieList = null;
            if ($theaterData != null) {
                $movieList = $theater->getNowShowingMoviesInTheater($theaterData);
            }
            return view('pages/movieCategoryListPage',
                [
                    'category' => $category,
                    'movieList' => $movieList,
                ]);
        }
        return Redirect::route('error');
    }

    public function showFindTheaterPage()
    {
        return View::make('pages/findMovieTheaterPage');
    }

    public function showTheaterListPage()
    {
        $input = Input::all();
        $cityName = $input['cityName'];
        $theater = new Theater();
        $movieTheaterList = $theater->getMovieTheaterList($cityName);
        Session::forget('notFoundMsg');
        return View::make('pages/theatersListPage', [
            'cityName' => $cityName,
            'movieTheaterList' => $movieTheaterList
        ]);
    }


}