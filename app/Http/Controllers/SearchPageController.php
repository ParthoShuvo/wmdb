<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/23/2015
 * Time: 1:55 AM
 */

namespace App\Http\Controllers;


use App\Models\Movie;
use App\Models\TVSeries;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class SearchPageController extends Controller
{

    /**
     * SearchPageController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed|null
     */
    public function findSearchResult()
    {
        $view = null;
        $input = Input::all();
        $keyword = ucwords($input['keyword']);
        $searchCategory = $input['searchCategory'];
        switch ($searchCategory) {
            case 'Movie':
                $view = $this->searchMovie($keyword);
                break;
            case 'TV Shows':
                $view = $this->searchTVSeries($keyword);
                break;
            case 'Celeb':
                break;
        }
        return $view;
    }

    /**
     * @param $movieName
     * @return mixed
     */
    private function searchMovie($movieName)
    {

        $category = 'Results For ' . '<span style="color:#FF4D4D">"' . $movieName . '"</span>';
        $movieName = ucwords($movieName);
        $movieList = null;
        $movieList = Movie::where('movieName', 'Like', $movieName . '%')
            ->get(['movieId'])
            ->toArray();
        if ($movieList == null || count($movieList) == 0) {
            $movieList = $this->findDataFromSpiltMovieName($movieName);
        }
        $movieList = Movie::searchResultMovieList($movieList);
        $view = View::make('pages/movieCategoryListPage',
            [
                'category' => $category,
                'movieList' => $movieList,
                'searchResult' => true,
                'type' => 'Movie Search'
            ]);
        return $view;

    }

    /**
     * @param $movieName
     * @return array
     */
    private function findDataFromSpiltMovieName($movieName)
    {
        $splitMovieName = explode(' ', $movieName);
        $movieList = [];
        foreach ($splitMovieName as $movieName) {
            $tmpMovieList = Movie::where('movieName', 'Like', '%' . $movieName . '%')
                ->get(['movieId']);
            if ($tmpMovieList != null && count($tmpMovieList) > 0) {
                $movieList = array_merge($movieList, $tmpMovieList->toArray());
            }
        }
        return $movieList;
    }

    /**
     * @param $tvSeriesName
     * @return mixed
     */
    private function searchTVSeries($tvSeriesName)
    {
        $category = 'Results For ' . '<span style="color:#FF4D4D">"' . $tvSeriesName . '"</span>';
        $tvSeriesName = ucwords($tvSeriesName);
        $tvSeriesList = null;
        $tvSeriesList = TVSeries::where('seriesName', 'Like', $tvSeriesName . '%')
            ->get(['tvSeriesId'])
            ->toArray();
        if ($tvSeriesList == null || count($tvSeriesList) == 0) {
            $tvSeriesList = $this->findDataFromSpiltTVSeriesName($tvSeriesName);
        }
        $tvSeriesList = TVSeries::searchResultMovieList($tvSeriesList);
        $view = View::make('pages/movieCategoryListPage',
            [
                'category' => $category,
                'movieList' => $tvSeriesList,
                'searchResult' => true,
                'type' => 'TV Series Search'
            ]);

        return $view;
    }

    /**
     * @param $tvSeriesName
     * @return array
     */
    private function findDataFromSpiltTVSeriesName($tvSeriesName)
    {
        $splitTVSeriesName = explode(' ', $tvSeriesName);
        $tvSeriesList = [];
        foreach ($splitTVSeriesName as $tvSeries) {
            $tmpTVSeriesList = TVSeries::where('seriesName', 'Like', '%' . $tvSeries . '%')
                ->get(['tvSeriesId']);
            if ($tmpTVSeriesList != null && count($tmpTVSeriesList) > 0) {
                $tvSeriesList = array_merge($tvSeriesList, $tmpTVSeriesList->toArray());
            }
        }
        return $tvSeriesList;
    }


}