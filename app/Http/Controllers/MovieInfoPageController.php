<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/14/2015
 * Time: 4:31 AM
 */

namespace App\Http\Controllers;


use App\Models\BoxOffice;
use App\Models\CastAndCrew;
use App\Models\Feedback;
use App\Models\Movie;
use App\Models\ProfileAwardsMovies;
use App\Models\Trailer;

class MovieInfoPageController extends Controller
{


    private $actorsList = null;
    private $directorsList = null;
    private $boxOffice = null;
    private $awardList = null;
    private $movieRatingData = null;
    private $movieReviews = null;
    private $hasMovieTrailer = false;
    private $currentUserFeedback = null;


    /**
     * MovieInfoPageController constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param $movieName
     * @return \Illuminate\View\View
     */
    public function showMovieInfoView($movieName)
    {
        $movie = new Movie($movieName);
        $movieData = $movie->getMovieData();
        $dataFound = false;
        $movieReleased = true;
        if ($movieData != null) {
            $dataFound = true;
            $movieId = $movieData['movieId'];
            $this->setCastsAndCrews($movieId);
            $this->setBoxOfficeCollection($movieId);
            $this->setAwardList($movieId);
            $movieTrailer = new Trailer();
            $this->hasMovieTrailer = $movieTrailer->hasTrailer($movieData);
            $this->setMovieFeedBackData($movieId);
            $movieReleased = $this->isMovieReleased($movieData['releaseDate']);
        }

        return view('pages/movieInfoPage', [
            'dataFound' => $dataFound,
            'movieData' => $movieData,
            'directorsList' => $this->directorsList,
            'actorsList' => $this->actorsList,
            'boxOffice' => $this->boxOffice,
            'reviews' => $this->movieReviews,
            'awardList' => $this->awardList,
            'movieRating' => $this->movieRatingData,
            'hasMovieTrailer' => $this->hasMovieTrailer,
            'currentUserFeedBack' => $this->currentUserFeedback,
            'movieReleased' => $movieReleased
        ]);

    }


    /**
     * @param $movieId
     */
    public function setCastsAndCrews($movieId)
    {
        $castsAndCrews = new CastAndCrew($movieId);
        if ($castsAndCrews != null) {
            $this->actorsList = $castsAndCrews->getActorList();
            $this->directorsList = $castsAndCrews->getDirectorList();
        }
    }

    /**
     * @param $movieId
     */
    public function setBoxOfficeCollection($movieId)
    {
        $boxOfficeCollection = new BoxOffice($movieId);
        $this->boxOffice = $boxOfficeCollection->getBoxOfficeCollection();
    }

    /**
     * @param $movieId
     */
    private function setAwardList($movieId)
    {
        $movieAward = new ProfileAwardsMovies($movieId);
        $this->awardList = $movieAward->getAwardsAndNominations();
        $profileAwardList = $movieAward->getMovieProfileAwardsAndNominations();
        if ($profileAwardList != null && count($profileAwardList) > 0) {
            foreach ($profileAwardList as $profileAward) {
                array_push($this->awardList, $profileAward);
            }
        }
    }

    /**
     * @param $movieId
     */
    private function setMovieFeedBackData($movieId)
    {
        $movieFeedback = new Feedback($movieId);
        $this->movieRatingData = $movieFeedback->getRating();
        $this->movieReviews = $movieFeedback->getAllReviews();
        $this->currentUserFeedback = $movieFeedback->getUserMovieFeedBackInfo();

    }

    /**
     * @param $releaseDate
     * @return bool
     */
    private function isMovieReleased($releaseDate)
    {
        $movieReleased = true;
        $todayDate = date("Y-m-d");
        $diff = strtotime($releaseDate) - strtotime($todayDate);
        if ($diff > 0) {
            $movieReleased = false;
        }
        return $movieReleased;
    }


}