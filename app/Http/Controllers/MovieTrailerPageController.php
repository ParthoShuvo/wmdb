<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/10/2015
 * Time: 1:15 AM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\DataRetrievers\interfaces\MovieDataAttributes;
use App\Models\Movie;
use App\Models\Trailer;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


class MovieTrailerPageController extends Controller implements MovieDataAttributes
{


    /**
     * MovieTraillerPageController constructor.
     */
    public function __construct()
    {
    }

    public function showAllTrailersPage()
    {
        $movie = new Movie();
        $movieName = $movie->getLatestTrailerReleasedMovieDetails();
        return $this->showMovieTrailerPage($movieName);
    }

    /**
     * @param $movieName
     * @return mixed
     * @internal param $trailerType
     * @internal param $mediaName
     */
    public function showMovieTrailerPage($movieName)
    {
        $trailerDetails = null;
        $trailerDetails = $this->getMovieTrailerDataDetails($movieName);
        if ($trailerDetails != null) {
            return $this->showTrailerPage($trailerDetails);
        }
        return Redirect::route('error');
    }

    /**
     * @param $movieName
     * @return mixed|null
     */
    private function getMovieTrailerDataDetails($movieName)
    {
        $movie = new Movie($movieName);
        $movieData = $movie->getMovieData();
        $trailerDetails = null;
        if ($movieData != null) {
            $trailer = new Trailer();
            $trailerDetails = $this->getMovieTrailerDetails($trailer, $movieData);
        }
        return $trailerDetails;
    }

    /**
     * @param $trailer
     * @param $movieData
     * @return mixed
     */
    private function getMovieTrailerDetails(Trailer $trailer, $movieData)
    {
        $movieTrailer = $trailer->getTrailer($movieData);
        $recentMovieTrailer = $movieTrailer[0];
        $trailerDetails['movieName'] = $movieData->movieName;
        $trailerDetails['movieGenre'] = $movieData->genre;
        $trailerDetails['movieTrailerLink'] = $recentMovieTrailer->trailerCode;
        $trailerDetails['trailerNo'] = $recentMovieTrailer->trailerNo;
        $trailerDetails['movieDoc'] = $movieData->summary;
        $trailer->increaseViewCount($recentMovieTrailer['trailerId']);
        return $trailerDetails;
    }

    /**
     * @param $trailerDetails
     * @return mixed
     */
    public function showTrailerPage($trailerDetails)
    {
        $trailer = new Trailer();
        $movie = new Movie();
        $recentlyAddedTrailerList = HomePageController
            ::sliceData($trailer->getRecentlyAddedTrailers(), 0, 6);
        $popularMovieTrailerList = HomePageController
            ::sliceData($movie->getTopRankedMovies(), 0, 6);
        $popularTrailerList = HomePageController
            ::sliceData($trailer->getPopularTrailerList(), 0, 6);
        return View::make('pages/movieTrailer', [
            'trailer' => $trailerDetails,
            'recentlyTrailerList' => $recentlyAddedTrailerList,
            'popularTrailerList' => $popularTrailerList,
            'popularMovieTrailerList' => $popularMovieTrailerList
        ]);
    }


}