<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/18/2015
 * Time: 9:21 AM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface TrailerDataAttributes
{
    const trailerId = 'trailerId';
    const trailerNo = 'trailerNo';
    const trailerCode = 'trailerCode';
    const uploadDate = 'uploadDate';
    const viewCount = 'viewCount';
}