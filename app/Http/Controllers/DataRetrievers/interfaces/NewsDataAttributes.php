<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/18/2015
 * Time: 9:24 PM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface NewsDataAttributes
{
    const newsId = 'newsId';
    const newsType = 'newsType';
    const headLine = 'headLine';
    const news = 'news';
    const uploadTime = 'uploadTime';
}