<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/22/2015
 * Time: 1:46 AM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface UserDataAttributes
{
    const userIdDB = 'userID';
    const firstNameDB = 'firstName';
    const lastNameDB = 'lastName';
    const userNameDB = 'userName';
    const cityNameDB = 'cityName';
    const passwordDB = 'password';
    const birthdayDB = 'birthDate';
    const emailDB = 'email';
}