<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 1:03 PM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface TVSeriesSeasonsDataAttributes
{
    const seasonNo = 'seasonNo';
    const seasonId = 'seasonId';

}