<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/18/2015
 * Time: 1:33 AM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface MovieDataAttributes
{
    const movieId = 'movieId';
    const movieName = 'movieName';
    const releaseDate = 'releaseDate';
    const duration = 'duration';
    const genre = 'genre';
    const maturity = 'maturity';
    const summary = 'summary';
    const story = 'story';
    const imageName = 'imageName';
}