<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/22/2015
 * Time: 2:08 AM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface FeedBackDataAttributes
{
    const feedbackId = 'feedbackId';
    const rating = 'rating';
    const review = 'review';
    const watchlist = 'watchlist';
}