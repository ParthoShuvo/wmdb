<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/19/2015
 * Time: 4:57 AM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface ProfileDataAttributes
{
    const profileId = 'profileId';
    const profileName = 'name';
    const profession = 'profession';
    const birthName = 'birthName';
    const birthDate = 'birthDate';
    const birthPlace = 'birthPlace';
    const bio = 'bio';
    const height = 'height';
    const image = 'profileImage';
}