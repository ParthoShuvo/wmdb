<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 12:57 PM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface TVSeriesDataAttributes
{
    const tvSeriesId = 'tvSeriesId';
    const seriesName = 'seriesName';
    const startYear = 'startYear';
    const endYear = 'endYear';
    const duration = 'duration';
    const summary = 'summary';
    const network = 'network';
    const story = 'story';
    const maturity = 'maturity';
    const genre = 'genre';
    const imageName = 'imageName';
}