<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/11/2015
 * Time: 2:24 PM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface TheaterDataAttributes
{
    const theaterId = 'theaterId';
    const theaterName = 'theaterName';
    const theaterImage = 'theaterImage';
    const theaterAddress = 'address';
    const theaterLocality = 'locationCity';
    const noOfHalls = 'noOfHalls';
    const countryCode = 'countryCode';
    const cityName = 'locationCity';
}