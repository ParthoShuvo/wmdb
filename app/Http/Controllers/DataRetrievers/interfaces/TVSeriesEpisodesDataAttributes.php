<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 8/21/2015
 * Time: 1:04 PM
 */

namespace App\Http\Controllers\DataRetrievers\interfaces;


interface TVSeriesEpisodesDataAttributes
{
    const episodeId = 'episodeId';
    const episodeNo = 'episodeNo';
    const episodeName = 'episodeName';
    const episodeImage = 'episodeImage';
    const onAirDate = 'onAirDate';
    const summary = 'summary';
}