<?php
/**
 * Created by PhpStorm.
 * User: Shuvojit Saha Shuvo
 * Date: 7/19/2015
 * Time: 11:59 AM
 */

namespace App\Http;


use DateTime;

class DateTimeConverters
{
    /**
     * DateTimeConverters constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param $givenDate
     * @return string
     */
    static public function dateFormater($givenDate)
    {
        $date = DateTime::createFromFormat("Y-m-d", $givenDate);
        $releaseDate = $date->format('d F Y');
        return $releaseDate;
    }

    /**
     * @param $givenDate
     * @return string
     */
    static public function yearFormater($givenDate)
    {
        $date = DateTime::createFromFormat("Y-m-d", $givenDate);
        $year = $date->format('Y');
        return $year;
    }

    /**
     * @param null $date
     * @return string
     */
    static public function getYear($date = null)
    {
        if ($date == null) {
            $date = date('Y-m-d');
        }
        $dateObj = new DateTime($date);
        $year = $dateObj->format("Y");
        return $year;
    }

    /**
     * @param null $date
     * @return string
     */
    static public function getWeekNumber($date = null)
    {
        if ($date == null) {
            $date = date('Y-m-d');
        }
        $dateObj = new DateTime($date);
        $week = $dateObj->format("W");
        return $week;
    }

    public static function getDateTimeDifference($date1, $date2)
    {
        $dateTime1 = new DateTime($date1);
        $dateTime2 = new DateTime($date2);
        $interval = $dateTime1->diff($dateTime2);
        return $interval;
    }



}