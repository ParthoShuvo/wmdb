/**
 * Created by Shuvojit Saha Shuvo on 8/11/2015.
 */

function geoFindMe() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
}
function showPosition(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    window.location.href = '{!! route("nearestTheaters") !!}' + '?latitude=' + latitude + '&' + 'longitude=' + longitude;
}

