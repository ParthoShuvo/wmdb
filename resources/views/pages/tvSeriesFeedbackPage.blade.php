<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="{{URL::asset('css/contentsTemplatesStyleSheets/feedBackPageStyleSheet.css')}}"/>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{URL::asset('css/contentsTemplatesStyleSheets/movieInfoContentsStyleSheet.css')}}"/>
    <title>
        {!! 'Feedback - '. $tvSeriesData['seriesName'] !!}{!! '( '.$tvSeriesData['startYear'].' - ' !!}
        @if(isset($tvSeriesData['endYear']))
            {!! $tvSeriesData['endYear'] !!}
        @endif
        {!! ' )' !!}
    </title>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    <div id="feedBackDiv">
        <div id="movieDetailsDiv">
            <div id="movieBasicInfoDiv">
                <img src="{{URL::asset('images/tvSeries/'.$tvSeriesData['imageName'])}}">

                <div id="movieInfo">
                    <h1 id="movieName">{!! $tvSeriesData['seriesName'] !!}
                        <b style="color: #449d44; font-size: large">&nbsp;
                            {!! '( '.$tvSeriesData['startYear'].' - ' !!}
                            @if(isset($tvSeriesData['endYear']))
                                {!! $tvSeriesData['endYear'] !!}
                            @endif
                            {!! ' )' !!}
                        </b>
                    </h1>

                    <p id="movieViewInfo">
                        <b style="color: #FFFFFF;">
                            {!! 'TV Series' !!}
                        </b>
                        <b> |
                        </b>
                        @if($tvSeriesData['maturity'] != null && $tvSeriesData['maturity'] != '')
                            {!! $tvSeriesData['maturity'] !!}
                        @else
                            {!! 'No maturity added' !!}
                        @endif
                        <b> |
                        </b>
                        @if($tvSeriesData['duration'] != null && $tvSeriesData['duration'] != '')
                            {!! $tvSeriesData['duration']. ' min' !!}
                        @else
                            {!! 'Duration Not added'!!}
                        @endif
                        <b>| </b>
                        <b style="color: #449d44">
                            {!! 'on '. $tvSeriesData['network'] !!}
                        </b>
                    </p>
                    <hr/>
                    <p id="movieGenre">{!! $tvSeriesData['genre'] !!}</p>

                    <p id="movieRate">Rate:</p>
                    @if($tvSeriesRating != null && $tvSeriesRating['rate'] > 0)
                        <img src="{{URL::asset('images/staricon.png')}}"/>
                        <p id="rate">{!! $tvSeriesRating['rate'] !!}</p>
                        <p id="userNumber">from <b style="color: #449d44">
                                {!! $tvSeriesRating['totalUser'] !!}</b> users
                        </p>
                    @else
                        <p style="color: #ffffff; position: absolute; margin: 0px; padding: 0px; margin-left: 320px; margin-top: -28px; font-size: 20px;">
                            Not Rated Yet</p>
                    @endif
                    <hr/>
                    <p id="summary">{!! $tvSeriesData['summary'] !!}</p>
                </div>
            </div>
            <div id="formDiv" style="background: #ffffff;">
                <h1>@if($userFeedBackData == null)
                        Give Your FeedBack
                    @else
                        Update Your FeedBack
                    @endif
                </h1>

                <form action="{!! route('tvFeedbackAuth', [$tvSeriesData['seriesName']]) !!}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <table>
                        <tr>
                            <td><label>Your Rating:</label></td>
                            <td>@include('templates.contentsTemplate.starRatingContentTemplate')</td>
                        </tr>
                        <tr>
                            <td><label>Your Review:</label></td>
                            <td>
                                <textarea id="reviewArea" cols="70" rows="5" name="review" required></textarea>
                            </td>
                        </tr>
                        <tr colspan="2">
                            <td style="padding-top: 20px;">
                                <input type="checkbox" name="watched" value="Yes" id="checkbox"
                                       @if(isset($userFeedBackData['userWatched'])
                                       && $userFeedBackData['userWatched'] != null
                                       && $userFeedBackData['userWatched'] == 1)
                                       checked
                                        @endif
                                        />
                                <b style="font-size: 20px; color: #a94442;">add to watchlist</b>
                            </td>
                        </tr>
                    </table>
                    <input
                            style="width: 250px; height: 40px; font-size: larger; margin-top: 50px;margin-left: 270px; right: 100px; margin-bottom: 20px; background: #4c9cb2; border:0px; color: #fff; border-radius: 3px 3px 3px 3px; font-family: 'Roboto Condensed', sans-serif;"
                            type="submit" value="SAVE"/>
                </form>
            </div>
        </div>
    </div>
@endsection
</body>
</html>
