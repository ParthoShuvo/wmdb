<!DOCTYPE html>
<html>
<head>
    <title>{!! $newsType !!}</title>
    <style>
        #content {
            margin: 0px;
            padding: 20px 50px;
            width: auto;
            height: auto;
            min-height: 180px;;
            background: #FFFFFF;
            margin-top: 1px;
        }

        #content > #newsDiv {
            width: auto;
            height: auto;
            border-bottom: 1px solid #c9302c;
        }

        #content > #newsDiv > h3 {
            font-size: 25px;;
        }

        #content > h1 {
            margin: 0px;
            padding: 0px;
            font-size: 30px;
            color: #66512c;
        }

        #content > #newsDiv > p {
            font-size: 20px;
        }

        #content > #newsDiv > p > a > img {
            width: 120px;
            height: 150px;
        }

        #content > #newsDiv > #browseTopNews {
            visibility: hidden;
        }
    </style>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    <div id="content">
        <h1>{!! $newsType !!}</h1>
        <hr style="margin: 0px; margin-top: 10px;margin-bottom: 10px; border: 1px solid #a94442;"/>
        @if(isset($news) && count($news) > 0)
            @foreach($news as $topNews)
                @include('templates.contentsTemplate.latestNewsContentsTemplate')
            @endforeach
        @else
            @include('templates.notAddedYetTemplate')
        @endif
    </div>
@endsection
</body>
</html>



