<!DOCTYPE html>
<html>
<head>
    <title>Welcome To WMDB</title>
    <style>

    </style>
</head>
<body>
<div id="wrapper"> <!---start of a wrapper div-->
    @extends('templates/pageModelTemplate')
    @section('content')
        @include('templates/homePageTemplates/homePageArticleTemplate')
    @endsection
</div>
<!-- end of wrapper div  -->
</body>
</html>