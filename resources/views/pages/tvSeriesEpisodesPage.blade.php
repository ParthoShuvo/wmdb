<!DOCTYPE html>
<html>
<head>
    <title>{!! $tvSeriesName.'( Season - '. $pageNum.')' !!}</title>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    @include('templates.contentsTemplate.tvSeriesSeasonsInfoContentsTemplate')
@endsection
</body>
</html>