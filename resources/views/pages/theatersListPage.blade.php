<!DOCTYPE html>
<html>
<head>
    <title>Movie Theaters</title>
    <style>
        #content {
            margin: 0px;
            padding: 0px;
            width: auto;
            height: auto;
            min-height: 130px;
            padding-top: 5px;
            padding-bottom: 30px;
            margin-top: 1px;
            padding-left: 20px;
            padding-right: 20px;
            background: #ffffff;
        }
    </style>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    <div id="content">
        @if($movieTheaterList != null && count($movieTheaterList) > 0)
            @include('templates.contentsTemplate.theatersListTemplate')
        @endif
    </div>
@endsection
</body>
</html>



