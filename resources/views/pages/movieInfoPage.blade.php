<!DOCTYPE html>
<html>
<head>
    <title>
        @if($dataFound)
            @if(isset($movieData) && count($movieData) > 0)
                {!! $movieData['movieName'] !!}
            @else
                {!! $tvSeriesData['seriesName'] !!}
            @endif
        @else
            {!! '404 Not Found' !!}
        @endif
    </title>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    @if($dataFound)
        @if(isset($movieData) && count($movieData) > 0)
            @include('templates.contentsTemplate.movieInfoContentsTemplate')
        @else
            @include('templates.contentsTemplate.tvSeriesInfoContentsTemplate')
        @endif
    @else
        @include('templates.404ErrorTemplate')
    @endif
@endsection
</body>
</html>



