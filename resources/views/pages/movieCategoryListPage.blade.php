<!DOCTYPE html>
<html>
<head>
    <title>
        @if(isset($searchResult) && $searchResult)
            {!! 'Search Result' !!}
        @else
            {!! $category !!}
        @endif
    </title>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    @if($category == 'Box Office Collection')
        @include('templates.contentsTemplate.boxOfficeCollectionTemplate')
    @elseif($category == 'All Time Top Chart')
        @include('templates.contentsTemplate.allTimeTopChartTemplate')
    @else
        @if($category == 'Top Ranked TV Series' || $category == 'New TV Series' ||
        (isset($type) && $type == 'TV Series Search') || $category == 'Upcoming TV Series')
            @include('templates.contentsTemplate.tvSeriesListContentsTemplate')
        @else
            @include('templates.contentsTemplate.movieCategoryContentsTemplate')
            @if(isset($totalPages) && $totalPages != null && $totalPages > 1)
                @include('templates.paginationTemplate')
            @endif
        @endif
    @endif
@endsection
</body>
</html>
