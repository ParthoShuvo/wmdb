<!DOCTYPE html>
<html>
<head>
    <title>{!! $profileData['name'] !!}</title>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    @if(!$dataFound)
        @include('templates.404ErrorTemplate')
    @else
        @include('templates.contentsTemplate.profileInfoContentTemplate')
    @endif
@endsection
</body>
</html>



