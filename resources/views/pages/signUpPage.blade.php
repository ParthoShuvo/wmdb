<!DOCTYPE html>
<html>
<head>
    <title>Sign Up</title>
    <style>

        #logInBarDiv {
            visibility: hidden;
        }

    </style>
</head>
<body>
@extends('templates/pageModelTemplate')
@section('content')
    @include('templates/signInPageTemplates/signUpPageTemplate')
@endsection
</body>
</html>



