<!DOCTYPE html>
<html>
<head>
    <title>Sign Up Successful</title>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    @if(Session::has('msg') && Session::get('msg') != null)
        @include('templates.notAddedYetTemplate')
    @else
        @include('templates.signInPageTemplates.signUpNotificationTemplate')
    @endif
@endsection
</body>
</html>



