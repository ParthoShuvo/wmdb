<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" media="all" type="text/css"
          href="{!! URL::asset('css/contentsTemplatesStyleSheets/userReviewContentsTemplateStyleSheet.css') !!}"/>
</head>
<body>
<div id="movieReviewListDiv">
    @if($movieList != null && count($movieList) > 0)
        <ul>
            @foreach($movieList as $movie)
                <li>
                    <div id="reviewInfoDiv">
                        <div id="left" style="width: 112px; height: auto;">
                            <img src="{{URL::asset('images/movies/'.$movie['imageName'])}}"/>
                        </div>
                        <div id="right">
                            <div id="movieInfoDiv">
                                <div id="movieNameInfoDiv" style="margin: 0px; padding: 0px; margin-bottom: 5px;">
                                    <label id="movieNameLabel">Movie:</label>
                                    <a id="movieName"
                                       href="{!!route('moviesInfo', [$movie['movieName']])!!}">{!! $movie['movieName'] !!}
                                        <b style="color: #a94442;">&nbsp;({!! $movie['releaseYear'] !!})</b>
                                    </a>
                                </div>
                                <p style="color: #ffffff; width: 800px; margin: 0px; padding: 0px;">
                                    <label id="movieReviewLabel" style="color: #449d44;">Your Review:</label>
                                    {!! $movie['review'] !!}
                                    <b style="color: #c9302c; font-weight: bold; font-size: larger;">&nbsp;&nbsp;(reviewed
                                        on {!!($movie['feedbackDate'])!!})</b>
                                    <a href="{!! route('feedbackMovie', [$movie['movieName']]) !!}" id="button"
                                       style="margin: 0px; width: 150px; margin-top: 5px;">Update Your
                                        Review</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
            @else
                <p style="font-size: 30px; color: #8a6d3b; text-align: center; margin: 0px; padding: 0px; padding: 50px;">
                    No
                    Movies were added </p>
        </ul>
    @endif
</div>
</body>
</html>



