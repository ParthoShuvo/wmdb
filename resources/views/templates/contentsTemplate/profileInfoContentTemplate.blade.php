<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{URL::asset('css/contentsTemplatesStyleSheets/profileInfoContentTemplateStyleSheet.css')}}"/>
</head>
<body>
<div id="profileInfoDiv">
    <div id="profileBasicInfoDiv">
        @if($profileData['profession'] == 'Actor')
            <img src="{{URL::asset('images/actors/'.$profileData['profileImage'])}}">
        @else
            <img src="{{URL::asset('images/directors/'.$profileData['profileImage'])}}">
        @endif
        <div id="profileInfo">
            <h1 id="profileName">{!! $profileData['name'] !!}</h1>

            <h2 id="professionName">{!! $profileData['profession'] !!}</h2>
            <hr/>
            <p id="label">Birth Name:</p>
            @if($profileData['birthName'] != null && $profileData['birthName'] != '')
                <p id="birthName">{!! $profileData['birthName'] !!}</p>
            @else
                <p id="birthName">{!! 'Not Added Yet' !!}</p>
            @endif
            <p id="label">Date of birth:</p>
            @if($profileData['birthDate'] != null && $profileData['birthDate'] != '')
                <p id="dateOfBirth">{!! $profileData['birthDate'] !!}</p>
            @else
                <p id="dateOfBirth">{!! 'Not Added Yet' !!}</p>
            @endif
            <p id="label">Birth place:</p>
            @if($profileData['birthPlace'] != null && $profileData['birthPlace'] != '')
                <p id="birthPlace">{!! $profileData['birthPlace'] !!}</p>
            @else
                <p id="birthPlace">{!! 'Not Added Yet' !!}</p>
            @endif
            <p id="label">Height:</p>
            @if($profileData['height'] != null && $profileData['height'] != '')
                <p id="height">{!! $profileData['height'].' m' !!}</p>
            @else
                <p id="height">{!! 'Not Added Yet' !!}</p>
            @endif
        </div>
    </div>
    <div id="bioDiv">
        <h1>Biography</h1>
        @if($profileData['height'] != null && $profileData['height'] != '')
            <p>{!! $profileData['bio'] !!}</p>
        @else
            @include('templates.notAddedYetTemplate')
        @endif
    </div>
    <div id="knownForDiv">
        <h1>Known For</h1>
        @if($topMovies != null && count($topMovies) > 0)
            <ul>
                @foreach($topMovies as $movie)
                    <li>
                        <a href="{!!route('moviesInfo', $movie['movieName'])!!}">
                            <img src="{{ URL::asset('images/movies/'.$movie['imageName']) }}"/>
                        </a>
                    </li>
                @endforeach
            </ul>
        @else
            @include('templates.notAddedYetTemplate')
        @endif
    </div>
    <div id="filmographyDiv">
        <h1>Filmography</h1>
        <ul id="filmList">
            @foreach($movieData as $movie)
                <li>
                    <a id="movieName"
                       href="{!!route('moviesInfo', $movie['movieName'])!!}">
                        {!! $movie['movieName'] !!}
                    </a>

                    <p id="role">{!! $movie['role'] !!}</p>

                    <p id="releaseYear">{!! $movie['releaseYear'] !!}</p>
                </li>
            @endforeach
        </ul>
    </div>
    <div id="awardsDiv">
        <h1>Awards & Nominations</h1>
        @if($awards != null && count($awards) > 0)
            <ul id="profileInfoList">
                @foreach($awards as $award)
                    <li>
                        <p id="awardName">
                            {!! $award['awardName'] !!}
                            <b style="font-weight: bold; color: #ffffff;">
                                {!! $award['awardYear'] !!}
                            </b>
                        </p>

                        <p id="dot">&bull;&bull;&bull;</p>

                        <p id="awardCategory">{!! $award['awardCategory'] !!}
                            <b>
                                <a href="{!!route('moviesInfo', $movie['movieName'])!!}" id="movieName"
                                   style="color: #ffffff;">
                                    {!! '('.$award['movieName'].')' !!}
                                </a>
                            </b>
                        </p>
                    </li>
                @endforeach
            </ul>
        @else
            @include('templates.notAddedYetTemplate')
        @endif
    </div>
</div>
</body>
</html>



