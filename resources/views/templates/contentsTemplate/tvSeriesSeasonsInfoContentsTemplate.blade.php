<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/contentsTemplatesStyleSheets/movieCategoryContentsStyleSheet.css') }}">
</head>
<body>
<div id="content">
    <h1>
        {!! $tvSeriesName !!}<b style="color: #286090; font-weight: bold;">{!! ' (Season '.$pageNum.')' !!}</b>
    </h1>
    <hr style="margin: 0px; margin-top: -10px;margin-bottom: 10px; border: 1px solid #a94442;"/>
    <h2 style="margin: 0px; padding: 0px;">Seasons</h2>
    @include('templates.paginationTemplate')
    <div id="movieListDiv">
        <ul>
            @foreach($episodes as $episode)
                <li>
                    <img src="{{URL::asset('images/episodes/'.$episode['episodeImage'])}}"/>

                    <div id="movieInfoDiv">
                        <div id="movieNameInfoDiv">
                            <label id="movieNameLabel">Episode Name:</label>
                            <a id="movieName">{!! $episode['episodeName'] !!}
                                <b style="color: #a94442;">
                                    &nbsp;({!! $episode['onAirDate'] !!})</b></a>
                        </div>
                        <div id="summaryDiv">
                            <p>{!! $episode['summary'] !!}</p>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>
</body>
</html>



