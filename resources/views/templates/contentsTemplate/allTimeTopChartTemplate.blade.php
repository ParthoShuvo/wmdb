<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{!! URL::asset('css/contentsTemplatesStyleSheets/boxOfficeCollectionContentTemplateStyleSheet.css') !!}">
</head>
<body>
<div id="containerDiv">
    <div class="left">
        <div id="boxOfficeDiv">
            <h1>{!! $category !!}</h1>
            <hr style="margin: 0px; margin-top: 10px;margin-bottom: 10px; border: 1px solid #a94442;"/>
            @if($allTimeTopChart != null && count($allTimeTopChart) > 0)
                <table>
                    <tr id="label">
                        <td colspan="2" width="400px;">
                            <p style="margin: 0px; padding: 0px; margin-left: 50px;">Title</p>
                        </td>
                        <td>
                            <p style="margin: 0px; padding: 0px;">Weekend</p>
                        </td>
                        <td>
                            <p style="margin: 0px; padding: 0px; margin-left: 20px;">Gross</p>
                        </td>
                        <td>
                            <p style="margin: 0px; padding: 0px; margin-left: 10px;">Rate</p>
                        </td>
                    </tr>
                </table>
                <ul>
                    @for($i = 0; $i < count($allTimeTopChart); $i++)
                        <li
                                @if($i % 2 != 0)
                                style="background: #B2B2B2;"
                                @endif
                                >
                            <table cellpadding="0" cellspacing="0">
                                <tr id="movieInfoRow">
                                    <td colspan="2" style="width: 400px;">
                                        <div>
                                            <img src="{!!URL::asset('images/movies/'.$allTimeTopChart[$i]['imageName']) !!}"/>
                            <span>
                            <p>
                                <a href="{!!route('moviesInfo', [$allTimeTopChart[$i]['movieName']]) !!}">
                                    {!!$allTimeTopChart[$i]['movieName'] !!}</a>
                                <b style="color: #28343b; font-weight: bold;">
                                    {!! '('.$allTimeTopChart[$i]['releaseYear'].')' !!}
                                </b>
                            </p>
                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <p style="width: 70px; margin-top: -10px; color: #131313; text-align: center;">
                                            {!! '$' . round($allTimeTopChart[$i]['weekendCollection'],2).'M'!!}
                                        </p>
                                    </td>
                                    <td>
                                        <p style="width: 60px; margin-left: 20px; margin-top: -10px; color: #2e3436; text-align: center;">
                                            {!! '$' . round($allTimeTopChart[$i]['gross'], 2).'M'!!}
                                        </p>
                                    </td>
                                    <td>
                                        <p style="margin-left: 20px; margin-top: -10px; color: #449d44; float: right; font-weight: bold;">
                                            {!! round($allTimeTopChart[$i]['rate'],1)!!}
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </li>
                    @endfor
                </ul>
            @else
                @include('templates.notAddedYetTemplate')
            @endif
        </div>
    </div>
    <div class="right">
        @include('templates.sideBarTemplates.sideBarTemplate')
    </div>
</div>
</body>
</html>



