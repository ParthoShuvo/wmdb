<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/contentsTemplatesStyleSheets/movieCategoryContentsStyleSheet.css') }}">
    <script>
        function geoFindMe() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
        }
        function showPosition(position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            window.location.href = '{!! route("nearestTheaters") !!}' + '?latitude=' + latitude + '&' + 'longitude=' + longitude;
        }
    </script>
    <style>
        #findTheaterNotifyDiv {
            width: auto;
            height: 50px;
            background: #8a6d3b;
            margin: 0px;
            padding: 0px;
            border-radius: 10px;
            padding: 5px 50px;
        }

        #findTheaterNotifyDiv > p {
            margin: 0px;
            padding: 0px;
            font-size: 30px;
            text-align: center;
        }

        #findTheaterNotifyDiv > p > a {
            text-decoration: none;
            color: #ffffff;
        }

        #findTheaterNotifyDiv > p > a:hover {
            text-decoration: underline solid #337ab7;
            font-weight: bold;
        }
    </style>


</head>
<body>
<div id="content">
    @if($category == 'Now Showing Movies' || $category == 'Opening This Week')
        <div id="findTheaterNotifyDiv">
            <p>Click <a href="javascript:void(0)" onclick="return geoFindMe()">here</a> to find out your nearest movie
                theaters</p>
        </div>
    @endif
    <h1>
        {!! $category !!}
        @if($category == 'Now Showing Movies'|| $category == 'Opening This Week')
            <b style="color: #286090; font-weight: bold;"> (In Theaters)</b>
        @endif
    </h1>
    <hr style="margin: 0px; margin-top: -10px;margin-bottom: 10px; border: 1px solid #a94442;"/>
    @if(isset($dateWiseMovieList))
        @include('templates.contentsTemplate.dateWiseMovieContentsTemplate')
    @elseif($category == 'Your Reviews')
        @include('templates.contentsTemplate.userMovieReviewsContentsTemplate')
    @else
        <div id="movieListDiv">
            @if($movieList != null && count($movieList) > 0)
                <ul>
                    @foreach($movieList as $movie)
                        <li>
                            <img src="{{URL::asset('images/movies/'.$movie['imageName'])}}"/>

                            <div id="movieInfoDiv">
                                <div id="movieNameInfoDiv">
                                    <label id="movieNameLabel">Movie:</label>
                                    <a id="movieName"
                                       href="{!!route('moviesInfo', [$movie['movieName']])!!}">{!! $movie['movieName'] !!}
                                        <b
                                                style="color: #a94442;">
                                            &nbsp;({!! $movie['releaseYear'] !!})</b></a>
                                </div>
                                @if($category == 'Your Ratings')
                                    <div id="movieRateInfoDiv">
                                        <label id="movieRateLabel" style="color: #449d44;">Your Rate:</label>

                                        <p id="movieRate" style="margin-left: 100px;"><img
                                                    src="{{URL::asset('images/staricon.png')}}"/><b
                                                    style="position: absolute; margin: 0px; margin-left: 5px; ">

                                                {!! round($movie['rate'], 1) !!}

                                            </b>
                                            <a href="{!! route('feedbackMovie', [$movie['movieName']]) !!}" id="button">Update
                                                Your Rating</a>
                                        </p>
                                    </div>
                                @else
                                    <div id="movieRateInfoDiv">
                                        <label id="movieRateLabel">Rate:</label>

                                        <p id="movieRate"><img src="{{URL::asset('images/staricon.png')}}"/><b
                                                    style="position: absolute; margin: 0px; margin-left: 5px; ">
                                                @if($movie['rate'] == 0.0)
                                                    {!! 'Not rated yet' !!}
                                                @else
                                                    {!! round($movie['rate'], 1) !!}
                                                @endif
                                            </b>
                                        </p>
                                    </div>

                                @endif
                                <div id="summaryDiv">
                                    <p>{!! $movie['summary'] !!}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @else
                <p style="font-size: 30px; color: #8a6d3b; text-align: center; margin: 0px; padding: 0px; padding: 50px;">
                    No
                    Movies were added </p>
            @endif
        </div>
    @endif
</div>
</body>
</html>



