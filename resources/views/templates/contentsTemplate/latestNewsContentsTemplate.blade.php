<!DOCTYPE html>
<html>
<head>
    <style>
        #newsDiv {
            margin: 0px;
            padding: 0px;
            width: 648px;
            height: 306px;
        }

        #newsDiv > h3 {
            margin: 0px;
            padding: 0px;
            color: crimson;
            text-align: justify;
            padding: 0px 10px;
            padding-right: 20px;
            margin-bottom: 10px;
        }

        #newsDiv > p {
            margin: 0px;
            padding: 0px;
            font-size: 15px;
            animation: none;
            text-align: justify;
            padding: 0px 10px;
            padding-right: 20px;
            margin-bottom: 10px;
        }

        #newsDiv > p > a > img {
            float: left;
            margin: 0px;
            padding: 0px;
            width: 100px;
            height: 120px;
            margin-right: 10px;
        }

        #newsDiv > p > a {
            margin: 0px;
            padding: 0px;
            text-decoration: none;
        }

        #newsDiv > a {
            margin: 0px;
            padding: 0px;
            color: #4c9cb2;
            position: absolute;
            text-decoration: none;
            margin-top: 10px;
        }

    </style>
</head>
<body>
<div id="newsDiv">
    @if(isset($topNews))
        <h3>{!! $topNews['headLine'] !!}</h3>
        <p>
            @if($topNews['newsType'] == 'Movie')
                <a href="{!! route('moviesInfo', [$topNews['movieName']]) !!}">
                    <img src="{!! URL::asset('images/movies/'.$topNews['imageName'])!!}"/>
                </a>
            @else
                <a href="{!!route('profileInfo', ['profileType' => $topNews['profession'], 'profileName' => $topNews['name']])!!}">
                    <img src="@if($topNews['profession'] == 'Actor')
                    {!! URL::asset('images/actors/'.$topNews['profileImage'])!!}
                    @else
                    {!! URL::asset('images/directors/'.$topNews['profileImage'])!!}
                    @endif"/>
                </a>
                {!! $topNews['news'] !!}
            @endif
        </p>

        <base target="_parent"/>
        <p id="browseTopNews">
            @if($newsType == 'Top News')
                <a href="{!! route('news', ['newsType' => $newsType]) !!}" target="_parent">Show More Top News >></a>
            @elseif($newsType == 'Movie News')
                <a href="{!! route('news', ['newsType' => $newsType]) !!}" target="_parent">Show More Movie News >></a>
            @elseif($newsType == 'Celeb News')
                <a href="{!! route('news', ['newsType' => $newsType]) !!}" target="_parent">Show More Celeb News >></a>
            @else
                <a href="{!! route('news', ['newsType' => $newsType]) !!}" target="_parent">Show More TV News >></a>
            @endif
        </p>
    @else
        @include('templates.notAddedYetTemplate')
    @endif
</div>
</body>
</html>



