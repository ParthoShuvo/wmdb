<!DOCTYPE html>
<html>
<head>
</head>
<body>
@if($dateWiseMovieList != null && count($dateWiseMovieList) > 0)
    @foreach($dateWiseMovieList as $movieList)
        <h2 style="color: #c9302c; font-size: 20px; margin: 0px; padding: 0px; padding: 10px 20px;">
            {!! $movieList['releaseDate'] !!}
        </h2>
        <div id="movieListDiv">
            <ul>
                @foreach($movieList['movieList'] as $movie)
                    <li>
                        <img src="{{URL::asset('images/movies/'.$movie['imageName'])}}"/>

                        <div id="movieInfoDiv">
                            <div id="movieNameInfoDiv">
                                <label id="movieNameLabel">Movie:</label>
                                <a id="movieName"
                                   href="{!!route('moviesInfo', [$movie['movieName']])!!}">{!! $movie['movieName'] !!}
                                    <b
                                            style="color: #a94442;">
                                        &nbsp;({!! $movie['releaseYear'] !!})</b></a>
                            </div>
                            <div id="movieRateInfoDiv">
                                <label id="movieRateLabel">Rate:</label>

                                <p id="movieRate"><img src="{{URL::asset('images/staricon.png')}}"/><b
                                            style="position: absolute; margin: 0px; margin-left: 5px; ">
                                        @if($movie['rate'] == 0.0)
                                            {!! 'Not rated yet' !!}
                                        @else
                                            {!! round($movie['rate'], 1) !!}
                                        @endif
                                    </b>
                                </p>
                            </div>
                            <div id="summaryDiv">
                                <p>{!! $movie['summary'] !!}</p>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    @endforeach
@else
    @include('templates.notAddedYetTemplate')
@endif
</body>
</html>



