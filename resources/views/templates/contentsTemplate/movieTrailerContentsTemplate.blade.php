<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/contentsTemplatesStyleSheets/movieTrailerContentsStyleSheet.css') }}">
</head>
<body>
<div id="content">
    <h1>Trailers</h1>

    <div id="videoFrameDiv">
        @if(isset($trailer['movieName']))
            <div>
                <h3><span>{!! $trailer['movieName'] !!}</span></h3>
                <b style="color: #2e6da4; font-weight: bolder; font-size: 20px;">{!!'Trailer No. #'.$trailer['trailerNo'] !!}</b>
                <h4><span>{!! $trailer['movieGenre'] !!}</span></h4>

                <p><span>{!! $trailer['movieDoc'] !!}</span></p>
            </div>
            <iframe id="videoPlayer" width="560" height="315"
                    src="{!! 'https://www.youtube.com/embed/'.$trailer['movieTrailerLink'].'?autoplay=1' !!}"
                    frameborder="0"
                    allowfullscreen>
            </iframe>
        @else
            <div>
                <h3><span>{!! $trailer['seriesName'].' - Season '.$trailer['seasonNo'] !!}</span></h3>
                <b style="color: #2e6da4; font-weight: bolder; font-size: 20px;">{!!'Trailer No. #'.$trailer['trailerNo'] !!}</b>
                <h4><span>{!! $trailer['genre'] !!}</span></h4>

                <p><span>{!! $trailer['summary'] !!}</span></p>
            </div>
            <iframe id="videoPlayer" width="560" height="315"
                    src="{!! 'https://www.youtube.com/embed/'.$trailer['trailerCode'].'?autoplay=1' !!}"
                    frameborder="0"
                    allowfullscreen>
            </iframe>
        @endif
    </div>
    <div id="latestTrailerDiv">
        <h2 style="padding: 10px;"><a href="#">Recently Added</a></h2>
        <ul>
            @foreach($recentlyTrailerList as $recentTrailer)
                <li>
                    <a href="{!!route('movie_trailer_route', [$recentTrailer['movieName']])!!}">
                        <img src="{{ URL::asset('images/movies/'.$recentTrailer['imageName']) }}"/>
                    </a>
                    <a id="playButton" href="#">
                        <img style="width: 70px; height: 70px; margin-left: 40px;"
                             src="{{ URL::asset('images/playButton.png') }}"/>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    <div id="popularTrailersDiv">
        <h1 style="padding: 0px; margin: 0px;"><a href="#">Popular Trailers</a></h1>
        <ul>
            @foreach($popularTrailerList as $popularTrailer)
                <li>
                    <a href="{!! route('movie_trailer_route', [$popularTrailer['movieName']]) !!}">
                        <img src="{{ URL::asset('images/movies/'.$popularTrailer['imageName']) }}"/>
                    </a>
                    <a id="playButton" href="{!! route('movie_trailer_route', [$popularTrailer['movieName']]) !!}">
                        <img style="width: 70px; height: 70px; margin-left: 40px;"
                             src="{{ URL::asset('images/playButton.png') }}"/>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    <div id="latestTrailerDiv">
        <h2 style="padding: 10px;"><a href="#">Popular Movies</a></h2>
        <ul>
            @foreach($popularMovieTrailerList as $popularMovieTrailer)
                <li>
                    <a href="{!! route('movie_trailer_route', [$popularMovieTrailer['movieName']]) !!}">
                        <img src="{{ URL::asset('images/movies/'.$popularMovieTrailer['imageName']) }}"/>
                    </a>
                    <a id="playButton" href="{{ URL::asset('images/'.$popularMovieTrailer['movieName']) }}">
                        <img style="width: 70px; height: 70px; margin-left: 40px;"
                             src="{{ URL::asset('images/playButton.png') }}"/>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
</body>
</html>
