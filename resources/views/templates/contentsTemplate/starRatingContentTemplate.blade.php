<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="{{URL::asset('css/contentsTemplatesStyleSheets/starRatingContentStyleSheet.css')}}"/>
</head>
<body>
<fieldset class="rating">
    <input type="radio" id="star10" name="rating" value="10" required/>
    <label class="full" for="star10" title="10"></label>
    <input type="radio" id="star9half" name="rating" value="9.5" required/>
    <label class="half" for="star9half" title="9.5"></label>
    <input type="radio" id="star9" name="rating" value="9" required/>
    <label class="full" for="star9" title="9"></label>
    <input type="radio" id="star8half" name="rating" value="8.5" required/>
    <label class="half" for="star8half" title="8.5"></label>
    <input type="radio" id="star8" name="rating" value="8" required/>
    <label class="full" for="star8" title="8"></label>
    <input type="radio" id="star7half" name="rating" value="7.5" required/>
    <label class="half" for="star7half" title="7.5"></label>
    <input type="radio" id="star7" name="rating" value="7" required/>
    <label class="full" for="star7" title="7"></label>
    <input type="radio" id="star6half" name="rating" value="6.5" required/>
    <label class="half" for="star6half" title="6.5"></label>
    <input type="radio" id="star6" name="rating" value="6" required/>
    <label class="full" for="star6" title="6"></label>
    <input type="radio" id="star5half" name="rating" value="5.5" required/>
    <label class="half" for="star5half" title="5.5"></label>
    <input type="radio" id="star5" name="rating" value="5" required/>
    <label class="full" for="star5" title="5"></label>
    <input type="radio" id="star4half" name="rating" value="4.5" required/>
    <label class="half" for="star4half" title="4.5"></label>
    <input type="radio" id="star4" name="rating" value="4" required/>
    <label class="full" for="star4" title="4"></label>
    <input type="radio" id="star3half" name="rating" value="3.5" required/>
    <label class="half" for="star3half" title="3.5"></label>
    <input type="radio" id="star3" name="rating" value="3" required/>
    <label class="full" for="star3" title="3"></label>
    <input type="radio" id="star2half" name="rating" value="2.5" required/>
    <label class="half" for="star2half" title="2.5"></label>
    <input type="radio" id="star2" name="rating" value="2" required/>
    <label class="full" for="star2" title="2"></label>
    <input type="radio" id="star1half" name="rating" value="1.5" required/>
    <label class="half" for="star1half" title="1.5"></label>
    <input type="radio" id="star1" name="rating" value="1" required/>
    <label class="full" for="star1" title="1"></label>
    <input type="radio" id="starhalf" name="rating" value="0.5" required/>
    <label class="half" for="starhalf" title="0.5"></label>
</fieldset>
@if(isset($userFeedBackData['userRating']) && $userFeedBackData['userRating'] > 0)
    <script>
        function findRadioStarButton(userRate) {
            var radioBtn = null;
            switch (userRate) {
                case 0.5:
                    radioBtn = document.getElementById("starhalf");
                    break;
                case 1:
                    radioBtn = document.getElementById("star1");
                    break;
                case 1.5:
                    radioBtn = document.getElementById("star1half");
                    break;
                case 2:
                    radioBtn = document.getElementById("star2");
                    break;
                case 2.5:
                    radioBtn = document.getElementById("star2half");
                    break;
                case 3:
                    radioBtn = document.getElementById("star3");
                    break;
                case 3.5:
                    radioBtn = document.getElementById("star3half");
                    break;
                case 4:
                    radioBtn = document.getElementById("star4");
                    break;
                case 4.5:
                    radioBtn = document.getElementById("star4half");
                    break;
                case 5:
                    radioBtn = document.getElementById("star5");
                    break;
                case 5.5:
                    radioBtn = document.getElementById("star5half");
                    break;
                case 6:
                    radioBtn = document.getElementById("star6");
                    break;
                case 6.5:
                    radioBtn = document.getElementById("star6half");
                    break;
                case 7:
                    radioBtn = document.getElementById("star7");
                    break;
                case 7.5:
                    radioBtn = document.getElementById("star7half");
                    break;
                case 8:
                    radioBtn = document.getElementById("star8");
                    break;
                case 8.5:
                    radioBtn = document.getElementById("star8half");
                    break;
                case 9:
                    radioBtn = document.getElementById("star9");
                    break;
                case 9.5:
                    radioBtn = document.getElementById("star9half");
                    break;
                case 10:
                    radioBtn = document.getElementById("star10");
                    break;
                default :
                    break;
            }
            return radioBtn;
        }
        var userRate = parseFloat("{!! $userFeedBackData['userRating'] !!}");
        findRadioStarButton(userRate).checked = true;
    </script>
@endif
</body>
</html>



