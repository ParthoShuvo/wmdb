<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{URL::asset('css/contentsTemplatesStyleSheets/movieInfoContentsStyleSheet.css')}}"/>
    <style>
        #episodesDiv > ul > li {
            width: 480px;
            height: 30px;
            background: #B2B2B2;
            margin-bottom: 10px;
            padding-top: 5px;
            padding-left: 10px;
            padding-right: 20px;;
        }

        #episodesDiv > ul > li > a {
            text-decoration: none;
            color: #66512c;
        }

        #episodesDiv > ul > li > a:hover {
            text-decoration: #2e6da4 solid underline;
        }
    </style>
</head>
<body>
<div id="movieInfoDiv">
    <div id="movieBasicInfoDiv">
        <img src="{{URL::asset('images/tvSeries/'.$tvSeriesData['imageName'])}}">
        <div id="movieInfo">
            <h1 id="movieName">{!! $tvSeriesData['seriesName'] !!}
                <b style="color: #449d44; font-size: large">&nbsp;
                    {!! '( '.$tvSeriesData['startYear'].' - ' !!}
                    @if(isset($tvSeriesData['endYear']))
                        {!! $tvSeriesData['endYear'] !!}
                    @endif
                    {!! ' )' !!}
                </b>
            </h1>
            <p id="movieViewInfo">
                <b style="color: #FFFFFF;">
                    {!! 'TV Series' !!}
                </b>
                <b> |
                </b>
                @if($tvSeriesData['maturity'] != null && $tvSeriesData['maturity'] != '')
                    {!! $tvSeriesData['maturity'] !!}
                @else
                    {!! 'No maturity added' !!}
                @endif
                <b> |
                </b>
                @if($tvSeriesData['duration'] != null && $tvSeriesData['duration'] != '')
                    {!! $tvSeriesData['duration']. ' min' !!}
                @else
                    {!! 'Duration Not added'!!}
                @endif
                <b>| </b>
                <b style="color: #449d44">
                    {!! 'on '. $tvSeriesData['network'] !!}
                </b>
            </p>
            <hr/>
            <p id="movieGenre">{!! $tvSeriesData['genre'] !!}</p>
            @if($isTVSeriesOnAir)
                <p id="movieRate">Rate:</p>
                @if($tvSeriesRating != null && $tvSeriesRating['rate'] > 0)
                    <img src="{{URL::asset('images/staricon.png')}}"/>
                    <p id="rate">{!! $tvSeriesRating['rate'] !!}</p>
                    <p id="userNumber">from <b style="color: #449d44">
                            {!! $tvSeriesRating['totalUser'] !!}</b> users
                    </p>
                @else
                    <p style="color: #ffffff; position: absolute; margin: 0px; padding: 0px; margin-left: 320px; margin-top: -28px; font-size: 20px;">
                        Not Rated Yet</p>
                @endif
                @if($currentUserFeedBack == null)
                    <a href="{!! route('feedbackTVSeries', [$tvSeriesData['seriesName']]) !!}" class="button">
                        Rate This
                    </a>
                @endif
            @else
                <b style="color:#ffffff ;position: absolute; margin: 0px; padding: 0px; margin-left: 270px; margin-top: -25px; font-size: 20px;">Not
                    on air yet
                    <b style="color: #269abc; font-weight: normal;">
                        (voting begins after on air)
                    </b>
                </b>
            @endif
            <hr/>
            <p id="summary">{!! $tvSeriesData['summary'] !!}</p>
            @if($isTVSeriesOnAir)
                <a href="{!! route('feedbackTVSeries', [$tvSeriesData['seriesName']]) !!}" id="watchlist">
                    @if(isset($currentUserFeedBack) && $currentUserFeedBack['userWatched'] == 1)
                        &nbsp;{!! 'added to watchlist' !!}
                    @else
                        {!! 'Add Watchlist' !!}
                    @endif
                </a>
            @endif
            @if($hasTVSeriesTrailer)
                <a href="{!! route('tv_series_trailer_route', [$tvSeriesData['seriesName']]) !!}"
                   id="watchTrailer"
                   @if(!$isTVSeriesOnAir)style="
                           margin-left: 70px;"
                        @endif>
                    Watch Trailer
                </a>
            @endif
            @if($isTVSeriesOnAir)
                <a href="{!! route('feedbackTVSeries', [$tvSeriesData['seriesName']]) !!}" id="writeReview">
                    @if($currentUserFeedBack != null)
                        Edit Your Review
                    @else
                        Write Review
                    @endif
                </a>
            @endif
        </div>
    </div>
    <div class="left">
        <div id="episodesDiv">
            <h1>Seasons & Episodes</h1>
            @if(!$isTVSeriesOnAir)
                @include('templates.notAddedYetTemplate')
            @else
                <table style="padding-left: 20px; padding-right: 20px;">
                    <tr>
                        <td style="width: 300px; padding: 20px; color: #262626; font-size: 20px;">Seasons</td>
                        <td style="width: 300px; padding: 20px; color: #262626; font-size: 20px;">Year</td>
                    </tr>
                </table>
                <ul style="margin: 0px; list-style: none;">
                    @for($i = 0; $i < count($seasons); $i++)
                        <li style="@if($i % 2 == 0) color:#777777; @endif">
                            <a href="{!! route('tvSeriesSeasons', [$tvSeriesData['seriesName'],$i+1]) !!}">{!! 'Season '.$seasons[$i]['seasonNo'] !!}</a>
                            <a href="{!! route('tvSeriesSeasons', [$tvSeriesData['seriesName'],$i+1]) !!}"
                               style="margin-left: 210px;">{!! $seasons[$i]['startYear'] !!}</a>
                        </li>
                    @endfor
                </ul>
            @endif
        </div>
        <div id="directorInfoDiv">
            <h1>Directors</h1>
            @if($directorsList != null && count($directorsList))
                <ul id="profileInfoList">
                    @foreach($directorsList as $director)
                        <li>
                            <img src="{{URL::asset('images/directors'.$director['image'])}}"/>
                            <a id="profileName"
                               href="{!! route('profileInfo', ['profileType' => 'director', 'profileName' => $director['directorName']]) !!}">{!! $director['directorName'] !!}</a>

                            <p id="dot">&bull;&bull;&bull;</p>

                            <p id="role">{!! $director['directorPosition'] !!}</p>
                        </li>
                    @endforeach
                </ul>
            @else
                @include('templates.notAddedYetTemplate')
            @endif
        </div>
        <div id="actorsInfoDiv">
            <h1>Casts</h1>
            @if($actorsList != null && count($actorsList))
                <ul id="profileInfoList">
                    @foreach($actorsList as $actor)
                        <li>
                            <img src="{{URL::asset('images/actors/'.$actor['image'])}}"/>
                            <a id="profileName"
                               href="{!! route('profileInfo', [
                               'profileType' => 'actor',
                               'profileName' => $actor['actorName']]) !!}">
                                {!! $actor['actorName']!!}
                            </a>
                            <p id="dot"
                               style="margin: 0px; padding: 0px; margin-top: 10px; margin-left: 150px;">&bull;&bull;&bull;</p>

                            <p id="role">{!! $actor['role'] !!}
                                @if($movieData['genre'] == 'Animation')
                                    <b style="color: #ffffff">(voice)</b>
                                @endif
                            </p>
                        </li>
                    @endforeach
                </ul>
            @else
                @include('templates.notAddedYetTemplate')
            @endif
        </div>
        <div id="storyLineDiv">
            <h1>StoryLine</h1>

            <p id="storyLine">{!! $tvSeriesData['story']!!}</p>
        </div>
        <div id="awardsDiv">
            <h1 style="margin-bottom: 10px;">Awards & Nominations</h1>
            @if($awardList != null && count($awardList) > 0)
                <ul id="awardList">
                    @foreach($awardList as $award)
                        <li>
                            <p id="awardName">{!! $award['awardName'] !!}
                                <b style="color: #ffffff">
                                    {!! $award['awardYear']!!}
                                </b>
                            </p>

                            <p id="dot1">&bull;&bull;&bull;</p>

                            <p id="awardCategory">{!! $award['awardCategory'] !!}</p>
                        </li>
                    @endforeach
                </ul>
            @else
                @include('templates.notAddedYetTemplate')
            @endif
        </div>
    </div>
    <div id="rightContentDiv" class="right">
        <div id="reviewsDiv">
            <h1>Reviews</h1>
            @if($reviews == null && count($reviews) == 0)
                @include('templates.notAddedYetTemplate')
            @else
                <ul>
                    @foreach($reviews as $review)
                        <li>
                            <p id="userName">{!! $review['userName'] !!}</p>

                            <p id="review">{!! $review['review'] !!}</p>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
</body>
</html>



