<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/contentsTemplatesStyleSheets/movieCategoryContentsStyleSheet.css') }}">
</head>
<body>
<div id="content">
    <h1>
        {!! $category !!}
    </h1>
    <hr style="margin: 0px; margin-top: -10px;margin-bottom: 10px; border: 1px solid #a94442;"/>
    <div id="movieListDiv">
        @if($movieList != null && count($movieList) > 0)
            <ul>
                @foreach($movieList as $tvSeries)
                    <li>
                        <img src="{{URL::asset('images/tvSeries/'.$tvSeries['imageName'])}}"/>

                        <div id="movieInfoDiv">
                            <div id="movieNameInfoDiv">
                                <label id="movieNameLabel">TV Series:</label>
                                <a id="movieName"
                                   href="{!!route('tvSeries', ['seriesName' => $tvSeries['seriesName']])!!}">{!! $tvSeries['seriesName'] !!}
                                    <b
                                            style="color: #a94442;">
                                        &nbsp;({!! $tvSeries['startYear'].' - '!!}
                                        @if($tvSeries['endYear'] != null)
                                            {!! $tvSeries['endYear'] !!}
                                        @endif
                                        {!! ')' !!}
                                    </b>
                                </a>
                            </div>
                            <div id="movieRateInfoDiv">
                                <label id="movieRateLabel">Rate:</label>

                                <p id="movieRate"><img src="{{URL::asset('images/staricon.png')}}"/><b
                                            style="position: absolute; margin: 0px; margin-left: 5px; ">
                                        @if($tvSeries['rate'] == 0.0 || $tvSeries['rate'] == null)
                                            {!! 'Not rated yet' !!}
                                        @else
                                            {!! round($tvSeries['rate'], 1) !!}
                                        @endif
                                    </b>
                                </p>
                            </div>
                            <div id="summaryDiv">
                                <p>{!! $tvSeries['summary'] !!}</p>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        @else
            <p style="font-size: 30px; color: #8a6d3b; text-align: center; margin: 0px; padding: 0px; padding: 50px;">
                No TV Series were found
            </p>
        @endif
    </div>
</div>
</body>
</html>



