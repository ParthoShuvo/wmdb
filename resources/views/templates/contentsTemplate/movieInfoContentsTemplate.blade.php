<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{URL::asset('css/contentsTemplatesStyleSheets/movieInfoContentsStyleSheet.css')}}"/>
</head>
<body>
<div id="movieInfoDiv">
    <div id="movieBasicInfoDiv">
        <img src="{{URL::asset('images/movies/'.$movieData['imageName'])}}">

        <div id="movieInfo">
            <h1 id="movieName">{!! $movieData['movieName'] !!}
                <b style="color: #449d44; font-size: large">&nbsp;
                    (<?php use App\Http\DateTimeConverters;
                    echo DateTimeConverters::yearFormater($movieData['releaseDate']);
                    ?>)
                </b>
            </h1>

            <p id="movieViewInfo">
                @if($movieData['maturity'] != null && $movieData['maturity'] != '')
                    {!! $movieData['maturity'] !!}
                @else
                    {!! 'No maturity constraints added yet' !!}
                @endif
                <b> |
                </b>
                @if($movieData['duration'] != null && $movieData['duration'] != '')
                    {!! $movieData['duration']. ' min' !!}
                @else
                    {!! 'Duration Not added yet'!!}
                @endif
                <b>| </b>
                <b style="color: #4cae4c"><?php
                    echo DateTimeConverters::DateFormater($movieData['releaseDate']);
                    ?>
                </b>
            </p>
            <hr/>
            <p id="movieGenre">{!! $movieData['genre'] !!}</p>
            @if($movieReleased)
                <p id="movieRate">Rate:</p>
                @if($movieRating != null && $movieRating['rate'] > 0)
                    <img src="{{URL::asset('images/staricon.png')}}"/>
                    <p id="rate">{!! $movieRating['rate'] !!}</p>
                    <p id="userNumber">from <b style="color: #449d44">
                            {!! $movieRating['totalUser'] !!}</b> users
                    </p>
                @else
                    <p style="color: #ffffff; position: absolute; margin: 0px; padding: 0px; margin-left: 320px; margin-top: -28px; font-size: 20px;">
                        Not Rated Yet</p>
                @endif
                @if($currentUserFeedBack == null)
                    <a href="{!! route('feedbackMovie',  $movieData['movieName']) !!}" class="button">
                        Rate This
                    </a>
                @endif
            @else
                <b style="color:#ffffff ;position: absolute; margin: 0px; padding: 0px; margin-left: 270px; margin-top: -25px; font-size: 20px;">Not
                    released yet
                    <b style="color: #269abc; font-weight: normal;">
                        (voting begins after release)
                    </b>
                </b>
            @endif
            <hr/>
            <p id="summary">{!! $movieData['summary'] !!}</p>
            @if($movieReleased)
                <a href="{!! route('feedbackMovie',  $movieData['movieName']) !!}" id="watchlist">
                    @if(isset($currentUserFeedBack) && $currentUserFeedBack['userWatched'] == 1)
                        &nbsp;{!! 'watchlist added' !!}
                    @else
                        {!! 'Add Watchlist' !!}
                    @endif
                </a>
            @endif
            @if($hasMovieTrailer)
                <a href="{!! route('movie_trailer_route', [$movieData['movieName']]) !!}" id="watchTrailer"
                   @if(!$movieReleased)style="
                           margin-left: 70px;"
                        @endif>
                    Watch Trailer
                </a>
            @endif
            @if($movieReleased)
                <a href="{!! route('feedbackMovie',  $movieData['movieName']) !!}" id="writeReview">
                    @if($currentUserFeedBack != null)
                        Edit Your Review
                    @else
                        Write Review
                    @endif
                </a>
            @endif
        </div>
    </div>
    <div class="left">
        <div id="directorInfoDiv">
            <h1>Directors</h1>
            @if($directorsList != null && count($directorsList))
                <ul id="profileInfoList">
                    @foreach($directorsList as $director)
                        <li>
                            <img src="{{URL::asset('images/directors'.$director['image'])}}"/>
                            <a id="profileName"
                               href="{!! route('profileInfo', ['profileType' => 'director', 'profileName' => $director['directorName']]) !!}">{!! $director['directorName'] !!}</a>

                            <p id="dot">&bull;&bull;&bull;</p>

                            <p id="role">{!! $director['directorPosition'] !!}</p>
                        </li>
                    @endforeach
                </ul>
            @else
                @include('templates.notAddedYetTemplate')
            @endif
        </div>
        <div id="actorsInfoDiv">
            <h1>Casts</h1>
            @if($actorsList != null && count($actorsList))
                <ul id="profileInfoList">
                    @foreach($actorsList as $actor)
                        <li>
                            <img src="{{URL::asset('images/actors/'.$actor['image'])}}"/>
                            <a id="profileName"
                               href="{!! route('profileInfo', [
                               'profileType' => 'actor',
                               'profileName' => $actor['actorName']]) !!}">
                                {!! $actor['actorName']!!}
                            </a>

                            <p id="dot"
                               style="margin: 0px; padding: 0px; margin-top: 10px; margin-left: 150px;">&bull;&bull;&bull;</p>

                            <p id="role">{!! $actor['role'] !!}
                                @if($movieData['genre'] == 'Animation')
                                    <b style="color: #ffffff">(voice)</b>
                                @endif
                            </p>
                        </li>
                    @endforeach
                </ul>
            @else
                @include('templates.notAddedYetTemplate')
            @endif
        </div>
        <div id="storyLineDiv">
            <h1>StoryLine</h1>

            <p id="storyLine">{!! $movieData['story']!!}</p>
        </div>
        <div id="boxOfficeDiv">
            <h1>Box Office:</h1>
            @if($boxOffice != null)
                <p id="budgetLabel">Budget:</p>

                <p id="budget">{!! $boxOffice['budget'] !!}</p>

                <p id="openingWeekendLabel">Opening Weekend:</p>

                <p id="openingWeekend">{!! $boxOffice['openingWeekend'] !!}</p>

                <p id="grossLabel">Gross:</p>

                <p id="gross">{!! $boxOffice['gross'] !!}</p>
            @else
                @include('templates.notAddedYetTemplate')
            @endif

        </div>
        <div id="awardsDiv">
            <h1 style="margin-bottom: 10px;">Awards & Nominations</h1>
            @if($awardList != null && count($awardList) > 0)
                <ul id="awardList">
                    @foreach($awardList as $award)
                        <li>
                            <p id="awardName">{!! $award['awardName'] !!}
                                <b style="color: #ffffff">
                                    {!! $award['awardYear']!!}
                                </b>
                            </p>

                            <p id="dot1">&bull;&bull;&bull;</p>

                            <p id="awardCategory">{!! $award['awardCategory'] !!}</p>
                        </li>
                    @endforeach
                </ul>
            @else
                @include('templates.notAddedYetTemplate')
            @endif
        </div>
    </div>
    <div id="rightContentDiv" class="right">
        <div id="reviewsDiv">
            <h1>Reviews</h1>
            @if($reviews == null && count($reviews) == 0)
                @include('templates.notAddedYetTemplate')
            @else
                <ul>
                    @foreach($reviews as $review)
                        <li>
                            <p id="userName">{!! $review['userName'] !!}</p>

                            <p id="review">{!! $review['review'] !!}</p>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
</body>
</html>
