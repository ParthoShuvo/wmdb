<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" media="all" type="text/css"
          href="{!! URL::asset('css/contentsTemplatesStyleSheets/theatersListTemplateStyleSheet.css') !!}"/>
</head>
<body>
<h1>Theaters In (
    <b style="color: #286090; font-weight: bold;">
        {!! $cityName !!}
    </b>)
</h1>
<hr style="margin: 0px; margin-top: -10px;margin-bottom: 10px; border: 1px solid #a94442;"/>
<ul id="theatersList">
    @foreach($movieTheaterList as $movieTheater)
        <li>
            <img src="{!! URL::asset('images/theaters/'.$movieTheater['theaterImage']) !!}"/>

            <div id="theaterInfoDiv">
            <span>
                <label>Theater Name:</label>
                <a href="{!! $movieTheater['webSite'] !!}">{!! $movieTheater['theaterName'] !!}</a>
            </span>
                <br/>
            <span>
                <label>Address:</label>
                <b>{!! $movieTheater['address'] !!}</b>
            </span>
                <br/>
                <a class="button" href="{!! route('nowShowingMoviesInTheater', [
                $movieTheater['locationCity'], $movieTheater['theaterName']]) !!}">
                    Now Showing Movies
                </a>
            </div>
        </li>
    @endforeach
</ul>
</body>
</html>



