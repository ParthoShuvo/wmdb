<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" media="all" type="text/css"
          href="{!! URL::asset('css/contentsTemplatesStyleSheets/theaterFinderTemplateStyleSheet.css') !!}"/>
</head>
<body>
<div id="containerDiv">
    @if(Session::has('notFoundMsg') && !empty(Session::get('notFoundMsg')))
        <h1 style="font-size: 25px; color: #c9302c;">{!! Session::get('notFoundMsg') !!}</h1>
        <hr style="margin: 0px; margin-top: -10px;margin-bottom: 10px; border: 1px solid #a94442;"/>
    @endif
    <h1 style="font-size: 30px; color: #2e6da4; font-weight: bolder;">Select City To Find Theater</h1>

    <div id="theaterFinderDiv">
        <form action="{!! route('findTheater') !!}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <table>
                <tr>
                    <td>
                        <input type="text" id="cityName" name="cityName"
                               placeholder="Enter City Name" pattern="[A-Za-z]+"
                               required title="input must be alphabet"
                                />
                    </td>
                    <td>
                        <select name="countryCode">
                            <option value="US">US</option>
                            <option value="BD">BD</option>
                            <option value="IN">IN</option>
                            <option value="UK">UK</option>
                            <option value="CA">CA</option>
                        </select>
                    </td>
                    <td>
                        <input type="SUBMIT" id="submitBtn" value="Find"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>



