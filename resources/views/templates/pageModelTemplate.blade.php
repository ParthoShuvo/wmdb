<!DOCTYPE html>

<html>
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ URL::asset('images/favicons/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ URL::asset('images/favicons/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::asset('images/favicons/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('images/favicons/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('images/favicons/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('images/favicons/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::asset('images/favicons/apple-touch-icon-144x144.png') }}">
    <link rel="icon" type="image/png" href="{{ URL::asset('images/favicons/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ URL::asset('images/favicons/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ URL::asset('images/favicons/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="{{ URL::asset('images/favicons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="{{ URL::asset('images/favicons/mstile-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('css/wrapperStyleSheet.css') }}">
</head>
<body>
<div id="wrapper"> <!---start of a wrapper div-->
    @include('templates/headerTemplate')
    <article><!-- start of a article body-->
        @yield('content')
    </article>
    <!-- end of a article body -->
    @include('templates/footerTemplate')
</div>
<!-- end of wrapper div  -->
</body>
</html>