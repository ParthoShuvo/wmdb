<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>

    <link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('css/footerTemplateStyleSheet.css') }}">
</head>
<body>
<footer><!-- start of a footer div -->
    <nav id="socialMediaNav">
        <ul>
            <li><a href="https://www.facebook.com/imdb"><img src="{{ URL::asset('images/facebook_img.png') }}"/></a>
            </li>
            <li><a href="https://twitter.com/IMDb"><img src="{{ URL::asset('images/twitter_img.png') }}"/></a></li>
            <li><a href="https://plus.google.com/+IMDb"><img src="{{ URL::asset('images/google_plus_img.png') }}"/></a>
            </li>
        </ul>
    </nav>
    <nav id="infoNav">
        <ul>
            <li><a href="{!! route('home') !!}">Home<b style="margin-left: 15px; font-size: large; position: absolute;">|</b></a>
            </li>
            <li><a href="">About Us<b style="margin-left: 15px; font-size: large; position: absolute;">|</b></a></li>
            <li><a href="{!! route('terms') !!}" style="width: 150px;">Terms & Policies<b
                            style="margin-left: 15px; font-size: large; position: absolute;">|</b></a></li>
            <li><a href="#" style="margin-left: -15px;">Help</a></li>
        </ul>
    </nav>
    <p>&copy;NextZenTeam inc.</p>
</footer>
<!-- end of a footer div -->
</body>
</html>
