<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/homePageTemplateStyleSheets/topNewsTemplateStyleSheet.css') }}">
    <script>
        function changeColor(sender) {
            var listItem = document.getElementById('newsList').getElementsByTagName('li');
            for (var i = 0; i < listItem.length; i++) {
                var list = listItem[i];
                var link = list.querySelector('a');
                link.style.color = '#337ab7';
            }
            sender.style.color = '#111111';
        }
    </script>

</head>
<body>
<div id="topNewsDiv"><!-- start of topNewsDiv -->
    <h2 style="margin: 0px; padding: 0px; margin-left: 10px; float: left;">
        <a>Latest News</a></h2>
    <ul id="newsList">
        <li><a href="{!! route('latestNews', ['Top News']) !!}" target="news" onclick="changeColor(this)"
               style="color: #111111;">Top</a> |
        </li>
        <li><a href="{!! route('latestNews', ['Movie News']) !!}" target="news" onclick="changeColor(this)">Movies</a> |
        </li>
        <li><a href="{!! route('latestNews', ['TV News']) !!}" target="news" onclick="changeColor(this)">TV</a> |</li>
        <li><a href="{!! route('latestNews', ['Celeb News']) !!}" target="news" onclick="changeColor(this)">Celebs</a>
        </li>
    </ul>
    <iframe src="{!! route('latestNews', ['Top News']) !!}" name="news" scrolling="no"
            style="width: 648px; height:306px; margin: 0px; margin-top: -10px; border: none;">
    </iframe>
</div>
<!-- end of topNewsDiv -->
</body>
</html>
