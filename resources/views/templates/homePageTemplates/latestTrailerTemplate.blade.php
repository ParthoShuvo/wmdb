<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/homePageTemplateStyleSheets/latestTrailerTemplateStyleSheet.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="{{URL::asset('css/prettyPhotoLibStyleSheets/prettyPhotoStyleSheet.css')}}"
          type="text/css" media="screen" charset="utf-8"/>
    <script src="{{ URL::asset('js/jquery.prettyPhoto.js') }}" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<div id="latestTraillerDiv"><!-- start of latestTraillerDiv -->
    <h2 style="padding: 10px;"><a href="{!! route('allTrailers') !!}">Latest Trailers</a></h2>
    @if($recentlyTrailerList != null && count($recentlyTrailerList) > 0)
        <ul>
            @foreach($recentlyTrailerList as $trailer)
                <li>
                    <a href="{!! 'http://www.youtube.com/watch?v='.$trailer['trailerCode'] !!}"
                       rel="prettyPhoto">
                        @if(!isset($trailer['seriesName']))
                            <img src="{{ URL::asset('images/movies/'.$trailer['imageName']) }}"/>
                        @else
                            <img src="{{ URL::asset('images/tvSeries/'.$trailer['imageName']) }}"/>
                        @endif
                    </a>
                    <img id="playButton" src="{{ URL::asset('images/playButton.png') }}"/>
                </li>
                <script>
                    $(document).ready(function () {
                        $("a[rel^='prettyPhoto']").prettyPhoto({
                            default_width: 650,
                            default_height: 350, theme: 'facebook', horizontal_padding: 20
                        });
                    });
                </script>
            @endforeach
        </ul>
        <p id="browseMoreTraillers"><a href="{!! route('allTrailers') !!}">Browse More Trailers>></a></p>
    @else
        @include('templates.notAddedYetTemplate')
    @endif
</div>
</body>
</html>