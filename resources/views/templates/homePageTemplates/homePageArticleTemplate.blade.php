<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
@include('templates.homePageTemplates.topRankedMovieTemplate')
<div id="container" style="height: auto; min-height: 970px;">
    <div id="left" style="float: left; width: 648px; display: inline-block;">
        @include('templates.homePageTemplates.latestTrailerTemplate')
        @include('templates.homePageTemplates.nowPlayingMoviesTemplate')
        @include('templates.homePageTemplates.topNewsTemplate')
    </div>
    <div id="right" style="float: left;width: 360px; display: inline-block">
       @include('templates.sideBarTemplates.sideBarTemplate')
    </div>
</div>
</body>
</html>