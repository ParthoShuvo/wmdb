<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <meta charset="UTF-8">
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/homePageTemplateStyleSheets/topRankedMovieTemplateStyleSheet.css') }}">
</head>
<body>
<div style="width: auto; height: 50px; margin: 0px; padding: 0px; background: #28343b; margin-top: -29px;">
    <!--start of a div -->
    <p><a href="{!! route('topRankedMovies',['pageNum' => 1]) !!}">Top Ranked Movies</a></p>
</div>
<!-- end of a div -->
<div id="topRankedDiv" style="margin:0px; padding: 0px;"><!--start of topRankedDiv-->
    <a href="{!! route('moviesInfo', $topMovie['movieName']) !!}">
        <img id="topRankedImage" src="{{ URL::asset('images/movies/'.$topMovie['imageName']) }}"/>
    </a>
    <div id="topRankedImageBlurDiv"></div>
    <div id="topRankedImageInfoDiv"><!--start top ranked image info div -->
        <div id="movieTitleDiv"><!-- start of movieTitleDiv -->
            <h2 id="movieTitle">{!!$topMovie['movieName']!!}</h2>
        </div>
        <!-- end of movieTitleDiv -->
        <div id="movieRateDiv"><!-- start of movieRateDiv -->
            <img src="{{ URL::asset('images/staricon.png') }}" id="rateImage"/>

            <p id="rate">{!!round($topMovie['rate'],1)!!}</p>
        </div>
        <!-- end of movieRateDiv -->
        <p id="movieGenre">{!!$topMovie['genre']!!}</p>

        <p id="movieDoc">{!!$topMovie['summary']!!}</p>
    </div>
    <!-- end top ranked image info div -->
    <div id="topRankedMovieListDiv"><!-- start of topRankedMovieListDiv -->
        <ul id="movieList">
            @for($i = 0; $i < count($topRankedMovieList); $i++)
                <li>
                    <a href="{!! route('moviesInfo', [$topRankedMovieList[$i]['movieName']]) !!}">
                        <img src="{{ URL::asset('images/movies/'.$topRankedMovieList[$i]['imageName']) }}"/>
                    </a>

                    <div>
                        <p style="margin: 0px; padding: 0px; font-size: 20px;
                        padding: 20px; text-align: center; color: #880000; font-weight: bolder;">
                            #Rank {!! $i + 1 !!}
                        </p>
                    </div>
                </li>
            @endfor
        </ul>
        <p><a href="{!! route('topRankedMovies', ['pageNum' => 1]) !!}">Show More >></a></p>
    </div>
    <!-- end of topRankedMovieRankedDiv -->
</div>
<!--end of topRankedDiv-->
</body>