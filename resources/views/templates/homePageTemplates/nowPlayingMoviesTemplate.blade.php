<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/homePageTemplateStyleSheets/nowShowingMoviesTemplateStyleSheet.css') }}">
    <style>

    </style>
</head>
<body>
<div id="nowShowingDiv"><!-- start of nowShowingDiv -->
    <h1 id="nowShowingHeading" style="font-size: 30px;">
        <a id="nowShowing" href="{!! route('nowShowing') !!}">Now Showing</a>
    </h1>
    @if($nowShowingMovieList != null && count($nowShowingMovieList) > 0)
        <ul>
            @foreach($nowShowingMovieList as $movie)
                <li><a href="{!! route('moviesInfo', [$movie['movieName']]) !!}"><img
                                src="{{ URL::asset('images/movies/'.$movie['imageName']) }}"/></a>

                    <div>
                        <p>{!! $movie['movieName'] !!}</p>
                        @if($movie['gross'] != null && $movie['gross'] != '')
                            <p>{!! '$'.round($movie['gross']/1000000, 2).'M' !!}</p>
                        @endif
                    </div>
                </li>
            @endforeach
        </ul>
        <p id="browseMoreBoxOfficeResults">
            <a href="{!! route('boxOffice') !!}">Show More Box Office Results >></a>
        </p>
    @else
        @include('templates.notAddedYetTemplate')
    @endif
</div>
<!-- end of nowShowingDiv -->
</body>
</html>