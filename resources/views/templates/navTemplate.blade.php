<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('css/navTemplateStyleSheet.css') }}">
    <script>
        function geoFindMe() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
            else {
                window.location.href = '{!! route("theaterFinderPage") !!}';
            }
        }
        function showPosition(position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            window.location.href = '{!! route("nearestTheaters") !!}' + '?latitude=' + latitude + '&' + 'longitude=' + longitude;
        }
    </script>
    <style>
        #indicator {
            width: 10px;
            height: 5px;
            margin: 0px;
            margin-left: 5px;
            padding-bottom: 2px;
        }
    </style>
</head>
<body>
<nav><!--start of nav -->
    <ul>
        <li><a href="{!! route('home') !!}">Home</a></li>
        <li><a href="">Movies<img id="indicator" src="{{ URL::asset('images/whiteDownArrow.png') }}"/></a>
            <ul>
                <li><a href="{!!route('moviesCategory', ['category' => 'Action', 'pageNum' => 1])!!}">Action</a></li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Comedy',  'pageNum' => 1])!!}">Comedy</a></li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Romantic', 'pageNum' => 1])!!}">Romantic</a>
                </li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Drama', 'pageNum' => 1])!!}">Drama</a></li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Thriller', 'pageNum' => 1])!!}">Thriller</a>
                </li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Horror', 'pageNum' => 1])!!}">Horror</a></li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Sci-fi', 'pageNum' => 1])!!}">Sci-fi</a></li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Adventure', 'pageNum' => 1])!!}">Adventure</a>
                </li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Biography', 'pageNum' => 1])!!}">Biography</a>
                </li>
                <li><a href="{!!route('moviesCategory', ['category' => 'Animation', 'pageNum' => 1])!!}">Animation</a>
                </li>
            </ul>
        </li>
        <li><a href="">TV Series<img id="indicator" src="{{ URL::asset('images/whiteDownArrow.png') }}"/></a>
            <ul style="margin-left: -50px;">
                <li><a href="{!! route('tvSeriesList', ['pageType' => 'top-rated-tv-series', 'pageNum' => 1]) !!}"
                       style="width: 200px;">Top Rated TV Series</a></li>
                <li><a href="{!! route('tvSeriesList', ['pageType' =>'new-tv-series', 'pageNum' => 1]) !!}"
                       style="width: 200px;">New
                        TV Series</a></li>
                <li><a href="{!! route('tvSeriesList', ['pageType' => 'upcoming-tv-series', 'pageNum' => 1]) !!}"
                       style="width: 200px;">Upcoming TV Series</a></li>
            </ul>
        </li>
        <li><a href="#">News
                <img id="indicator" src="{{ URL::asset('images/whiteDownArrow.png') }}"/></a>
            <ul style="margin-left: -50px;">
                <li><a href="{!! route('news', ['newsType' => 'Top News']) !!}" style="width: 200px;">Top News</a></li>
                <li><a href="{!! route('news', ['newsType' => 'Movie News']) !!}" style="width: 200px;">Movie News</a>
                </li>
                <li><a href="{!! route('news', ['newsType' => 'TV News']) !!}" style="width: 200px;">TV News</a></li>
                <li><a href="{!! route('news', ['newsType' => 'Celeb News']) !!}" style="width: 200px;">Celeb News</a>
                </li>
            </ul>
        </li>
        <li><a href="#">Theaters
                <img id="indicator" src="{{ URL::asset('images/whiteDownArrow.png') }}"/></a>
            <ul style="margin-left: -50px;">
                <li><a href="javascript:void(0)" onclick="return geoFindMe()" style="width: 200px;">Nearest Theaters</a>
                </li>
                <li><a href="{!! route('theaterFinderPage') !!}" style="width: 200px;">Find Theaters</a></li>
            </ul>
        </li>
        <li><a href="#">BoxOffice<img id="indicator" src="{{ URL::asset('images/whiteDownArrow.png') }}"/></a>
            <ul style="margin-left: -50px;">
                <li><a href="{!! route('boxOffice') !!}" style="width: 200px;">Weekly Top Chart</a></li>
                <li><a href="{!! route('allTimeTopChart') !!}" style="width: 200px;">All Time Top Chart</a></li>
                <li><a href="{!! route('topRankedMovies', ['pageNum' => 1]) !!}" style="width: 200px;">Top 100
                        Movies</a></li>
            </ul>
        </li>
    </ul>
</nav>
<!-- end of nav -->
</body>
</html>
