<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/paginationTemplateStyleSheet.css') }}">
</head>
<body>
<div id="paginationDiv">
    <ul>
        @for($i = 0; $i < $totalPages; $i++)
            <li>
                <a href="
                @if($category == 'Top Ranked Movies')
                {!! route('topRankedMovies',['pageNum' => ($i + 1)]) !!}
                @elseif($category == 'Your WatchList')
                {!! route('watchList', [Session::get('userName'), 'pageNum' => ($i+1)]) !!}
                @elseif($category == 'Your Ratings')
                {!! route('ratings', [Session::get('userName'), 'pageNum' => ($i+1)]) !!}
                @elseif($category == 'Your Reviews')
                {!! route('reviews', [Session::get('userName'), 'pageNum' => ($i+1)]) !!}
                @elseif($category == 'TV Series')
                {!! route('tvSeriesSeasons',['seriesName' => $tvSeriesName, 'seasonNo' => ($i+1)]) !!}
                @elseif($category == 'Top Rated TV Series')
                {!! route('tvSeriesList', ['top-rated-tv-series', 'pageNum' => ($i+1)]) !!}
                @elseif($category == 'New TV Series')
                {!! route('tvSeriesList', ['new-tv-series', 'pageNum' => ($i+1)]) !!}
                @elseif($category == 'Upcoming TV Series')
                {!! route('tvSeriesList', ['upcoming-tv-series', 'pageNum' => ($i+1)]) !!}
                @else
                {!!route('moviesCategory', ['category' => $category, 'pageNum' => ($i + 1)])!!}
                @endif"
                   style="@if($pageNum == ($i + 1)) background:#269abc;@endif">
                    {!! $i + 1 !!}
                </a>
            </li>
        @endfor
    </ul>
</div>
</body>
</html>



