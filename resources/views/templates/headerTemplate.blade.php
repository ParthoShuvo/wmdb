<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('css/headerTemplateStyleSheet.css') }}">
</head>
<body>
<div id="topBar"><!-- start of a top div -->
</div>
<!--end of a top bar -->
<header><!--start of a header -->
    <div id="logoDiv"> <!-- start of a image div -->
        <a href="{!! route('home') !!}"><img src="{{ URL::asset('images/logo.png') }}" alt="Logo"/></a>
    </div>
    @include('templates.searchTemplate')
            <!-- end of a image div -->
    @if(!Session::has('isAdminLogIn') && Session::get('isAdminLogIn') == false)
        @if(!Auth::check() && !Session::has('userName'))
            <div id="logInBarDiv"><!-- start of a logInBarDiv-->
                <p><a href="{!! route('signIn') !!}" id="signIn">Sign In</a>
                    &nbsp;|&nbsp;
                    <a href="{!! route('signUp') !!}" id="signUp">Sign Up</a></p>
            </div><!-- end of a logInBarDiv -->
        @else
            @include('templates.signInPageTemplates.userLogInfoDivTemplate')
        @endif
    @endif
    @include('templates/navTemplate')
</header>
<!--end of a header -->
</body>
</html>