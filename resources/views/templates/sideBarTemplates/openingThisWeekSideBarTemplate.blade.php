<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/sideBarTemplateStyleSheets/openingThisWeekSideBarTemplateStyleSheet.css') }}">
</head>
<body>
<div id="openingThisWeekDiv"><!-- start of openingthisWeekdiv -->
    <h2><a href="{!! route('openingThisWeek') !!}">Opening This Week</a></h2>
    @if($openingThisWeekMovieList != null && count($openingThisWeekMovieList) > 0)
        <ul id="openingThisWeekMovieList">
            @foreach($openingThisWeekMovieList as $movie)
                <li>
                    <table>
                        <tr>
                            <td>
                                <img src="{{ URL::asset('images/movies/'.$movie['imageName']) }}"/>
                            </td>
                            <td>
                                <a href="{!! route('moviesInfo', [$movie['movieName']]) !!}">
                                    {!! $movie['movieName'] !!}
                                </a>
                            </td>
                        </tr>
                    </table>
                </li>

            @endforeach
            <p><a href="{!! route('openingThisWeek') !!}">Show more opening this week >></a></p>
        </ul>
    @else
        <b style="margin: 0px; padding: 20px; color: #8a6d3b; text-align: center; font-size: 20px; margin-top: 20px;">No
            Movies will release this week</b>
        <hr style="color: #8a6d3b;"/>
    @endif
</div>
<!-- end of openingthisWeekDiv -->
</body>
</html>



 