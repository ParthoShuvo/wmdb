<!DOCTYPE html>
<html>
<head>

</head>
<body>
<div id="comingSoonDiv"><!-- start of comingSoonDiv -->
    <h2><a href="{!! route('upComingMovieList') !!}">Coming Soon</a></h2>
    @if($upcomingMovieList != null && count($upcomingMovieList) > 0)
        <ul id="comingSoonMovieList">
            @foreach($upcomingMovieList as $movie)
                <li>
                    <table>
                        <tr>
                            <td>
                                <img src="{{ URL::asset('images/movies/'.$movie['imageName']) }}"/>
                            </td>
                            <td>
                                <a href="{!! route('moviesInfo', [$movie['movieName']]) !!}">
                                    {!! $movie['movieName'] !!}
                                </a>
                            </td>
                        </tr>
                    </table>
                </li>
            @endforeach
            <p><a href="{!! route('upComingMovieList') !!}">Show more coming soon movies >></a></p>
            <hr/>
        </ul>
    @else
        <b style="margin: 0px; padding: 20px; color: #8a6d3b; text-align: center; font-size: 20px; margin-top: 20px;">No
            Movies will release in upcoming days</b>
    @endif
</div>
<!-- end of comingSoonDiv -->
</body>
</html>



 