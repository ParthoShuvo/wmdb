<!DOCTYPE html>
<html>
<head>

    <style>
        aside {
            width: 359px;
            height: auto;
            min-height: 950px;
            background: rgb(238, 238, 238); /* Old browsers */
            background: -moz-linear-gradient(top, rgba(238, 238, 238, 1) 6%, rgba(204, 204, 204, 1) 54%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(6%, rgba(238, 238, 238, 1)), color-stop(54%, rgba(204, 204, 204, 1))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(238, 238, 238, 1) 6%, rgba(204, 204, 204, 1) 54%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(238, 238, 238, 1) 6%, rgba(204, 204, 204, 1) 54%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(238, 238, 238, 1) 6%, rgba(204, 204, 204, 1) 54%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(238, 238, 238, 1) 6%, rgba(204, 204, 204, 1) 54%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#eeeeee', endColorstr='#cccccc', GradientType=0); /* IE6-9 */
            padding-top: 15px;
            border-left: 1px #cc6600 solid;
            padding-bottom: 15px;
        }
    </style>
</head>
<body>
<aside><!--aside starts-->
    @include('templates/sideBarTemplates/openingThisWeekSideBarTemplate')
    @include('templates/sideBarTemplates/upComingMoviesSideBarTemplate')
</aside>
<!-- aside ends-->
</body>
</html>



 