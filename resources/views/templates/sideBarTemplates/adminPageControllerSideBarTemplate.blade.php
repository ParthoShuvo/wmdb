<!DOCTYPE html>
<html>
<head>
    <script>
        function changeColor(sender) {
            var listItem = document.getElementById('controllerList').getElementsByTagName('li');
            for (var i = 0; i < listItem.length; i++) {
                var list = listItem[i];
                var link = list.querySelector('a');
                link.style.color = '#FFFFFF';
            }
            sender.style.color = '#843534';
        }
    </script>

    <style>
        #left {
            float: left;
            width: 250px;
            background: #555555;
            height: 640px;
            border-right: 3px solid #8a6d3b;
            border-radius: 2px 0px 0px 2px;
            padding-left: 20px;
            padding-right: 20px;
            padding-top: 30px;
            padding-bottom: 30px;
        }

        #controllerList {
            margin: 0px;
            padding: 0px;

            list-style: none;

        }

        #controllerList > li {
            margin: 0px;
            padding: 0px;
            padding: 5px 0px;

        }

        #controllerList > li > a {
            font-size: 20px;
            text-decoration: none;
            color: #FFFFFF;
            display: block;
            font-weight: bolder;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            border-bottom: 1px solid #BE5C00;
            padding-bottom: 5px;
        }

        #controllerList > li > a:hover {
            color: #8a6d3b;
        }


    </style>
</head>
<body>
<div id="left">
    <ul id="controllerList">
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'movie']) !!}" target="insertData"
               style="color: #843534;" onclick="changeColor(this)">Movies</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'movie-trailer']) !!}"
               target="insertData" onclick="changeColor(this)">Movie Trailers</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'box-office']) !!}" target="insertData"
               onclick="changeColor(this)">Box Office Collection</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'movie-award']) !!}" target="insertData"
               onclick="changeColor(this)">Movie Awards</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'profile']) !!}" target="insertData"
               onclick="changeColor(this)">Profiles</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'movie-cast-and-crew']) !!}"
               target="insertData" onclick="changeColor(this)">Cast And Crew Movies</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'news']) !!}" target="insertData"
               onclick="changeColor(this)">News</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'movie-news']) !!}" target="insertData"
               onclick="changeColor(this)">Movie news</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'movie-theaters']) !!}"
               target="insertData" onclick="changeColor(this)">Movie Theaters</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'celeb-news']) !!}" target="insertData"
               onclick="changeColor(this)">Celeb News</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'now-showing']) !!}" target="insertData"
               onclick="changeColor(this)">Now Showing</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'tv-series']) !!}" target="insertData"
               onclick="changeColor(this)">TV Series</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'tv-series-seasons']) !!}"
               target="insertData" onclick="changeColor(this)">TV Series Seasons</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'tv-series-episodes']) !!}"
               target="insertData" onclick="changeColor(this)">TV Series Episodes</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' => 'tv-series-trailer']) !!}"
               target="insertData" onclick="changeColor(this)">TV Series Trailers</a></li>
        <li><a href="{!! route('movieInsertDataPage', ['insertDataPageType' =>'movie-cast-and-crew-award'])!!}"
               target="insertData" onclick="changeColor(this)">Profile Awards Movies</a></li>
    </ul>
</div>
</body>
</html>



