<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/searchTemplateStyleSheet.css') }}">
</head>
<body>
<div id="searchDiv">
    <form action="{!! route('search') !!}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <table cellspacing="0" cellpadding="0px">
            <tr>
                <td>
                    <input type="search" name="keyword" placeholder="Find Movies, TV Shows, Celebrities . . . . . . . ."
                           required/>
                </td>
                <td>
                    <select name="searchCategory">
                        <option value="Movie">Movie</option>
                        <option value="TV Shows">TV Shows</option>
                        <option value="Celeb">Celeb</option>
                    </select>
                </td>
                <td>
                    <input type="image" src="{!! URL::asset('images/searchImage.png') !!}" alt="Submit"
                           style="width: 30px; height: 30px; padding: 0px; margin: 0px; margin-left: 0.5px;
                           margin-top: 2px; border-radius: 0px 2px 2px 0px;">
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>



