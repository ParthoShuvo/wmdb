<!DOCTYPE html>
<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/signInPageTemplateStyleSheets/signInPageTemplateStyleSheet.css') }}">

    <style>
        #wrongInputNotify {
            color: #d9534f;
            text-align: center;
            visibility: visible;
        }
    </style>
</head>
<body>

<div id="logInDiv"><!-- start of log in div -->
    <form action="{{ route('signInAuth')}}" method="post" autocomplete="off">
        <hr id="leftHr"/>
        <p id="logInTitleDiv">LOG IN</p>
        <hr id="rightHr"/>
        @if($errors->has('userName'))
            <p id="wrongInputNotify">{!! 'user name '.$errors->first('userName')!!}</p>
        @elseif($errors->has('password'))
            <p id="wrongInputNotify">{!! 'password '.$errors->first('password')!!}</p>
        @elseif(Session::has('flash_msg') && Session::get('flash_msg') != null)
            <p id="wrongInputNotify">{!! Session::get('flash_msg') !!}</p>
        @endif
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div id="userInput">
            <input type="text" name="userName" id="userName" placeholder="USER NAME"
                   pattern="^(?=^.{8,}$)([a-zA-Z]+[0-9][a-zA-Z0-9]*|[0-9]+[a-zA-Z][a-zA-Z0-9]*)$"
                   required
                   title="Only alphabetic characters are accepted"/>
            <input type="password" name='password' id="password" placeholder="PASSWORD" pattern=".{8,}"
                   required
                   title="Minimum length of the password is 8"/>
        </div>
        <div id="userInteractionDiv"><!--start of userInteractionDiv -->
            <p><input type="checkbox" name="rememberMe" value="rememberMe">&nbsp;&nbsp;Remember Me</p>

            <p style="float: right; margin-top: -36px; margin-right: 5px;"><a href="#">Forget
                    Password</a>&nbsp;|&nbsp;{!!link_to('signUp', 'Create New Account')!!}</p>
        </div>
        <!--end of userInteractionDiv -->
        <input
                style="width: 250px; height: 40px; font-size: larger; margin-left: 100px; right: 100px; margin-bottom: 20px; background: #4c9cb2; border:0px; color: #fff; border-radius: 3px 3px 3px 3px; font-family: 'Roboto Condensed', sans-serif;"
                type="submit" value="LOG IN"/>
    </form>
</div>
<!-- end of log in div -->
</body>
</html>



 