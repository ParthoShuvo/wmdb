<!DOCTYPE html>
<html>
<head>

    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/signInPageTemplateStyleSheets/signUpPageTemplateStyleSheet.css') }}">
    <style>
        #wrongInputNotify {
            color: #d9534f;
            text-align: center;
            visibility: visible;
        }
    </style>
</head>
<body>
<div id="signUpDiv"><!-- start of sign Up div -->
    <form action="{!! route('createUser') !!}" method="post" autocomplete="off">
        <p id="singUpTitleDiv">Sign Up<b style="color: #fff; font-weight: normal"> (It's Free)</b></p>

        <p style="float: right; margin-top: -58px; margin-right: 15px; color: red;">*Required All fields</p>
        @if($errors->has('email'))
            <p id="wrongInputNotify">
                {!! 'email address'.$errors->first('email') !!}
            </p>
        @endif
        @if($errors->has('userName'))
            <p id="wrongInputNotify">
                {!! 'user name'.$errors->first('userName') !!}
            </p>
        @endif
        @if($errors->has('confirmPassword'))
            <p id="wrongInputNotify">
                {!! $errors->first('confirmPassword') !!}
            </p>
        @endif
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <table>
            <tr>
                <td><input type="text" name='firstName' id="firstName" placeholder="FIRST NAME"
                           pattern="[a-zA-Z ]+"
                           required title="Only alphabetic characters are accepted"/>
                </td>
                <td><input type="text" name='lastName' id="lastName" placeholder="LAST NAME"
                           pattern="[a-zA-Z ]+"
                           required title="Only alphabetic characters are accepted"/></td>
            </tr>
            <tr>
                <td><input type="email" name="email" id="email" placeholder="EMAIL" required/></td>
            </tr>
            <tr>
                <td><input type="text" name="cityName" id="cityName" placeholder="City" pattern="[a-zA-Z ]+"
                           required
                           title="Only alphabetic characters are accepted"/></td>
            </tr>
            <tr>
                <td><input type="text" name="userName" id="userName" placeholder="USER NAME"
                           pattern="^(?=^.{8,}$)([a-zA-Z]+[0-9][a-zA-Z0-9]*|[0-9]+[a-zA-Z][a-zA-Z0-9]*)$"
                           required
                           title="Only alphabetic characters are accepted"/></td>
            </tr>
            <tr>
                <td><input type="password" name='password' id="password" placeholder="PASSWORD" pattern=".{8,}"
                           required
                           title="Minimum length of the password is 8"/>
                </td>
                <td><input type="password" name='confirmPassword' id="confirmPassword"
                           placeholder="CONFIRM PASSWORD"
                           pattern=".{8,}" required title="Minimum length of the password id 8"/></td>
            </tr>
            <tr>
                <td><input type="text" name="birthDate" id="date" placeholder="BIRTHDAY: yyyy-mm-dd"
                           pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" required
                           title="Birthday should be dd-mm-yyyy format"/>
                </td>
            </tr>
            <tr>
                <td style="color: #fff;"><input type="checkbox" name="checkedPolicy" value="policeyChecked"
                                                required/>I
                    accept <a
                            href="#">terms and conditions</a></td>
            </tr>
            <tr>
                <td><p style="margin-left: 5px;">{!!link_to('signIn', 'Already have an account?')!!}</td>
            </tr>
        </table>
        <input style="width: 250px; height: 40px; font-size: larger; margin-left: 100px; right: 100px; margin-bottom: 20px; background: #4c9cb2; border:0px; color: #fff; border-radius: 3px 3px 3px 3px;"
               type="submit" value="SIGN UP"/>
    </form>
</div>
<!-- end of sign Up div -->
</body>
</html>



