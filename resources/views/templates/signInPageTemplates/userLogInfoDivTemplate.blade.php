<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{URL::asset('css/signInPageTemplateStyleSheets/userLogInInfoDivStyleSheet.css')}}"/>
</head>
<body>
<div id="userLogInInfoDiv"><!-- start of a user log in info div -->

    <p><b style="color: #66afe9;">{!! session('userName') !!}</b></p>
    <ul>
        <li><a href="#" id="controller">
                <img id="userIcon" src="{{ URL::asset('images/userIcon.png') }}"/>
            </a>
            <ul>
                <li><a href="#" style="border-top: none">My Profile</a></li>
                <li><a href="{!! route('ratings', [Session::get('userName'), 'pageNum' => 1]) !!}">My Ratings</a></li>
                <li><a href="{!! route('reviews', [Session::get('userName'), 'pageNum' => 1]) !!}">My Reviews</a></li>
                <li><a href="{!! route('watchList', [Session::get('userName'), 'pageNum' => 1]) !!}">My watchList</a>
                </li>
                <li><a href="{!! route('logOut')!!}">Log Out</a></li>
            </ul>
        </li>
    </ul>
</div>
<!-- end of a user log in info div -->
</body>
</html>



