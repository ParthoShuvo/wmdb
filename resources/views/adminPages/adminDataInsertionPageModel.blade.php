<!DOCTYPE html>
<html>
<head>
    <style>
        #formDiv {
            margin: 0px;
            padding: 0px;
            width: 690px;
            height: 700px;
            background: #FFFFFF;
        }
    </style>
</head>
<body>
<div id="formDiv">
    <h1 style="color: #843534; text-align: center; padding: 0px; margin: 0px; ">{!! ucfirst($pageType) !!} Data
        Insert</h1>
    <hr style="width: 650px; border: 1px solid #8a6d3b;"/>
    <div id="container">
        <form action="{!! route('adminInsertData', ['insertDataType' => $pageType]) !!}" method="POST"
              enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if($pageType == 'movie')
                @include('adminPages.dataInsertionAdminPages.movieDataInsertionPage')
            @elseif($pageType == 'movie-trailer' || $pageType == 'tv-series-trailer')
                @include('adminPages.dataInsertionAdminPages.movieTrailerDataInsertionPage')
            @elseif($pageType == 'box-office')
                @include('adminPages.dataInsertionAdminPages.movieBoxOfficeInsertionPage')
            @elseif($pageType == 'profile')
                @include('adminPages.dataInsertionAdminPages.profileInsertionPage')
            @elseif($pageType == 'movie-cast-and-crew')
                @include('adminPages.dataInsertionAdminPages.movieCastAndCrewInsertionPage')
            @elseif($pageType == 'movie-award' || $pageType == 'movie-cast-and-crew-award')
                @include('adminPages.dataInsertionAdminPages.movieAwardsInsertionPage')
            @elseif($pageType == 'news')
                @include('adminPages.dataInsertionAdminPages.newsDataInsertionPage')
            @elseif($pageType == 'celeb-news' || $pageType == 'movie-news')
                @include('adminPages.dataInsertionAdminPages.typeWiseNewsIncluderPage')
            @elseif($pageType == 'movie-theaters')
                @include('adminPages.dataInsertionAdminPages.movieTheatersInsertionPage')
            @elseif($pageType == 'now-showing')
                @include('adminPages.dataInsertionAdminPages.nowShowingMoviesInTheatersInsertionPage')
            @elseif($pageType == 'tv-series')
                @include('adminPages.dataInsertionAdminPages.tvSeriesDataInsertion')
            @elseif($pageType == 'tv-series-seasons')
                @include('adminPages.dataInsertionAdminPages.tvSeriesSeasonsDataInsertionPage')
            @elseif($pageType == 'tv-series-episodes')
                @include('adminPages.dataInsertionAdminPages.tvSeriesEpisodesDataInsertion')
            @endif
        </form>
    </div>
</div>
</body>
</html>



