<!DOCTYPE html>
<html>
<head>
    <style>
        table p {
            font-size: 20px;
            color: #8a6d3b;
            font-weight: bolder;
        }

        table input {
            width: 250px;
            margin-left: 20px;
            height: 30px;
            font-size: 15px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table select {
            margin-left: 20px;
            font-size: 20px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table textarea {
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }
    </style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    <tr>
        <td>
            <p>Enter Theater Name:</p>
        </td>
        <td>
            <input type="text" name="theaterName" placeholder="Movie Theater Name" maxlength="50" required/>
        </td>
    </tr>
    <tr>
        <td>
            <p>Enter City Name:</p>
        </td>
        <td>
            <input type="text" name="locationCity" placeholder="City Name" maxlength="20" required/>
        </td>
    </tr>
    <tr>
        <td>
            <p>Select Country Code:</p>
        </td>
        <td>
            <select name="countryCode" required>
                <option value="US">US</option>
                <option value="BD">BD</option>
                <option value="IN">IN</option>
                <option value="UK">UK</option>
                <option value="CA">CA</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <p>Enter Address:</p>
        </td>
        <td>
            <input type="text" name="address" placeholder="Address Name" maxlength="100"/>
        </td>
    </tr>
    <tr>
        <td><p>Enter No Of Halls:</p></td>
        <td>
            <input type="number" name="noOfHalls" placeholder="No Of Halls" pattern="[1-9][0-9]{0,2}"/>
        </td>
    </tr>
    <tr>
        <td><p>Enter Website Link:</p></td>
        <td>
            <input type="url" name="webSite"/>
        </td>
    </tr>
    <tr>
        <td><p>Upload Movie Theater Image:</p></td>
        <td><input type="file" name="theaterImage" accept="image/*"></td>
    </tr>
</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
<input type="submit" value="Insert Data"
       style="margin: 50px; width: 150px; height: 40px; font-size: 20px; font-weight: bold; background: #286090; color: #FFFFFF; border: none; border-radius: 5px;"/>
</body>
</html>



