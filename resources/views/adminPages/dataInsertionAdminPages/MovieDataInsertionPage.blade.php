<!DOCTYPE html>
<html>
<head>
    <style>
        table p {
            font-size: 20px;
            color: #8a6d3b;
            font-weight: bolder;
        }

        table input {
            width: 250px;
            margin-left: 20px;
            height: 30px;
            font-size: 15px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table select {
            margin-left: 20px;
            font-size: 20px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table textarea {
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }


    </style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    <tr>
        <td>
            <p>Enter Movie Name:</p>
        </td>
        <td>
            <input type="text" name="movieName" placeholder="Movie Name" maxlength="50" required/>
        </td>
    </tr>
    <tr>
        <td>
            <p>Select Movie Genre:</p>
        </td>
        <td>
            <select name="genre" required>
                <option value="Action">Action</option>
                <option value="Adventure">Adventure</option>
                <option value="Comedy">Comedy</option>
                <option value="Romantic">Romantic</option>
                <option value="Drama">Drama</option>
                <option value="Thriller">Thriller</option>
                <option value="Horror">Horror</option>
                <option value="Sci-fi">Sci-fi</option>
                <option value="Biography">Biography</option>
                <option value="Animation">Animation</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Select Movie Language:</p></td>
        <td>
            <select name="language" required>
                <option value="English">English</option>
                <option value="Hindi">Hindi</option>
                <option value="Korean">Korean</option>
                <option value="Chinese">Chinese</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Select Movie Maturity:</p></td>
        <td>
            <select name="maturity">
                <option value="{!! null !!}">Maturity</option>
                <option value="PG">PG</option>
                <option value="PG-13">PG-13</option>
                <option value="R">R</option>
                <option value="G">G</option>
                <option value="NC-17">NC-17</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Enter Movie Duration:</p></td>
        <td>
            <input type="number" name="duration" placeholder="Movie Duration" pattern="[1-9][0-9]{0,3}"/>
        </td>
    </tr>
    <tr>
        <td><p>Enter Release Date:</p></td>
        <td>
            <input type="date" name="releaseDate" required/>
        </td>
    </tr>
    <tr>
        <td><p>Summary:</p></td>
        <td>
            <textarea name="summary" cols="40" rows="5" placeholder="Write Movie Summary" required></textarea>
        </td>
    </tr>
    <tr>
        <td><p>Story:</p></td>
        <td>
            <textarea name="story" cols="40" rows="5" placeholder="Write Story of the movie"></textarea>
        </td>
    </tr>
    <tr>
        <td><p>Upload Image:</p></td>
        <td><input type="file" name="imageName" accept="image/*" required></td>
    </tr>
</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
<input type="submit" value="Insert Data"
       style="margin: 50px; width: 150px; height: 40px; font-size: 20px; font-weight: bold; background: #286090; color: #FFFFFF; border: none; border-radius: 5px;"/>
</body>
</html>



