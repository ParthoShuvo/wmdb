<!DOCTYPE html>
<html>
<head>
    <style>
        table p {
            font-size: 20px;
            color: #8a6d3b;
            font-weight: bolder;
        }

        table input {
            width: 250px;
            margin-left: 20px;
            height: 30px;
            font-size: 15px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table select {
            margin-left: 20px;
            font-size: 20px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table textarea {
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }
    </style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    <tr>
        <td>
            <p>Select TV Series:</p>
        </td>
        <td>
            @if($tvSeriesData != null && count($tvSeriesData) > 0)
                <select name="tvSeriesId" required>
                    @foreach($tvSeriesData as $tvSeries)
                        <option value="{!! $tvSeries['tvSeriesId'] !!}">
                            {!! $tvSeries['seriesName'] !!}</option>
                    @endforeach
                </select>
            @endif
        </td>
    </tr>
    <tr>
        <td><p>Start Year:</p></td>
        <td>
            <input type="number" name="startYear" placeholder="Starting Year" pattern="[1-9][0-9]{4,4}" required/>
        </td>
    </tr>
    <tr>
        <td><p>Enter Season No:</p></td>
        <td>
            <input type="number" name="seasonNo" placeholder="Season No" pattern="[1-9][0-9]{0,2}" required/>
        </td>
    </tr>
</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
<input type="submit" value="Insert Data"
       style="margin: 50px; width: 150px;
       height: 40px; font-size: 20px; font-weight: bold;
       background: #286090; color: #FFFFFF; border: none; border-radius: 5px;"/>
</body>
</html>



