<!DOCTYPE html>
<html>
<style>
    table p {
        font-size: 20px;
        color: #8a6d3b;
        font-weight: bolder;
    }

    table input {
        width: 250px;
        margin-left: 20px;
        height: 30px;
        font-size: 15px;
        color: #ac2925;
        border: 1px solid #286090;
        border-radius: 2px;
    }

    table select {
        margin-left: 20px;
        font-size: 20px;
        color: #ac2925;
        border: 1px solid #286090;
        border-radius: 2px;
    }

    table textarea {
        color: #ac2925;
        border: 1px solid #286090;
        border-radius: 2px;
    }

</style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    <tr>
        <td>
            <p>Select Movie Name:</p>
        </td>
        <td>
            @if($movieData != null && count($movieData) > 0)
                <select name="movieId" required>
                    @foreach($movieData as $movie)
                        <option value="{!! $movie['movieId'] !!}">{!! $movie['movieName'] !!}</option>
                    @endforeach
                </select>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <p>Enter Budget:</p>
        </td>
        <td>
            <input type="number" name="budget" placeholder="Budget" pattern="[1-9][0-9]{0,15}"/>
        </td>
    </tr>
    <tr>
        <td>
            <p>Enter Weekend Collection:</p>
        </td>
        <td>
            <input type="number" name="weekendCollection" placeholder="Weekend Collection" pattern="[1-9][0-9]{0,15}"/>
        </td>
    </tr>
    <tr>
        <td>
            <p>Enter Gross:</p>
        </td>
        <td>
            <input type="number" name="gross" placeholder="Gross" pattern="[1-9][0-9]{0,15}"/>
        </td>
    </tr>

</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
<input type="submit" value="Insert Data"
       style="margin: 50px; width: 150px; height: 40px; font-size: 20px; font-weight: bold; background: #286090; color: #FFFFFF; border: none; border-radius: 5px;"/>
</body>
</html>



