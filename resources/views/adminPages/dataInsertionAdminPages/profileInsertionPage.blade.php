<!DOCTYPE html>
<html>
<style>
    table p {
        font-size: 20px;
        color: #8a6d3b;
        font-weight: bolder;
    }

    table input {
        width: 250px;
        margin-left: 20px;
        height: 30px;
        font-size: 15px;
        color: #ac2925;
        border: 1px solid #286090;
        border-radius: 2px;
    }

    table select {
        margin-left: 20px;
        font-size: 20px;
        color: #ac2925;
        border: 1px solid #286090;
        border-radius: 2px;
    }

    table textarea {
        color: #ac2925;
        border: 1px solid #286090;
        border-radius: 2px;
    }


</style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    <tr>
        <td>
            <p>Enter Movie Name:</p>
        </td>
        <td>
            <input type="text" name="name" placeholder="Celeb Name"
                   maxlength="30"  required/>
        </td>
    </tr>
    <tr>
        <td>
            <p>Select Profession:</p>
        </td>
        <td>
            <select name="profession" required>
                <option value="Actor">Actor</option>
                <option value="Director">Director</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Select Birth Name:</p></td>
        <td>
            <input type="text" name="birthName" placeholder="Birth Name"
                   maxlength="50"/>
        </td>
    </tr>
    <tr>
        <td><p>Select BirthDate:</p></td>
        <td>
            <input type="date" name="birthDate" placeholder="Birth Date"/>
        </td>
    </tr>
    <tr>
        <td><p>Select BirthPlace:</p></td>
        <td>
            <input type="text" name="birthPlace" placeholder="Birth Place"
                   maxlength="50"/>
        </td>
    </tr>
    <tr>
        <td><p>Bio:</p></td>
        <td>
            <textarea name="bio" cols="40" rows="5" placeholder="Write Movie Summary" required></textarea>
        </td>
    </tr>
    <tr>
        <td><p>Enter Height:</p></td>
        <td>
            <input type="number" step="0.01" name="height" placeholder="Height"/>
        </td>
    </tr>
    <tr>
        <td><p>Upload Image:</p></td>
        <td><input type="file" name="profileImage" accept="image/*"></td>
    </tr>
</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
<input type="submit" value="Insert Data"
       style="margin: 50px; width: 150px; height: 40px; font-size: 20px; font-weight: bold; background: #286090; color: #FFFFFF; border: none; border-radius: 5px;"/>
</body>
</html>



