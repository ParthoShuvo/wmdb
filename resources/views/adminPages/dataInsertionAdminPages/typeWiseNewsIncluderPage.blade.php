<!DOCTYPE html>
<html>
<head>
    <style>
        table p {
            font-size: 20px;
            color: #8a6d3b;
            font-weight: bolder;
        }

        table input {
            width: 250px;
            margin-left: 20px;
            height: 30px;
            font-size: 15px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table select {
            margin-left: 20px;
            font-size: 20px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table textarea {
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }
    </style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    @if(isset($movieData) && count($movieData) > 0)
        <tr>
            <td>
                <p>Select Movie Name:</p>
            </td>
            <td>
                @if($movieData != null && count($movieData) > 0)
                    <select name="movieId" required>
                        @foreach($movieData as $movie)
                            <option value="{!! $movie['movieId'] !!}">{!! $movie['movieName'] !!}</option>
                        @endforeach
                    </select>
                @endif
            </td>
        </tr>
    @else
        <tr>
            <td>
                <p>Select Profile Name:</p>
            </td>
            <td>
                @if($profileData != null && count($profileData) > 0)
                    <select name="profileId" required>
                        @foreach($profileData as $profile)
                            <option value="{!! $profile['profileId'] !!}">{!! $profile['name'] !!}</option>
                        @endforeach
                    </select>
                @endif
            </td>
        </tr>
    @endif
    <tr>
        <td><p>Select News:</p></td>
        <td>
            @if(isset($newsData) && count($newsData) > 0)
                <select name="newsId" required>
                    @foreach($newsData as $news)
                        <option value="{!! $news['newsId'] !!}">
                            {!! substr($news['headLine'], 20) !!}
                        </option>
                    @endforeach
                </select>
            @endif
        </td>
    </tr>
</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
@if(isset($newsData) && count($newsData) > 0)
    <input type="submit" value="Insert Data"
           style="margin: 50px; width: 150px; height: 40px; font-size: 20px; font-weight: bold;
       background: #286090; color: #FFFFFF; border: none; border-radius: 5px;"/>
@endif
</body>
</html>



