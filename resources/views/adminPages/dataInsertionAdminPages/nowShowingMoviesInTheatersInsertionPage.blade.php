<!DOCTYPE html>
<html>
<head>
<head>
    <style>
        table p {
            font-size: 20px;
            color: #8a6d3b;
            font-weight: bolder;
        }

        table input {
            width: 250px;
            margin-left: 20px;
            height: 30px;
            font-size: 15px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table select {
            margin-left: 20px;
            font-size: 20px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table textarea {
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        #radio {
            position: absolute;
            font-size: 20px;
            margin-top: 2px;
            color: #ac2925;
        }

    </style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    <tr>
        <td>
            <p>Select Movie Name:</p>
        </td>
        <td>
            @if($movieData != null && count($movieData) > 0)
                <select name="movieId" required>
                    @foreach($movieData as $movie)
                        <option value="{!! $movie['movieId'] !!}">{!! $movie['movieName'] !!}</option>
                    @endforeach
                </select>
            @endif
        </td>
    </tr>
    <tr>
        <td>
            <p>Select Movie Theater:</p>
        </td>
        <td>
            @if($movieTheaterData != null && count($movieTheaterData) > 0)
                <select name="theaterId" required>
                    @foreach($movieTheaterData as $movieTheater)
                        <option value="{!! $movieTheater['theaterId'] !!}">{!! $movieTheater['theaterName'] !!}</option>
                    @endforeach
                </select>
            @endif
        </td>
    </tr>
    <tr>
        <td><p>Movie Showing:</p></td>
        <td>
            <input type="radio" name="active" value="0" checked
                   style="margin: 0px; padding: 0px; width: 40px"/><b id="radio">No</b>
            <input type="radio" name="active" value="1"
                   style="margin: 0px; padding: 0px; width: 30px; margin-left: 30px;"/><b
                    id="radio">Yes</b>
        </td>
    </tr>
</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
<input type="submit" value="Insert Data"
       style="margin: 50px; width: 150px; height: 40px; font-size: 20px; font-weight: bold; background: #286090; color: #FFFFFF; border: none; border-radius: 5px;"/>
</body>
</html>



