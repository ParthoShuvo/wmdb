<!DOCTYPE html>
<html>
<head>
    <style>
        table p {
            font-size: 20px;
            color: #8a6d3b;
            font-weight: bolder;
        }

        table input {
            width: 250px;
            margin-left: 20px;
            height: 30px;
            font-size: 15px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table select {
            margin-left: 20px;
            font-size: 20px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table textarea {
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }
    </style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    <tr>
        <td>
            <p>Enter TV Series Name:</p>
        </td>
        <td>
            <input type="text" name="seriesName" placeholder="TV Series Name" maxlength="30" required/>
        </td>
    </tr>
    <tr>
        <td><p>Start Year:</p></td>
        <td>
            <input type="number" name="startYear" placeholder="Starting Year" pattern="[1-9][0-9]{4,4}" required/>
        </td>
    </tr>
    <tr>
        <td><p>End Year:</p></td>
        <td>
            <input type="number" name="endYear" placeholder="Ending Year" pattern="[1-9][0-9]{4,4}"/>
        </td>
    </tr>
    <tr>
        <td><p>Enter Duration Of Per Episode:</p></td>
        <td>
            <input type="number" name="duration" placeholder="duration" pattern="[1-9][0-9]{0,3}"/>
        </td>
    </tr>

    <tr>
        <td><p>Summary:</p></td>
        <td>
            <textarea name="summary" cols="40" rows="5" placeholder="Write TV Series Summary" required></textarea>
        </td>
    </tr>
    <tr>
        <td>
            <p>Enter TV Series broadcasting network:</p>
        </td>
        <td>
            <input type="text" name="network" placeholder="Network" maxlength="20" required/>
        </td>
    </tr>
    <tr>
        <td><p>Story:</p></td>
        <td>
            <textarea name="story" cols="40" rows="5" placeholder="Write Story of the TV Series"></textarea>
        </td>
    </tr>
    <tr>
        <td>
            <p>Select TV Series Genre:</p>
        </td>
        <td>
            <select name="genre" required>
                <option value="Action">Action</option>
                <option value="Adventure">Adventure</option>
                <option value="Comedy">Comedy</option>
                <option value="Romantic">Romantic</option>
                <option value="Drama">Drama</option>
                <option value="Thriller">Thriller</option>
                <option value="Horror">Horror</option>
                <option value="Sci-fi">Sci-fi</option>
                <option value="Biography">Biography</option>
                <option value="Animation">Animation</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Select Movie Maturity:</p></td>
        <td>
            <select name="maturity">
                <option value="{!! null !!}">Maturity</option>
                <option value="TV-G">TV-G</option>
                <option value="TV-Y">TV-Y</option>
                <option value="TV-Y7">TV-Y7</option>
                <option value="TV-PG">TV-PG</option>
                <option value="TV-14">TV-14</option>
                <option value="TV-MA">TV-MA</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Upload Image:</p></td>
        <td><input type="file" name="imageName" accept="image/*"></td>
    </tr>
</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
<input type="submit" value="Insert Data"
       style="margin: 50px; width: 150px; height: 40px;
       font-size: 20px; font-weight: bold; background: #286090;
       color: #FFFFFF; border: none; border-radius: 5px;"/>
</body>
</html>



