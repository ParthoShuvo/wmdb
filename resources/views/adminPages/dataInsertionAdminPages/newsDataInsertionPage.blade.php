<!DOCTYPE html>
<html>
<head>
    <style>
        table p {
            font-size: 20px;
            color: #8a6d3b;
            font-weight: bolder;
        }

        table input {
            width: 250px;
            margin-left: 20px;
            height: 30px;
            font-size: 15px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table select {
            margin-left: 20px;
            font-size: 20px;
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }

        table textarea {
            color: #ac2925;
            border: 1px solid #286090;
            border-radius: 2px;
        }


    </style>
</head>
<body>
@if(Session::has('errorMsg'))
    <p style="text-align: center; color: #843534;">Data already exist</p>
@endif
<table style="padding: 0px 50px;">
    <tr>
        <td>
            <p>Select News Category:</p>
        </td>
        <td>
            <select name="newsType" required>
                <option value="Profile">Celeb News</option>
                <option value="Movie">Movie News</option>
                <option value="TV">TV News</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <p>Enter News HeadLine:</p>
        </td>
        <td>
            <input type="text" name="headLine" placeholder="HeadLine" maxlength="100" required/>
        </td>
    </tr>
    <tr>
        <td><p>News:</p></td>
        <td>
            <textarea name="news" cols="40" rows="5" placeholder="Write news" required></textarea>
        </td>
    </tr>
    <tr>
        <td><p>Enter News Upload Date Time:</p></td>
        <td>
            <input type="datetime" name="uploadTime" placeholder="Upload Date Time" required/>
        </td>
    </tr>
</table>
<input type="hidden" name="pageType" value="{!! $pageType !!}"/>
<input type="submit" value="Insert Data"
       style="margin: 50px; width: 150px; height: 40px; font-size: 20px; font-weight: bold; background: #286090; color: #FFFFFF; border: none; border-radius: 5px;"/>
</body>
</html>



