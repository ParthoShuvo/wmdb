<!DOCTYPE html>
<html>
<head>
    <title>Admin Log In</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <link media="all" type="text/css" rel="stylesheet"
          href="{{ URL::asset('css/signInPageTemplateStyleSheets/signInPageTemplateStyleSheet.css') }}">

    <style>
        #wrongInputNotify {
            color: #d9534f;
            text-align: center;
            visibility: visible;
        }
        #leftHr, #rightHr
        {
            width: 130px;
        }
        #rightHr
        {
            margin-right: 20px;
            margin-left: 300px;;
        }
    </style>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    <div id="logInDiv"><!-- start of log in div -->
        <form action="{{ route('adminAuth')}}" method="post">
            <hr id="leftHr"/>
            <p id="logInTitleDiv">ADMIN LOG IN</p>
            <hr id="rightHr"/>
            @if(Session::has('flash_msg') && Session::get('flash_msg') != null)
                <p id="wrongInputNotify">{!! Session::get('flash_msg') !!}</p>
            @endif
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div id="userInput">
                <input type="text" name="userName" id="userName" placeholder="ADMIN NAME"
                       pattern="^(?=^.{8,}$)([a-zA-Z]+[0-9][a-zA-Z0-9]*|[0-9]+[a-zA-Z][a-zA-Z0-9]*)$"
                       required
                       title="Only alphabetic characters are accepted"/>
                <input type="password" name='password' id="password" placeholder="PASSWORD" pattern=".{8,}"
                       required
                       title="Minimum length of the password is 8"/>
            </div>
            <!--end of userInteractionDiv -->
            <input
                    style="width: 250px; height: 40px; font-size: larger; margin-left: 100px; right: 100px; margin-top: 20px;
                    margin-bottom: 20px; background: #c0a16b; border:0px; color: #fff;
                    border-radius: 3px 3px 3px 3px; font-family: 'Roboto Condensed', sans-serif;"
                    type="submit" value="LOG IN"/>
        </form>
    </div>
    <!-- end of log in div -->
@endsection
</body>
</html>







