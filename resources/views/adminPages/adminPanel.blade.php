<!DOCTYPE html>
<html>
<head>
    <title>Admin Panel</title>
    <style>
        #adminPanelContainerDiv {
            width: auto;
            height: auto;
            margin: 0px;
            background: #FFFFFF;
            margin-top: 1px;
        }

        #adminPanelContainerDiv > h1 {
            margin: 0px;
            padding: 0px;
            text-align: center;
            font-size: 30px;
            padding: 20px 0px;
            color: #2e6da4;
            background: #222222;
        }

        #adminPanelContainerDiv > h1 > hr {
            position: absolute;
            width: 330px;
            margin: 0px;
            padding: 0px;
            margin-top: 20px;
            margin-left: 10px;
            border: 1px #66512c solid;
        }

        #controlPanelDiv {
            border: 1px solid #843534;
            display: inline-block;
            width: auto;
            height: 700px;
            margin: 10px;
            border-radius: 5px;
        }

    </style>
</head>
<body>
@extends('templates.pageModelTemplate')
@section('content')
    <div id="adminPanelContainerDiv">
        <h1>
            <hr/>
            Welcome to Admin-Panel
            <hr style="margin-top: -17px; margin-left: 670px;"/>
        </h1>
        <div id="controlPanelDiv" style="">
            @include('templates.sideBarTemplates.adminPageControllerSideBarTemplate')
            @include('templates.sideBarTemplates.adminPanelControllerFrameTemplate')
        </div>
    </div>
@endsection
</body>
</html>



