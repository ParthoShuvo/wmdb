<!DOCTYPE html>
<html>
<head>
    <link media="all" type="text/css" rel="stylesheet"
          href="<?php echo e(URL::asset('css/contentsTemplatesStyleSheets/movieInfoContentsStyleSheet.css')); ?>"/>
</head>
<body>
<div id="movieInfoDiv">
    <div id="movieBasicInfoDiv">
        <img src="<?php echo e(URL::asset('images/movies/'.$movieData['imageName'])); ?>">

        <div id="movieInfo">
            <h1 id="movieName"><?php echo $movieData['movieName']; ?>

                <b style="color: #449d44; font-size: large">&nbsp;
                    (<?php use App\Http\DateTimeConverters;
                    echo DateTimeConverters::yearFormater($movieData['releaseDate']);
                    ?>)
                </b>
            </h1>

            <p id="movieViewInfo">
                <?php if($movieData['maturity'] != null && $movieData['maturity'] != ''): ?>
                    <?php echo $movieData['maturity']; ?>

                <?php else: ?>
                    <?php echo 'No maturity constraints added yet'; ?>

                <?php endif; ?>
                <b> |
                </b>
                <?php if($movieData['duration'] != null && $movieData['duration'] != ''): ?>
                    <?php echo $movieData['duration']. ' min'; ?>

                <?php else: ?>
                    <?php echo 'Duration Not added yet'; ?>

                <?php endif; ?>
                <b>| </b>
                <b style="color: #4cae4c"><?php
                    echo DateTimeConverters::DateFormater($movieData['releaseDate']);
                    ?>
                </b>
            </p>
            <hr/>
            <p id="movieGenre"><?php echo $movieData['genre']; ?></p>
            <?php if($movieReleased): ?>
                <p id="movieRate">Rate:</p>
                <?php if($movieRating != null && $movieRating['rate'] > 0): ?>
                    <img src="<?php echo e(URL::asset('images/staricon.png')); ?>"/>
                    <p id="rate"><?php echo $movieRating['rate']; ?></p>
                    <p id="userNumber">from <b style="color: #449d44">
                            <?php echo $movieRating['totalUser']; ?></b> users
                    </p>
                <?php else: ?>
                    <p style="color: #ffffff; position: absolute; margin: 0px; padding: 0px; margin-left: 320px; margin-top: -28px; font-size: 20px;">
                        Not Rated Yet</p>
                <?php endif; ?>
                <?php if($currentUserFeedBack == null): ?>
                    <a href="<?php echo route('feedbackMovie',  $movieData['movieName']); ?>" class="button">
                        Rate This
                    </a>
                <?php endif; ?>
            <?php else: ?>
                <b style="color:#ffffff ;position: absolute; margin: 0px; padding: 0px; margin-left: 270px; margin-top: -25px; font-size: 20px;">Not
                    released yet
                    <b style="color: #269abc; font-weight: normal;">
                        (voting begins after release)
                    </b>
                </b>
            <?php endif; ?>
            <hr/>
            <p id="summary"><?php echo $movieData['summary']; ?></p>
            <?php if($movieReleased): ?>
                <a href="<?php echo route('feedbackMovie',  $movieData['movieName']); ?>" id="watchlist">
                    <?php if(isset($currentUserFeedBack) && $currentUserFeedBack['userWatched'] == 1): ?>
                        &nbsp;<?php echo 'added to watchlist'; ?>

                    <?php else: ?>
                        <?php echo 'Add Watchlist'; ?>

                    <?php endif; ?>
                </a>
            <?php endif; ?>
            <?php if($hasMovieTrailer): ?>
                <a href="<?php echo route('movie_trailer_route', [$movieData['movieName']]); ?>" id="watchTrailer"
                   <?php if(!$movieReleased): ?>style="
                           margin-left: 70px;"
                        <?php endif; ?>>
                    Watch Trailer
                </a>
            <?php endif; ?>
            <?php if($movieReleased): ?>
                <a href="<?php echo route('feedbackMovie',  $movieData['movieName']); ?>" id="writeReview">
                    <?php if($currentUserFeedBack != null): ?>
                        Edit Your Review
                    <?php else: ?>
                        Write Review
                    <?php endif; ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="left">
        <div id="directorInfoDiv">
            <h1>Directors</h1>
            <?php if($directorsList != null && count($directorsList)): ?>
                <ul id="profileInfoList">
                    <?php foreach($directorsList as $director): ?>
                        <li>
                            <img src="<?php echo e(URL::asset('images/directors'.$director['image'])); ?>"/>
                            <a id="profileName"
                               href="<?php echo route('profileInfo', ['profileType' => 'director', 'profileName' => $director['directorName']]); ?>"><?php echo $director['directorName']; ?></a>

                            <p id="dot">&bull;&bull;&bull;</p>

                            <p id="role"><?php echo $director['directorPosition']; ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <?php echo $__env->make('templates.notAddedYetTemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        </div>
        <div id="actorsInfoDiv">
            <h1>Casts</h1>
            <?php if($actorsList != null && count($actorsList)): ?>
                <ul id="profileInfoList">
                    <?php foreach($actorsList as $actor): ?>
                        <li>
                            <img src="<?php echo e(URL::asset('images/actors/'.$actor['image'])); ?>"/>
                            <a id="profileName"
                               href="<?php echo route('profileInfo', [
                               'profileType' => 'actor',
                               'profileName' => $actor['actorName']]); ?>">
                                <?php echo $actor['actorName']; ?>

                            </a>

                            <p id="dot"
                               style="margin: 0px; padding: 0px; margin-top: 10px; margin-left: 150px;">&bull;&bull;&bull;</p>

                            <p id="role"><?php echo $actor['role']; ?>

                                <?php if($movieData['genre'] == 'Animation'): ?>
                                    <b style="color: #ffffff">(voice)</b>
                                <?php endif; ?>
                            </p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <?php echo $__env->make('templates.notAddedYetTemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        </div>
        <div id="storyLineDiv">
            <h1>StoryLine</h1>

            <p id="storyLine"><?php echo $movieData['story']; ?></p>
        </div>
        <div id="boxOfficeDiv">
            <h1>Box Office:</h1>
            <?php if($boxOffice != null): ?>
                <p id="budgetLabel">Budget:</p>

                <p id="budget"><?php echo $boxOffice['budget']; ?></p>

                <p id="openingWeekendLabel">Opening Weekend:</p>

                <p id="openingWeekend"><?php echo $boxOffice['openingWeekend']; ?></p>

                <p id="grossLabel">Gross:</p>

                <p id="gross"><?php echo $boxOffice['gross']; ?></p>
            <?php else: ?>
                <?php echo $__env->make('templates.notAddedYetTemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>

        </div>
        <div id="awardsDiv">
            <h1 style="margin-bottom: 10px;">Awards & Nominations</h1>
            <?php if($awardList != null && count($awardList) > 0): ?>
                <ul id="awardList">
                    <?php foreach($awardList as $award): ?>
                        <li>
                            <p id="awardName"><?php echo $award['awardName']; ?>

                                <b style="color: #ffffff">
                                    <?php echo $award['awardYear']; ?>

                                </b>
                            </p>

                            <p id="dot1">&bull;&bull;&bull;</p>

                            <p id="awardCategory"><?php echo $award['awardCategory']; ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <?php echo $__env->make('templates.notAddedYetTemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        </div>
    </div>
    <div id="rightContentDiv" class="right">
        <div id="reviewsDiv">
            <h1>Reviews</h1>
            <?php if($reviews == null && count($reviews) == 0): ?>
                <?php echo $__env->make('templates.notAddedYetTemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php else: ?>
                <ul>
                    <?php foreach($reviews as $review): ?>
                        <li>
                            <p id="userName"><?php echo $review['userName']; ?></p>

                            <p id="review"><?php echo $review['review']; ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
</body>
</html>



